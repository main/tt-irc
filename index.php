<?php
	set_include_path(get_include_path() . PATH_SEPARATOR .
		dirname(__FILE__) ."/include");

	if (!file_exists("config.php")) {
		print "<b>Fatal Error</b>: You forgot to copy
		<b>config.php-dist</b> to <b>config.php</b> and edit it.\n";
		exit;
	}

	require_once "functions.php";
	require_once "sanity_check.php";
	require_once "sessions.php";
	require_once "version.php";
	require_once "config.php";

	if (!sanity_check(true))
		return false;

	login_sequence();

	header('Content-Type: text/html; charset=utf-8');

	$user_theme = get_user_theme_path();

?>
<!DOCTYPE html>
<html id="html-root">
<head>
	<title data-bind="text: documentTitle">Tiny Tiny IRC</title>

	<?php stylesheet_tag("dist/app.min.css") ?>
	<?php stylesheet_tag("lib/bootstrap/css/bootstrap.min.css") ?>

	<script type="text/javascript">
		const __csrf_token = "<?php echo $_SESSION["csrf_token"]; ?>";
	</script>

	<?php
		$use_default_skin = true;
		$bodyclass = '';
		$params = get_user_theme_params();
		if ($params) {
			@$use_default_skin = !$params['no_default_skin'];
			$bodyclass = $params['dark'] ? 'dark' : '';
		}

		if ($bodyclass == "dark") {
			stylesheet_tag("lib/highlightjs/styles/pojoaque.css");
		} else {
			stylesheet_tag("lib/highlightjs/styles/default.css");
		}
	?>


	<?php if ($use_default_skin) stylesheet_tag("lib/bootstrap/css/bootstrap-theme.min.css") ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if (file_exists("local.css")) stylesheet_tag("local.css") ?>

	<link id="favicon" rel="shortcut icon" type="image/png" href="images/favicon.png" />

	<link rel="icon" type="image/png" sizes="144x144"
		href="images/icon-app.png" />

	<meta name="mobile-web-app-capable" content="yes">
	<meta http-equiv="Content-Security-Policy" content="block-all-mixed-content">

	<link rel="manifest" href="manifest.json?v2">

	<meta name="referrer" content="no-referrer">

	<?php
		javascript_tag("dist/app-libs.min.js");
		javascript_tag("dist/app.min.js");
	?>

	<?php if (file_exists("local.js")) javascript_tag("local.js") ?>

	<script type="text/javascript">
		'use strict';

		var _user_theme = "<?php echo $user_theme ?>";

		<?php
			init_js_translations();
		?>
	</script>

	<?php
		if ($user_theme) {
			stylesheet_tag("$user_theme/theme.css");

			if (file_exists("$user_theme/theme.js"))
				javascript_tag("$user_theme/theme.js");
		}
	?>

	<?php print_user_css(); ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<script type="text/javascript">
		'use strict';

		$(document).ready(function() {
			init();
		});

	</script>
</head>
<body class="main <?php echo $bodyclass ?>" user-theme="<?php echo $user_theme ?>">

<div id="dnd_overlay" style="display : none">
	<div class="inner">
		<?php echo __("Drop files to upload and post here.") ?>
	</div>
</div>

<div id="overlay" style="display : block">
	<div id="overlay_inner">
		<?php echo __("Loading, please wait...") ?>

		<div id="l_progress_o">
			<div id="l_progress_i"></div>
		</div>

	<noscript>
		<p><?php print_error(__("Your browser doesn't support Javascript, which is required
		for this application to function properly. Please check your
		browser settings.")) ?></p>
	</noscript>
	</div>
</div>

<div style="display : none" class="modal" id="infoBox">
	<div class="modal-dialog">
		<div class="modal-content">&nbsp;</div>
	</div>
</div>

<div id="searchBox" class="modal">

	<div class="modal-dialog">
		<form onsubmit="return model.search()">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" data-toggle='modal' data-target='#searchBox' class="close">&times;</button>
					<h4 class="modal-title"><?php echo __("Search") ?></h4>
				</div>
				<div class="modal-body">
					<div class="input-group">
						<input type="search" class="form-control search_query" placeholder="<?php echo __('Search for...') ?>"></input>
						<span class="input-group-btn">
							<button class="btn btn-default" type="button" onclick="model.clearSearch()">Clear</button>
						</span>
					</div>

					<div id="search-results-box" data-bind="with: activeChannel" >
						<ul class="" data-bind="foreach: displaySearchResults()">
							<li data-bind="html: format, attr: { rowid: $data.id, ident: $data.ident, ts: $data.unixts }, css: { 'sr-row': true, HL: is_hl, LV: is_lv }"> </li>
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="submit">
						<span class='glyphicon glyphicon-search'></span> <?php echo __('Search') ?></button>
					<button class="btn btn-default" data-toggle='modal' data-target='#searchBox'>
						<span class='glyphicon glyphicon-remove'></span> <?php echo __('Close') ?></button>

					<!-- goes after so it doesn't get clicked instead of search on submit -->
					<button class="btn btn-info pull-left" onclick="model.showHistory()">
						<span class='glyphicon glyphicon-time'></span> <?php echo __('History') ?></button>

				</div>
			</div>
		</div>
	</form>
</div>

<div id="notifyBox" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" data-toggle='modal' data-target='#notifyBox' class="close">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">&nbsp;</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-toggle='modal' data-target='#notifyBox'>
					<?php echo __('Close') ?></button>
			</div>
		</div>
	</div>
</div>

<div id="errorBox" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"><h4 class="modal-title text-danger"><?php echo __('Fatal Exception') ?></h4></div>
			<div class="modal-body">&nbsp;</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="window.location.reload()">
					<?php echo __('Reload') ?></button>
				<button class="btn btn-danger" data-toggle='modal' data-target='#errorBox'>
					<?php echo __('Ignore') ?></button>
			</div>
		</div>
	</div>
</div>


<div id="main">

<div id="tabs">
	<div id="main-toolbar">

		<a href="#" data-bind="visible: !networkError()">
			<span title="<?php echo __("Mute all alerts (Alt+M)") ?>"
				onclick="toggle_alerts()" class="text-muted glyphicon glyphicon-bell"
				data-bind="css : { 'text-danger' : !model.alerts_enabled() }"> </span></a>

		<a data-bind="visible: networkError">
			<span class="glyphicon glyphicon-exclamation-sign text-danger"
				title="<?php echo __("Communication problem with server.") ?>"> </span></a>

		<a href="#">
			<span class="text-muted glyphicon glyphicon-filter"
				title="<?php echo __("Filter muted channels (Alt+Shift+F)") ?>"
				id="filter-muted-prompt" onclick="toggle_filter_muted()"> </span></a>

		<?php
			$email = get_user_email($_SESSION['name']);

			if (strpos($email, "@localhost") === FALSE) {
				$grav_hash = md5(trim($email));
			?>
				<img onclick="Prefs.show()" class="avatar" title="<?php echo __('Preferences (Alt+P)') ?>" referrerpolicy="no-referrer"  src="https://secure.gravatar.com/avatar/<?php echo $grav_hash ?>?s=96">
		<?php } else { ?>
			<a href="#" onclick="Prefs.show()" title="<?php echo __("Preferences (Alt+P)") ?>"><span class="text-muted glyphicon glyphicon-cog"></a>
		<?php } ?>

		<?php if ($_SESSION["access_level"] >= 10) { ?>
		<a href="#" onclick="show_users()" title="<?php echo __("Users") ?>"><span class="text-warning glyphicon glyphicon-user"></a>
		<?php } ?>

		<?php if (!SINGLE_USER_MODE) { ?>
		<a title="<?php echo __("Log out (Ctrl+Alt+X)") ?>" href="backend.php?op=logout"><span class="text-danger glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
		<?php } ?>
	</div>

	<div id="tabs-inner">
		<ul id="tabs-list" data-bind="foreach: connections">
			<li channel="---" tab_type="S" data-bind="attr: { id: 'tab-' + id(), connection_id: id() }, css: { selected: selected, muted: muted(), attention: unread() > 0 }" onclick="change_tab(this, event)">
				<span class="glyphicon conn-pic" data-bind="attr: { id: 'cimg-' + id() }, css: { 'text-success glyphicon-ok-sign': connected(), 'text-muted glyphicon-remove-circle': !connected() }"> </span>
				<div data-bind="text: title"></div>
			</li>
			<ul class="sub-tabs" data-bind="foreach: channels, attr: { id: 'tabs-' + id() }">
				<li onclick="change_tab(this, event)" data-bind="attr: { id: 'tab-' + title() + ':' + $parent.id(), connection_id: $parent.id(), channel: title(), tab_type: type() }, css: { selected: selected, highlight: highlight, offline: offline, muted: muted(), attention: unread() > 0 }">
					<span onclick="close_tab(this)" title="Close this tab" class="close-img"
						data-bind="attr: { tab_id: 'tab-' + title() + ':' + $parent.id() }">&times;</span>
					<span class="unread" data-bind="visible: unread, text: unread"></span>
					<div class="indented" data-bind="text: title"></div>
				</li>
			</ul>
		</ul>
	</div>
</div>

<div id="content">
	<div id="topic"><div class="wrapper">
		<div class="form-control" data-bind="html: activeTopicFormatted, attr: { title: activeTopic }, css: { 'uneditable-input': true, disabled: activeTopicDisabled }" id="topic-input"></div>

		<textarea rows="1" type="text"
			class="form-control" id="topic-input-real" style="display : none"></textarea>
		</div>
	</div>

	<img id="spinner" style="display : none"
		alt="spinner" title="Loading..."
		src="<?php echo theme_image('images/indicator_tiny.gif') ?>"/>

	<div id="sidebar">

		<div id="sidebar-inner">

		<!-- fuck you very much, MSIE team -->
		<form action="javascript:void(null);" method="post" class="form form-inline">
		<div id="connect">
			<button class="btn btn-default"
				id="connect-btn" data-bind="style: { display: toggleConnection() ? 'inline' : 'none'  }, enable: toggleConnection, text: connectBtnLabel, click: toggleConnection"> </button>
		</span>

		</div>
		</form>

		<div id="userlist" data-bind="css: { 'show-away': away_visible }">
			<div class="userlist-aux-toolbar pull-right">
				<span class="text-muted glyphicon glyphicon-bell"
					title="<?php echo __("Mute channel alerts (Alt+Shift+M)") ?>"
					data-bind="css: { 'text-danger' : model.activeChannel() && model.activeChannel().muted }"
					onclick="toggle_chanmute()"> </span>

				<span title="<?php echo __("Show offline/away users (Alt+Shift+O)") ?>"
					onclick="toggle_away_visible()"
					class="text-muted glyphicon glyphicon-filter"
					data-bind="css: { 'text-success': away_visible }"> </span>

				<span class="text-muted glyphicon glyphicon-piggy-bank"
					title="<?php echo __("Toggle emoticons (Alt+E)") ?>"
					onclick="toggle_emoticons()" data-bind="css: { 'text-success': emoticons_visible }"> </span>

				<span class="text-muted glyphicon glyphicon-plus-sign"
					title="<?php echo __("Join channel (Alt+J)") ?>"
					onclick="join_channel()"> </span>

				<span>
					<label class="text-muted glyphicon glyphicon-upload"
						style="cursor : pointer"
						title="<?php echo __("Upload and post (Alt+Shift+U)") ?>">
						<form id="upload_and_post" enctype="multipart/form-data">
							<input type="hidden" name="op" value="uploadandpost"/>
							<input type="file" name="file" style="display : none" onchange="UploadAndPost.handleForm()"/>
						</form>
					</label>
				</span>
			</div>

			<div data-bind="with: activeChannel" id="userlist-inner">
				<ul id="userlist-list" data-bind="foreach: nicklist">
					<li data-bind="attr: { ident: $root.getNickIdent($root.activeChannel().connection_id(), $data) }, css: { away: $root.isUserAway($root.activeChannel().connection_id(), $data) }">
						<span class="user-pic text-muted glyphicon glyphicon-user" data-bind="css: { 'text-success': $root.nickIsOp($data), 'text-warning': $root.nickIsVoiced($data) }"> </span>
						<span class="nick" onclick="query_user(this)"
							data-bind="text: $root.stripNickPrefix($data),
								attr: { nick: $root.stripNickPrefix($data),
									title: $root.getNickHost($root.activeChannel().connection_id(), $data) }"></span>
					</li>
				</ul>
			</div>
		</div> <!-- userlist -->
	</div> <!-- sidebar-inner -->

	<div id="emoticons" data-bind="visible: emoticons_visible"><?php echo render_emoticons() ?></div>

	</div> <!-- sidebar -->

	<div id="log" data-bind="with: activeChannel" >
	<ul id="log-list" data-bind="foreach: displayLines">
		<li data-bind="html: format, attr: { rowid: $data.id, ident: $data.ident, ts: $data.unixts }, css: { row: true, HL: is_hl, LV: is_lv }"> </li>
	</ul>
	</div> <!-- log -->

	<div id="nick" onclick="change_nick()" data-bind="text: activeNick, css: { away: isAway }"></div>

	<div id="input"><div class="wrapper">
		<ul class="emoticons-popup" style="display : none"></ul>
		<textarea data-bind="enable: inputEnabled"
			class="form-control" rows="1" id="input-prompt"/></textarea>
	</div></div>

</div>

</div>

</body>
</html>
