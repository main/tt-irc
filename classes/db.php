<?php
require_once "config.php";

class DB {
	private static $instance;
	private $dbh;

	private function __construct() {

		$this->dbh = new PDO('pgsql:dbname='.DB_NAME.';host='.DB_HOST.';port='.DB_PORT,
			DB_USER,
			DB_PASS);

		$this->dbh->query("set client_encoding = 'UTF-8'");
		$this->dbh->query("set datestyle = 'ISO, european'");
		$this->dbh->query("set time zone '".DB_TIMEZONE."'");

	}

	private function __clone() {
		//
	}

	public static function get() {
		if (self::$instance == null)
			self::$instance = new self();

		return self::$instance->dbh;
	}

}
