package org.fox.ttirc;

import org.json.simple.JSONValue;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.*;
import java.lang.Thread.*;
import java.nio.channels.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.*;
import java.util.prefs.Preferences;

public class Master {

	private final static int MAX_REDIS_CONNECTIONS = 1024;
	private final static String MASTER_VERSION = "1.1";
	private final static int CONFIG_VERSION = 4;
	private final static String LOCK_FILE_NAME = "master.lock";
	private final static int SCHEMA_VERSION = 16;
	private final static long MASTER_IDLE_TIMEOUT = 5*1000L;
	private final static long CONNECTION_THROTTLE_TIMEOUT = 60 * 1000L;

	private Preferences m_prefs;
	private boolean m_active;
	private ConcurrentHashMap<Integer, ConnectionHandler> m_connectionHandlers = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Integer, Long> m_handlerSpawnTimes = new ConcurrentHashMap<>();

	private String m_lockDir;
	private FileLock m_lock;
	private FileChannel m_lockChannel;
	private Connection m_conn;
	private JedisPool m_jedisPool;

	private String m_jdbcUrl;
	private String m_dbUser;
	private String m_dbPass;

	private Logger logger = Logger.getLogger("org.fox.ttirc");

	public static void main(String[] args) {
		Master m = new Master(args);
		m.run();
	}

	private boolean isSchemaValid() {
		try {
			Statement st = m_conn.createStatement();

			st.execute("SELECT schema_version FROM ttirc_version");

			ResultSet rs = st.getResultSet();

			if (rs.next()) {
				return SCHEMA_VERSION == rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	Logger getLogger() {
		return logger;
	}

	String getLockDir() {
		return m_lockDir;
	}

	Jedis getJedis() {
		//logger.info("getJedis, Act:" + m_jedisPool.getNumActive() + " Idle:" + m_jedisPool.getNumIdle() + " Waiters:" + m_jedisPool.getNumWaiters());
		return m_jedisPool.getResource();
	}

	Connection initConnection() throws SQLException {
		Connection conn = DriverManager.getConnection(m_jdbcUrl, m_dbUser, m_dbPass);

		String dbTimezone = m_prefs.get("DB_TIMEZONE", "UTC");

		Statement st = conn.createStatement();
		// lol why cant i use preparedstatement here ffs
	  	st.execute("SET TIME ZONE '"+dbTimezone+"'");
		st.close();

		return conn;
	}

	private boolean lock() {
		File f = new File(m_lockDir + File.separator + LOCK_FILE_NAME);

		try {
			m_lockChannel = new RandomAccessFile(f, "rw").getChannel();
			m_lock = m_lockChannel.tryLock();

			if (m_lock != null) {
				logger.info("Lock acquired successfully (" + f.getName() + ").");
			} else {
				logger.severe("Couldn't acquire the lock: maybe another daemon is already running?");
				return false;
			}

		} catch (Exception e) {
			logger.severe("Couldn't make the lock: " + e.toString());
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private Master(String args[]) {
		m_prefs = Preferences.userNodeForPackage(getClass());
		m_active = true;

		String prefsNode = null;
		boolean needConfigure = false;
		boolean showHelp = false;
		boolean cleanupOnly = false;
		String logFileName = null;
		boolean needVersion = false;

		for (int i = 0; i < args.length; i++) {
			String arg = args[i];

			if (arg.equals("-help")) showHelp = true;
			if (arg.equals("-node")) prefsNode = args[i+1];
			if (arg.equals("-configure")) needConfigure = true;
			if (arg.equals("-cleanup")) cleanupOnly = true;
			if (arg.equals("-log")) logFileName = args[i+1];
			if (arg.equals("-version")) needVersion = true;
		}

		if (needVersion) {
			System.out.println("Tiny Tiny IRC " + getVersion());

			int year = Calendar.getInstance().get(Calendar.YEAR);

			System.out.println("Copyright (C) 2010-"+year+" Andrew Dolgov <cthulhoo(at)gmail.com>");
			System.exit(0);
		}

		if (showHelp) {
			System.out.println("Available options:");
			System.out.println("==================");
			System.out.println(" -help              - Show this help");
			System.out.println(" -node node         - Use custom preferences node");
			System.out.println(" -configure         - Display configuration dialog");
			System.out.println(" -cleanup           - Cleanup data and exit");
			System.out.println(" -log file          - Enable logging to specified file");
			System.out.println(" -version           - Display version information and exit");
			System.exit(0);
		}

		if (logFileName != null) {
			try {
				logger.info("Enabling logging to: " + logFileName);
				Handler fh = new FileHandler(logFileName);
				fh.setFormatter(new SimpleFormatter());
				logger.addHandler(fh);
			} catch (IOException e) {
				logger.warning(e.toString());
				e.printStackTrace();
			}
		}

		logger.info("Master " + getVersion() + " starting up...");

		if (prefsNode != null) {
			logger.info("Using custom preferences node: " + prefsNode);
			m_prefs = m_prefs.node(prefsNode);
		}

		if (m_prefs.getInt("CONFIG_VERSION", -1) != CONFIG_VERSION || needConfigure) {
			configure();
		}

		String LOCK_DIR = m_prefs.get("LOCK_DIR", "/var/tmp");

		File f = new File(LOCK_DIR);

		if (!f.isDirectory() || !f.canWrite()) {
			logger.severe("Lock directory [" + LOCK_DIR + "] must be a writable directory." );
			System.exit(1);
		}

		m_lockDir = LOCK_DIR;

		if (!lock()) System.exit(1);

		String dbHost = m_prefs.get("DB_HOST", "localhost");
		String dbUser = m_prefs.get("DB_USER", "user");
		String dbPass = m_prefs.get("DB_PASS", "pass");
		String dbName = m_prefs.get("DB_NAME", "user");
		String dbPort = m_prefs.get("DB_PORT", "5432");

		String jdbcUrl = null;

		jdbcUrl = "jdbc:postgresql://" + dbHost + ":" + dbPort +
			"/" + dbName;

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			logger.severe("error: JDBC driver for PostgreSQL not found.");
			e.printStackTrace();
			System.exit(1);
		}

		m_jdbcUrl = jdbcUrl;
		m_dbUser = dbUser;
		m_dbPass = dbPass;

		try {
			logger.info("Establishing database connection (" + jdbcUrl + ")...");
			m_conn = initConnection();
		} catch (SQLException e) {
			logger.severe("error: Couldn't connect to database.");
			e.printStackTrace();
			System.exit(1);
		}

		try {
			String redisHost = m_prefs.get("REDIS_HOST", "127.0.0.1");
			int redisPort = m_prefs.getInt("REDIS_PORT", 6379);

			logger.info("Connecting to Redis at " + redisHost + ":" + redisPort + "...");

			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxTotal(MAX_REDIS_CONNECTIONS);
			config.setMaxIdle(MAX_REDIS_CONNECTIONS);
			config.setMaxWaitMillis(1);
			config.setBlockWhenExhausted(false);

			m_jedisPool = new JedisPool(config, redisHost, redisPort);
		} catch (Exception e) {
			logger.severe("error: Couldn't connect to Redis.");
			e.printStackTrace();
			System.exit(1);
		}

		logger.info("Connections established.");

		cleanup();

		if (cleanupOnly)
			System.exit(0);

		if (!isSchemaValid()) {
			logger.severe("error: Incorrect schema version.");
			System.exit(1);
		}

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			cleanup();
			try {
				m_lock.release();
				m_lockChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}));
	}

	public String getVersion() {
		return MASTER_VERSION;
	}

	private void readOption(BufferedReader reader, Preferences prefs,
							String prefName, String caption, String defaultValue) throws IOException {

		String def = prefs.get(prefName, defaultValue);

		System.out.print(String.format("%s [%s]: ", caption, def));
		String in = reader.readLine();

		if (in.length() == 0) in = def;

		if (prefName.equals("LOCK_DIR")) {
			File f = new File(in);
			in = f.getAbsolutePath();
		}

		prefs.put(prefName, in);

	}

	private void configure() {
		System.out.println("Tiny Tiny IRC backend configuration");
		System.out.println("===================================");

		InputStreamReader input = new InputStreamReader(System.in);
		BufferedReader reader = new BufferedReader(input);
		boolean configured = false;

		try {

			while (!configured) {

				readOption(reader, m_prefs, "LOCK_DIR", "Directory for lock files", "/var/tmp");

				readOption(reader, m_prefs, "DB_HOST", "PostgreSQL database host", "localhost");
				readOption(reader, m_prefs, "DB_PORT", "Database port (usually 5432 or 3306)", "5432");
				readOption(reader, m_prefs, "DB_NAME", "Database name", "ttirc_db");
				readOption(reader, m_prefs, "DB_USER", "Database user", "ttirc_user");
				readOption(reader, m_prefs, "DB_PASS", "Database password", "ttirc_pwd");
				readOption(reader, m_prefs, "DB_TIMEZONE", "Database timezone (same as DB_TIMEZONE in config.php)", "UTC");
				readOption(reader, m_prefs, "REDIS_HOST", "Redis hostname", "127.0.0.1");
				readOption(reader, m_prefs, "REDIS_PORT", "Redis port", "6379");

				System.out.print("Done? [Y/N] ");
				String done = reader.readLine();

				configured = done.equalsIgnoreCase("Y");
			}

			m_prefs.putInt("CONFIG_VERSION", CONFIG_VERSION);

			System.out.println("Data saved. Please use -configure to change it later.");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void cleanup() {
		try {
			logger.info("Cleaning up...");

			PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections SET " +
					"status = ?, userhosts = '', active_server = ''");

			ps.setInt(1, Constants.CS_DISCONNECTED);
			ps.execute();
			ps.close();

			Statement st = m_conn.createStatement();

			st.execute("UPDATE ttirc_channels SET nicklist = ''");
			st.execute("UPDATE ttirc_system SET value = false WHERE key = 'MASTER_RUNNING'");

			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateHeartbeat() throws SQLException {
		Statement st = m_conn.createStatement();

		st.execute("UPDATE ttirc_system SET value = true WHERE key = 'MASTER_RUNNING'");
		st.execute("UPDATE ttirc_system SET value = NOW() WHERE key = 'MASTER_HEARTBEAT'");

		st.close();
	}

	private synchronized void checkConnectionHandlers() throws SQLException {

		for (int connectionId : m_connectionHandlers.keySet()) {

			ConnectionHandler ch = m_connectionHandlers.get(connectionId);

    		PreparedStatement ps = m_conn.prepareStatement("SELECT id FROM ttirc_connections WHERE id = ? " +
    			"AND enabled = true");

    		ps.setInt(1, connectionId);
    		ps.execute();

    		ResultSet rs = ps.getResultSet();

    		if (!rs.next()) {
    			try {
    				if (ch.getState() != State.TERMINATED) {
    	    			logger.info("Connection " + connectionId + " needs termination.");
    					ch.kill();
    				}
    			} catch (Exception ee) {
    				logger.warning(ee.toString());
    				ee.printStackTrace();
    			}
    		}

    		if (ch.getState() == State.TERMINATED) {
    			logger.info("Connection " + connectionId + " terminated.");
    			m_connectionHandlers.remove(connectionId);
    		}
		}

		Statement st = m_conn.createStatement();

		// kill aborted connecting... attempts
		st.execute("SELECT id FROM ttirc_connections WHERE " +
				"status = "+Constants.CS_CONNECTING+" AND enabled = false");

		ResultSet rs = st.getResultSet();

		while (rs.next()) {
			int connectionid = rs.getInt(1);

			setStatus(connectionid, Constants.CS_DISCONNECTED);
			pushMessage(connectionid, "---", "DISCONNECT", true, Constants.MSGT_EVENT, "---");
		}

		st.execute("SELECT ttirc_connections.id " +
	    		"FROM ttirc_connections, ttirc_users " +
	    		"WHERE owner_uid = ttirc_users.id AND " +
	    		"visible = true AND " +
	    		"(heartbeat > NOW() - INTERVAL '5 minutes' OR " +
	    		"permanent = true) AND " +
	    		"enabled = true ORDER BY id");

	    rs = st.getResultSet();

	    while (rs.next()) {
			 int connectionId = rs.getInt(1);

			 logger.info("Checking connection " + connectionId);

	    	if (!m_connectionHandlers.containsKey(connectionId)) {

				if (m_handlerSpawnTimes.containsKey(connectionId)) {
					long delta = new Date().getTime() - m_handlerSpawnTimes.get(connectionId);

					if (delta < CONNECTION_THROTTLE_TIMEOUT) {
						logger.info("Unable to spawn connection " + connectionId + ", throttled.");

						pushMessage(connectionId, "---", "CONNECTION_THROTTLED",
							true, Constants.MSGT_EVENT, "---");

						setStatus(connectionId, Constants.CS_CONNECTING);

						continue;
					}
				}

				logger.info("Spawning connection " + connectionId);

				ConnectionHandler ch = new NativeConnectionHandler(connectionId, this);
				m_connectionHandlers.put(connectionId, ch);
				m_handlerSpawnTimes.put(connectionId, new Date().getTime());

				ch.start();

				// spawn only one connection at a time
				return;
	    	}
	    }
	}

	private int connectionOwner(int connectionId) throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT owner_uid FROM " +
				"ttirc_connections WHERE id = ?");

		ps.setInt(1, connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		if (rs.next()) {
			return rs.getInt(1);
		} else {
			return -1;
		}
	}

	private String getNick(int connectionId) throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT active_nick FROM " +
				"ttirc_connections WHERE id = ?");

		ps.setInt(1, connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		if (rs.next()) {
			return rs.getString(1);
		} else {
			return "?UNKNOWN?";
		}
	}

	void pushMessage(int connectionId, String channel, String message,
			boolean incoming, int messageType, String fromNick) throws SQLException {

		String nick;

		if (channel.equals("---")) {
			nick = "---";
		} else {
			nick = getNick(connectionId);
		}

		int ownerUid = connectionOwner(connectionId);

		if (fromNick.length() != 0) nick = fromNick;

		Long messageId = messageId();

		Map<String, Object> messageObject = new HashMap<>();

		messageObject.put("id", messageId);
		messageObject.put("sender", nick);
		messageObject.put("message", message);
		messageObject.put("message_type", messageType);
		messageObject.put("ts", System.currentTimeMillis() / 1000L);
		messageObject.put("channel", channel);
		messageObject.put("incoming", incoming);

		try {
			String messageString = JSONValue.toJSONString(messageObject);

			try (Jedis jedis = getJedis()) {
				jedis.rpush("ttirc_messages," + connectionId, messageString);
				jedis.set("ttirc_last_message," + ownerUid, String.valueOf(messageId));
				jedis.publish("ttirc_connection," + connectionId, "UPDATE");
				jedis.publish("ttirc_user," + ownerUid, "UPDATE");
			}
		} catch (Exception e) {
			logger.warning("[pushMessage error] " + channel + " " + message);
			e.printStackTrace();
		}
	}

	Long messageId() {
		Long id;

		try (Jedis jedis = getJedis()) {
			id = jedis.incr("ttirc_last_message_id");
		}

		return id;
	}

	void setStatus(int connectionId, int status) throws SQLException {
		logger.info("[" + connectionId + "] Setting status: " + status);

		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections SET " +
				"status = ?, userhosts = '' WHERE id = ?");

		ps.setInt(1, status);
		ps.setInt(2, connectionId);
		ps.execute();
		ps.close();
	}

	private void run() {
		logger.info("Waiting for clients...");

		while (m_active) {
			try {
				//logger.info("Checking connections and status, " + m_connectionHandlers.keySet().size() + " active.");

				if (!isSchemaValid()) {
					logger.severe("error: Incorrect schema version.");
					System.exit(1);
				}

				updateHeartbeat();
				checkConnectionHandlers();

				Thread.sleep(MASTER_IDLE_TIMEOUT);

			} catch (SQLException e) {
				logger.severe(e.toString());
				System.exit(1);
			} catch (InterruptedException ei) {
				//
			}
		}

		cleanup();
	}

	public void finalize() throws Throwable {
		cleanup();

		m_lock.release();
		m_lockChannel.close();

		m_conn.close();
	}
}
