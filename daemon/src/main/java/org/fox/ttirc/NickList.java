package org.fox.ttirc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import org.json.simple.*;

public class NickList {

	private Connection m_conn;

	public class NickComparator implements Comparator<String> {

		public int compare(String o1, String o2) {
			char c1 = o1.charAt(0);
			char c2 = o2.charAt(0);

			if (c1 == '@' && c2 != '@') {
				return 0;
			}

			if (c1 != '@' && c2 == '@') {
				return 1;
			}

			if (c1 == '+' && c2 != '+') {
				return 0;
			}

			if (c1 != '+' && c2 == '+') {
				return 1;
			}

			return o1.compareTo(o2);
		}

	}

	public class Nick {
		private String nick;
		private boolean v = false;
		private boolean o = false;

		Nick(String nick) {

			if (nick.charAt(0) == '@') {
				nick = nick.substring(1);
				o = true;
			} else if (nick.charAt(0) == '+') {
				nick = nick.substring(1);
				v = true;
			}

			this.nick = nick;
		}

		void renameTo(String nick) {
			this.nick = nick;
		}

		void setVoiced(boolean v) {
			this.v = v;
		}

		void setOp(boolean o) {
			this.o = o;
		}

		public boolean isVoiced() {
			return v;
		}

		public boolean isOp() {
			return o;
		}

		String stripPrefix(String nick) {
			if (nick.charAt(0) == '@') {
				nick = nick.substring(1);
			} else if (nick.charAt(0) == '+') {
				nick = nick.substring(1);
			}
			return nick;
		}

		public boolean equals(Object obj) {
			return this.nick.equalsIgnoreCase(stripPrefix(obj.toString()));
		}

		public String toString() {
			String prefix = "";

			if (o)
				prefix += "@";
			else if (v)
				prefix += "+";

			return prefix + nick;
		}

		public int hashCode() {
			return nick.hashCode();
		}

		void updateModes(String nick) {
			if (nick.charAt(0) == '@') {
				o = true;
			} else if (nick.charAt(0) == '+') {
				v = true;
			}
		}

		/* returns nick without mode prefixes */
		public String getNick() {
			return nick;
		}
	}

	private Hashtable<String, Vector<Nick>> nicklist = new Hashtable<String, Vector<Nick>>();
	private NativeConnectionHandler handler;

	NickList(NativeConnectionHandler handler, Connection conn) {
		this.handler = handler;
		this.m_conn = conn;
	}

	void addNick(String chan, String nick) {

		if (!nicklist.containsKey(chan))
			nicklist.put(chan, new Vector<Nick>());

		//Nick n = new Nick(nick);
		Nick n = findNick(chan, nick);

		if (n != null) {
			n.updateModes(nick);
		} else {
			n = new Nick(nick);
			nicklist.get(chan).add(n);

			handler.requestUserhost(n);

			//System.out.println("Added " + nick + " on " + chan);
		}

		Sync(chan);
	}

	void removeNick(String chan, String nick) {
		if (!nicklist.containsKey(chan))
			nicklist.put(chan, new Vector<Nick>());

		Nick n = new Nick(nick);

		nicklist.get(chan).remove(n);

		if (numChans(nick) == 0)
			handler.removeUserhost(nick);

		Sync(chan);
	}

	void removeNick(String nick) {

		Enumeration<String> chans = nicklist.keys();

		Nick n = new Nick(nick);

		while (chans.hasMoreElements()) {
			String chan = chans.nextElement();

			nicklist.get(chan).remove(n);
		}

		handler.removeUserhost(nick);

		Sync();
	}

	private int numChans(String nick) {
		Enumeration<String> chans = nicklist.keys();
		int rv = 0;
		Nick n = new Nick(nick);

		while (chans.hasMoreElements()) {
			String chan = chans.nextElement();
			if (nicklist.get(chan).contains(n)) {
				++rv;
			}
		}

		return rv;
	}

	Vector<String> isOn(String nick) {

		Vector<String> tmp = new Vector<String>();
		Enumeration<String> chans = nicklist.keys();

		Nick n = new Nick(nick);

		while (chans.hasMoreElements()) {
			String chan = chans.nextElement();
			if (nicklist.get(chan).contains(n)) {
				tmp.add(chan);
			}
		}

		return tmp;
	}

	private void Sync() {
		Enumeration<String> en = nicklist.keys();

		while (en.hasMoreElements()) {
			String chan = en.nextElement();
			Sync(chan);
		}
	}

	private Nick findNick(String channel, String nick) {
		Enumeration<Nick> nicks = nicklist.get(channel).elements();

		while (nicks.hasMoreElements()) {
			Nick n = nicks.nextElement();

			if (n.equals(nick)) return n;
		}

		return null;
	}

	boolean setVoiced(String channel, String nick, boolean v) {
		Nick n = findNick(channel, nick);

		//System.err.println("setVoiced " + channel + " " + nick + " = " + v + "; " + n);

		if (n != null) {
			n.setVoiced(v);
			Sync(channel);
			return true;
		}
		return false;
	}

	boolean setOp(String channel, String nick, boolean o) {
		Nick n = findNick(channel, nick);

		if (n != null) {
			n.setOp(o);
			Sync(channel);
			return true;
		}
		return false;
	}

	void renameNick(String oldNick, String newNick) {
		Enumeration<String> chans = nicklist.keys();

		while (chans.hasMoreElements()) {
			String chan = chans.nextElement();

			Enumeration<Nick> nicks = nicklist.get(chan).elements();

			while (nicks.hasMoreElements()) {
				Nick nick = nicks.nextElement();

				if (nick.equals(oldNick)) {
					nick.renameTo(newNick);
					handler.renameUserhost(oldNick, newNick);
					handler.pushMessage(oldNick, chan, "NICK:" + newNick, Constants.MSGT_EVENT);
					handler.renamePrivateChannel(oldNick, newNick);
				}
			}
		}

		Sync();
	}

	@SuppressWarnings("unchecked")
	private void Sync(String channel)  {

		Enumeration<Nick> en = this.nicklist.get(channel).elements();

		JSONArray nicks = new JSONArray();

		while (en.hasMoreElements()) {
			Nick n = en.nextElement();
			nicks.add(n.toString());
		}

		//Collections.sort(nicks, new NickComparator());

		try {

			PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_channels " +
				"SET nicklist = ? WHERE LOWER(channel) = LOWER(?) AND connection_id = ?");

			ps.setString(1, nicks.toJSONString());
			ps.setString(2, channel);
			ps.setInt(3, handler.getConnectionId());
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		handler.updateLastChanmod();
	}

	/*public Vector<Nick> getNicks(String chan) {
		return nicklist.get(chan);
	}*/

	void removeChannel(String chan) {
		Enumeration<Nick> en = nicklist.get(chan).elements();

		while (en.hasMoreElements()) {
			Nick n = en.nextElement();

			if (numChans(n.getNick()) == 1)
				handler.removeUserhost(n.getNick());
		}

		nicklist.remove(chan);
	}
}
