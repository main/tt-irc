package org.fox.ttirc;

import org.fox.ttirc.irclib.IRCConnection;
import org.fox.ttirc.irclib.IRCEventListener;
import org.fox.ttirc.irclib.IRCModeParser;
import org.fox.ttirc.irclib.IRCUser;
import org.fox.ttirc.irclib.ssl.SSLIRCConnection;
import org.json.simple.JSONValue;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

public class NativeConnectionHandler extends ConnectionHandler {
	private final static String LOCK_FILE_NAME = "handle.m_conn-%d.m_lock";

	private int m_connectionId;
	private Master m_master;
	private Long m_lastSentId = 0L;

	private NickList m_nicklist;
	private ExtNickInfo m_extnickinfo = new ExtNickInfo(this);
	private Logger logger;

	private IRCConnection m_irc;
	private boolean m_active = true;

	private String m_lockDir;
	private FileLock m_lock;
	private FileChannel m_lockChannel;
	private Connection m_conn;
	private JedisSubscriber m_subscriber;

	private long m_lastPingSent;
	private long m_lastReceived;
	private long m_lastWhoSent;
	private int m_ownerUid;

	NativeConnectionHandler(int connectionId, Master master) {
		m_connectionId = connectionId;
		m_master = master;
		try {
			m_nicklist = new NickList(this, m_master.initConnection());
		} catch (SQLException e) {
			e.printStackTrace();
			m_active = false;
		}

		logger = m_master.getLogger();
		m_lockDir = m_master.getLockDir();

		Thread thread = new Thread(() -> {
			try {
				String channel = "ttirc_connection," + connectionId;

				logger.info("Subscribing to update channel: " + channel);

				m_subscriber = new JedisSubscriber();

				try (Jedis jedis = master.getJedis()) {
					jedis.subscribe(m_subscriber, channel);
				}

				logger.info("Ended subscription to: " + channel);

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		thread.start();

	}

	Master getMaster() {
		return m_master;
	}

	void requestUserhost(NickList.Nick nick) {
		if (!m_extnickinfo.hasNick(nick.getNick())) {
			//System.out.println("requestUserhost [N] " + nick.getNick());
			m_irc.doWho(nick.getNick());
			m_extnickinfo.update(nick.getNick());
		}
	}

	void removeUserhost(String nick) {
		m_extnickinfo.remove(nick);
	}

	void renameUserhost(String oldNick, String newNick) {
		m_extnickinfo.renameNick(oldNick, newNick);
	}

	void renamePrivateChannel(String oldNick, String newNick) {
		try {
			PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_channels SET " +
				"channel = ? WHERE LOWER(channel) = LOWER(?) AND chan_type = ? AND connection_id = ? AND " +
				"(SELECT COUNT(id) FROM ttirc_channels WHERE channel = ? and connection_id = ?) = 0");

			ps.setString(1, newNick);
			ps.setString(2, oldNick);
			ps.setInt(3, Constants.CT_PRIVATE);
			ps.setInt(4, m_connectionId);
			ps.setString(5, oldNick);
			ps.setInt(6, m_connectionId);

			ps.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		updateLastChanmod();
	}

	private void setActive(boolean active) {
		m_active = active;
	}

	private void setStatus(int status) throws SQLException {
		m_master.setStatus(m_connectionId, status);
	}

	private void setEnabled(boolean enabled) throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections SET " +
				"enabled = ? WHERE id = ?");

		ps.setBoolean(1, enabled);
		ps.setInt(2, m_connectionId);

		ps.execute();
		ps.close();
	}


	private String[] getRandomServer() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT server,port FROM ttirc_servers " +
				"WHERE connection_id = ? ORDER BY RANDOM()");

		ps.setInt(1, m_connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		String[] rv = new String[2];

		if (rs.next()) {
			rv[0] = rs.getString(1);
			rv[1] = rs.getString(2);
		}

		return rv;
	}

	private boolean connect() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT *, " +
			"ttirc_connections.nick AS local_nick, ttirc_users.nick AS normal_nick " +
			"FROM ttirc_connections, ttirc_users " +
			"WHERE ttirc_connections.id = ? AND owner_uid = ttirc_users.id");

		ps.setInt(1, m_connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		String nick;
		String localNick;
		String encoding;
		String email;
		String realname;
		String[] autojoin;
		String pass;
		boolean ssl;

		if (rs.next()) {
			nick = rs.getString("normal_nick");
			localNick = rs.getString("local_nick");
			encoding = rs.getString("encoding");
			email = rs.getString("email");
			realname = rs.getString("realname");
			autojoin = rs.getString("autojoin").split(",");
			//m_lastSentId = rs.getInt("last_sent_id");
			pass = rs.getString("server_password");
			ssl = rs.getBoolean("use_ssl");
			m_ownerUid = rs.getInt("owner_uid");

			// wtf? no nick?!
			if (nick.length() == 0) return false;

			if (localNick.length() > 0) nick = localNick;

			String[] server = getRandomServer();

			if (server.length != 2) {
				pushMessage("---", "---", "NOSERVER", Constants.MSGT_EVENT);

				PreparedStatement pst = m_conn.prepareStatement("UPDATE ttirc_connections SET "+
						"auto_connect = false, enabled = false WHERE id = ?");

				pst.setInt(1, m_connectionId);
				pst.execute();
				pst.close();

				return false;
			}

			ps.close();

			String host = server[0];
			int port = Integer.valueOf(server[1]);

			setStatus(Constants.CS_CONNECTING);

			pushMessage("---", "---", "CONNECTING:" + server[0] + ":" + server[1], Constants.MSGT_EVENT);

			ps = m_conn.prepareStatement("UPDATE ttirc_channels SET " +
					"nicklist = '' WHERE connection_id = ?");

			ps.setInt(1, m_connectionId);
			ps.execute();
			ps.close();

			updateLastChanmod();

			if (ssl)
				m_irc = new SSLIRCConnection(host, port, port, pass, nick, email, realname);
			else
				m_irc = new IRCConnection(host, port, port, pass, nick,	email, realname);

			m_irc.addIRCEventListener(new Listener(m_connectionId, this, autojoin));
			m_irc.setEncoding(encoding);
			m_irc.setPong(true);
			m_irc.setDaemon(false);
			m_irc.setColors(true);

			try {
				m_irc.connect();

				return true;
			} catch (IOException e) {
				e.printStackTrace();
				pushMessage("---", "---", "CONNECTION_ERROR:" + server[0] + ":" + server[1] + ":" + e.getMessage(),
						Constants.MSGT_EVENT);
				return false;
			}

		} else {
			return false;
		}

	}

	void pushMessage(String sender, String channel, String message, int messageType) {

			//logger.info("<<< " + sender + " " + channel + " " + message + " " + messageType);

			Long messageId = m_master.messageId();

			Map<String, Object> messageObject = new HashMap<>();

			messageObject.put("id", messageId);
			messageObject.put("sender", sender);
			messageObject.put("message", message);
			messageObject.put("message_type", messageType);
			messageObject.put("ts", System.currentTimeMillis() / 1000L);
			messageObject.put("channel", channel);
			messageObject.put("incoming", true);

			try {
				String messageString = JSONValue.toJSONString(messageObject);

				try (Jedis jedis = m_master.getJedis()) {
					jedis.rpush("ttirc_messages," + m_connectionId, messageString);
					jedis.set("ttirc_last_message," + m_ownerUid, String.valueOf(messageId));
					jedis.publish("ttirc_connection," + m_connectionId, "UPDATE");
					jedis.publish("ttirc_user," + m_ownerUid, "UPDATE");
				}

				/* Update m_extnickinfo for specified sender */
				m_extnickinfo.updateLastMessage(JSONValue.toJSONString(messageObject));
			} catch (Exception e) {
				logger.warning("[pushMessage error] " + channel + " " + message);
				e.printStackTrace();
			}
	}

	public void kill() {
		setActive(false);
	}

	private boolean lock() {
		File f = new File(m_lockDir + File.separator + LOCK_FILE_NAME.replace("%d", String.valueOf(m_connectionId)));

		try {
			m_lockChannel = new RandomAccessFile(f, "rw").getChannel();
			m_lock = m_lockChannel.tryLock();

			if (m_lock != null) {
				logger.info("[" + m_connectionId + "] Lock acquired successfully (" + f.getName() + ").");
			} else {
				logger.severe("[" + m_connectionId + "] Couldn't acquire the m_lock: maybe the connection is already m_active?");
				return false;
			}

		} catch (Exception e) {
			logger.severe("[" + m_connectionId + "] Couldn't make the m_lock: " + e.toString());
			e.printStackTrace();
			return false;
		}

		return true;
	}


	private String getQuitMessage() {
		try {
			PreparedStatement ps = m_conn.prepareStatement("SELECT quit_message FROM " +
					"ttirc_users, ttirc_connections WHERE " +
					"ttirc_users.id = owner_uid AND ttirc_connections.id = ?");

			ps.setInt(1, m_connectionId);
			ps.execute();

			ResultSet rs = ps.getResultSet();

			if (rs.next()) {
				return rs.getString("quit_message");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return "Tiny Tiny IRC";
	}

	public void handleCommand(String chan, String message) {
		String[] command = message.split(":", 2);

		if (command.length != 2) {
			logger.info("[" + m_connectionId + "] Incorrect command syntax: " + message);
			return;
		}

		//logger.info("COMMAND " + command[0] + "/" + command[1] + " on " + chan);

		if (command[0].equals("away")) {
			m_irc.doAway(command[1]);
			m_extnickinfo.setAwayReason(m_irc.getNick(), command[1]);
			return;
		}

		if (command[0].equals("quote")) {
			m_irc.send(command[1]);
			return;
		}

		if (command[0].equals("discon") || command[0].equals("disconnect")) {
			try {
				m_irc.doQuit(getQuitMessage());
				setEnabled(false);
			} catch (NullPointerException e) {
				//
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return;
		}

		if (command[0].equals("oper")) {
			m_irc.send("OPER " + command[1]);
			return;
		}

		if (command[0].equals("samode")) {
			m_irc.send("SAMODE " + command[1]);
			return;
		}

		if (command[0].equals("ping")) {
			m_irc.doPrivmsg(command[1], '\001' + "PING " + System.currentTimeMillis());
			return;
		}

		if (command[0].equals("msg")) {
			String[] msgparts = command[1].split(" ", 2);
			m_irc.doPrivmsg(msgparts[0], msgparts[1]);
			return;
		}

		if (command[0].equals("ctcp")) {
			String[] msgparts = command[1].split(" ", 2);
			m_irc.doPrivmsg(msgparts[0], '\001' + msgparts[1] + '\001');
			return;
		}

		if (command[0].equals("notice")) {
			String[] msgparts = command[1].split(" ", 2);
			m_irc.doNotice(msgparts[0], msgparts[1]);
			return;
		}

		if (command[0].equals("nick")) {
			m_irc.doNick(command[1]);
			return;
		}

		if (command[0].equals("whois")) {
			m_irc.doWhois(command[1]);
			return;
		}

		if (command[0].equals("join")) {
			m_irc.doJoin(command[1]);
			return;
		}

		if (command[0].equals("topic")) {
			m_irc.doTopic(chan, command[1]);
			return;
		}

		if (command[0].equals("part")) {
			m_irc.doPart(command[1]);
			return;
		}

		if (command[0].equals("action")) {
			m_irc.doPrivmsg(chan, "\001ACTION " + command[1] + "\001");
			return;
		}

		if (command[0].equals("mode")) {
			String[] msgparts = command[1].split(" ", 2);

			m_irc.doMode(msgparts[0], msgparts[1]);
			return;
		}

		if (command[0].equals("umode")) {
			m_irc.doMode(m_irc.getNick(), command[1]);
			return;
		}

		if (command[0].equals("op")) {
			m_irc.doMode(chan, "+o " + command[1]);
			return;
		}

		if (command[0].equals("deop")) {
			m_irc.doMode(chan, "-o " + command[1]);
			return;
		}

		if (command[0].equals("voice")) {
			m_irc.doMode(chan, "+v " + command[1]);
			return;
		}

		if (command[0].equals("devoice")) {
			m_irc.doMode(chan, "-v " + command[1]);
			return;
		}

		pushMessage("---", "---", "UNKNOWN_CMD:" + command[0], Constants.MSGT_EVENT);
	}

	private void checkMessages() {

		try {
			List<String> messages;
			//Long lastMessageId;

			try (Jedis jedis = m_master.getJedis()) {
				//lastMessageId = Long.valueOf(jedis.get("ttirc_last_message," + m_ownerUid));
				messages = jedis.lrange("ttirc_messages," + m_connectionId, -100, -1);
			}

			/* if (m_lastMessageId.equals(lastMessageId)) {
				return;
			}

			m_lastMessageId = lastMessageId; */

			if (messages != null) {
				Long tmpLastSentId = m_lastSentId;

				for (String messageString : messages) {
					Map<String, Object> messageObject = (Map<String, Object>) JSONValue.parse(messageString);

					if ("false".equals(messageObject.get("incoming"))) {
						Long messageId = (Long) messageObject.get("id");

						if (m_lastSentId == 0) {
							tmpLastSentId = messageId;
						}

						if (m_lastSentId > 0 && messageId > m_lastSentId) {
							//logger.info(">>> " + messageString);
							tmpLastSentId = messageId;

							// this is never > MAX_INT
							int messageType = ((Long)messageObject.get("message_type")).intValue();

							String channel = (String)messageObject.get("channel");
							String message = (String)messageObject.get("message");
							String[] lines;

							switch (messageType) {
								case Constants.MSGT_NOTICE:
									lines = splitByLength(message, 250);

									for (String line : lines)
										m_irc.doNotice(channel, line);

									break;
								case Constants.MSGT_PRIVMSG:
								case Constants.MSGT_PRIVATE_PRIVMSG:
									// TODO: figure out if it's an automatic away and unset only in this case, not sure if it's needed though

									if (m_extnickinfo.getAway(m_irc.getNick()))
										m_irc.doAway("");

									lines = splitByLength(message, 250);

									for (String line : lines)
										m_irc.doPrivmsg(channel, line);

									break;
								case Constants.MSGT_COMMAND:
									handleCommand(channel, message);
									break;
								default:
									logger.warning(m_connectionId + " received unknown MSG_TYPE: " + messageType);
							}

						}
					}
				}

				if (tmpLastSentId > 0) m_lastSentId = tmpLastSentId;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void disconnectIfDisabled() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT enabled, permanent, " +
			"(heartbeat > NOW() - INTERVAL '10 minutes') AS heartbeatCheck "+
            "FROM ttirc_connections, ttirc_users " +
            "WHERE owner_uid = ttirc_users.id AND visible = true AND " +
            "(heartbeat > NOW() - INTERVAL '10 minutes' OR permanent = true) AND " +
            "ttirc_connections.id = ?");

		ps.setInt(1, m_connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		if (rs.next()) {
			boolean enabled = rs.getBoolean("enabled");
			boolean permanent = rs.getBoolean("permanent");
			boolean heartbeatCheckResult = rs.getBoolean("heartbeatCheck");
			boolean isAway = m_extnickinfo.getAway(m_irc.getNick());

			// TODO: maybe add a frontend option for this [x] Automatically set away when disconnected

			if (permanent && !heartbeatCheckResult && !isAway) {
				pushMessage("---", "---", "AUTOAWAY_USER_IDLE", Constants.MSGT_EVENT);
				m_irc.doAway("Automatically set away: no clients connected.");
			}

			setActive(enabled);
		} else {
			pushMessage("---", "---", "DISCONNECT_USER_IDLE", Constants.MSGT_EVENT);

			logger.info("[" + m_connectionId + "] Disconnecting due to user inactivity.");
			setActive(false);
		}

		ps.close();
	}

	public void run() {
		try {
			m_conn = m_master.initConnection();

			if (!lock() || !connect()) {
				setActive(false);
			}

			long disconnectLastChecked = 0;

			while (m_active) {
				//logger.info("Last message received: " + (System.currentTimeMillis() - m_lastReceived));

				if (m_lastReceived > 0 && System.currentTimeMillis() - m_lastReceived > 5*60*1000) {

					pushMessage("---", "---", "DISCONNECT_PING_TIMEOUT",
							Constants.MSGT_EVENT);

					logger.info("Disconnecting from server, connection timeout.");
					setActive(false);
				}

				if (System.currentTimeMillis() - disconnectLastChecked > 3000) {
					disconnectIfDisabled();
					disconnectLastChecked = System.currentTimeMillis();
				}

				if (m_irc.isConnected()) {
					sendPing();
					periodicWho();
				} else {
					setActive(false);
				}

				sleep(1000L);
			}

		} catch (Exception e) {
			e.printStackTrace();

			pushMessage("---", "---", "CONNECTION_ERROR:" + m_irc.getHost() + ":" + m_irc.getPort() + ":" + e.getMessage(),
					Constants.MSGT_EVENT);

			logger.warning("[" + m_connectionId + "] Connection loop exception: " + e.toString());

			setActive(false);
		}

		cleanup();
		logger.warning("[" + m_connectionId + "] Terminated.");

	}

	private void periodicWho() {
		long timestamp = System.currentTimeMillis();

		if (timestamp - m_lastWhoSent > 60 * 1000) {
			if (m_irc != null && m_irc.isConnected()) {
				try {
					logger.info("[" + m_connectionId + "] Sending WHO to m_active channels [" + timestamp + "]");

					Vector<String> activeChans = getChannels();

					for (String chan : activeChans)
						m_irc.doWho(chan);
					} catch (Exception e) {
						e.printStackTrace();
					}

				m_lastWhoSent = timestamp;
			}
		}

	}

	private void sendPing() {
		long timestamp = System.currentTimeMillis();

		if (timestamp - m_lastPingSent > 2*60*1000) {
			if (m_irc != null && m_irc.isConnected()) {
				try {
					logger.info("[" + m_connectionId + "] Sending ping [" + timestamp + "]");
					m_irc.send("PING " + timestamp);
				} catch (Exception e) {
					e.printStackTrace();
				}

				m_lastPingSent = timestamp;
			}
		}
	}

	private void cleanup() {
		logger.info("[" + m_connectionId + "] Cleaning up...");

		if (m_irc != null) {
			if (m_irc.isConnected())
				m_irc.doQuit(getQuitMessage());

			m_irc.close();
		}

		try {
			m_lock.release();
			m_lockChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			m_subscriber.unsubscribe();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			setStatus(Constants.CS_DISCONNECTED);

			PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections " +
					"SET active_server = '' " +
					"WHERE id = ?");

			ps.setInt(1, m_connectionId);
			ps.execute();
			ps.close();

			ps = m_conn.prepareStatement("UPDATE ttirc_channels SET nicklist = '', topic = '' " +
					"WHERE connection_id = ?");

			ps.setInt(1, m_connectionId);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		updateLastChanmod();

		pushMessage("---", "DISCONNECT", "", Constants.MSGT_EVENT);

		try {
			m_conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void syncTopic(String channel, String owner, String topic, long topicSet) throws SQLException {

		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_channels SET " +
				"topic = ?, topic_owner = ?, topic_set = ? WHERE LOWER(channel) = LOWER(?) AND connection_id = ?");

		ps.setString(1, topic);
		ps.setString(2, owner);
		ps.setTimestamp(3, new java.sql.Timestamp(topicSet * 1000));
		ps.setString(4, channel);
		ps.setInt(5, m_connectionId);

		ps.execute();
		ps.close();

		updateLastChanmod();
	}

	private void syncTopic(String channel, String topic) throws SQLException {

		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_channels SET " +
			"topic = ? WHERE LOWER(channel) = LOWER(?) AND connection_id = ?");

		ps.setString(1, topic);
		ps.setString(2, channel);
		ps.setInt(3, m_connectionId);

		ps.execute();
		ps.close();

		updateLastChanmod();
	}

	private void syncTopic(String channel, String owner, long topicSet) throws SQLException {

		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_channels SET " +
			"topic_owner = ?, topic_set = ? WHERE LOWER(channel) = LOWER(?) AND connection_id = ?");

		ps.setString(1, owner);
		ps.setTimestamp(2, new java.sql.Timestamp(topicSet * 1000));
		ps.setString(3, channel);
		ps.setInt(4, m_connectionId);

		ps.execute();
		ps.close();

		updateLastChanmod();
	}

	private void checkChannel(String channel, int chanType) throws SQLException {

		PreparedStatement ps = m_conn.prepareStatement("SELECT id FROM ttirc_channels WHERE " +
			"LOWER(channel) = LOWER(?) AND connection_id = ?");

		ps.setString(1, channel);
		ps.setInt(2, m_connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		if (!rs.next()) {
			ps.close();

			ps = m_conn.prepareStatement("INSERT INTO ttirc_channels (channel, connection_id, chan_type, topic, topic_owner, topic_set, nicklist)" +
					"VALUES (?, ?, ?, '', '', NOW(), '')");
			ps.setString(1, channel);
			ps.setInt(2, m_connectionId);
			ps.setInt(3, chanType);
			ps.execute();
			ps.close();

			updateLastChanmod();
		}
	}

	protected void updateLastChanmod() {
		try (Jedis jedis = m_master.getJedis()) {
			jedis.incr("ttirc_last_chanmod," + m_ownerUid);
		}
	}

	private void removeChannel(String channel) throws SQLException {

		PreparedStatement ps = m_conn.prepareStatement("DELETE FROM ttirc_channels WHERE " +
				"LOWER(channel) = LOWER(?) AND connection_id = ?");

		ps.setString(1, channel);
		ps.setInt(2, m_connectionId);
		ps.execute();
		ps.close();

		updateLastChanmod();
	}

	private Vector<String> getChannels() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT channel FROM ttirc_channels WHERE " +
				"chan_type = ? AND connection_id = ?");

		ps.setInt(1, Constants.CT_CHANNEL);
		ps.setInt(2, m_connectionId);
		ps.execute();

		Vector<String> tmp = new Vector<String>();
		ResultSet rs = ps.getResultSet();

		while (rs.next()) {
			tmp.add(rs.getString(1));
		}

		return tmp;
	}

	private void syncNick() {
		try {
			PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections " +
				"SET active_nick = ? " +
				"WHERE id = ?");

			ps.setString(1, m_irc.getNick());
			ps.setInt(2, m_connectionId);
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		updateLastChanmod();
	}

	private void syncServer() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections " +
			"SET active_server = ? " +
			"WHERE id = ?");

		ps.setString(1, m_irc.getHost() + ":" + m_irc.getPort());
		ps.setInt(2, m_connectionId);
		ps.execute();
		ps.close();
	}

	public class Listener implements IRCEventListener {

		NativeConnectionHandler handler;
		int connectionId;
		String[] m_autojoin;

		Listener(int connectionId, NativeConnectionHandler handler, String[] autojoin) {
			this.connectionId = connectionId;
			this.handler = handler;
			m_autojoin = autojoin;
		}

		@Override
		public void onDisconnected() {
			handler.setActive(false);
		}

		@Override
		public void onError(String msg) {
			handler.pushMessage("---", "---", "ERROR:" + msg, Constants.MSGT_EVENT);
		}

		@Override
		public void onError(int num, String msg) {
			handler.pushMessage("---", "---", "ERROR:" + num + ":" + msg, Constants.MSGT_EVENT);

			if (num == 433) {
				handler.m_irc.doNick(handler.m_irc.getNick() + "-");
			}
		}

		@Override
		public void onInvite(String chan, IRCUser user, String passiveNick) {
			handler.pushMessage(user.getNick(), "---",
					"INVITE:" + chan, Constants.MSGT_EVENT);

		}

		@Override
		public void onJoin(String chan, IRCUser user) {
			handler.pushMessage(user.getNick(), chan,
					"JOIN:" + user.getNick() + ":" + user.getUsername() + "@" + user.getHost(),
					Constants.MSGT_EVENT);

			if (user.getNick().equals(m_irc.getNick())) {
				try {
					handler.checkChannel(chan, Constants.CT_CHANNEL);
					m_irc.doWho(chan);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			handler.m_nicklist.addNick(chan, user.getNick());
		}

		@Override
		public void onKick(String chan, IRCUser user, String passiveNick, String msg) {

			if (passiveNick.equals(m_irc.getNick())) {

				handler.pushMessage(user.getNick(), "---",
						"KICK:" + passiveNick + ":" + msg + ":" + chan,
						Constants.MSGT_EVENT);
				try {
					handler.removeChannel(chan);
				} catch (SQLException e) {
					e.printStackTrace();
				}

			} else {
				handler.pushMessage(user.getNick(), chan, "KICK:" + passiveNick + ":" + msg,
					Constants.MSGT_EVENT);

				handler.m_nicklist.removeNick(chan, passiveNick);
			}
		}

		@Override
		public void onMode(String chan, IRCUser user, IRCModeParser modeParser) {
			handler.pushMessage(user.getNick(), chan,
					"MODE:" + modeParser.getLine().trim() + ":" + chan,
					Constants.MSGT_EVENT);

			for (int i = 0; i < modeParser.getCount(); i++) {
				char mode = modeParser.getModeAt(i+1);
				char op = modeParser.getOperatorAt(i+1);

				String nick;

				switch (mode) {
				case 'o':
					nick = modeParser.getArgAt(i+1);
					handler.m_nicklist.setOp(chan, nick, op != '-');
					break;
				case 'v':
					nick = modeParser.getArgAt(i+1);
					handler.m_nicklist.setVoiced(chan, nick, op != '-');
					break;
				}
			}
		}

		@Override
		public void onMode(IRCUser user, String passiveNick, String mode) {
			handler.pushMessage(user.getNick(), "---",
					"MODE:" + mode + ":" + passiveNick,
					Constants.MSGT_EVENT);
		}

		@Override
		public void onNick(IRCUser user, String nick) {
			handler.m_nicklist.renameNick(user.getNick(), nick);
			handler.syncNick();
		}

		@Override
		public void onNotice(String target, IRCUser user, String msg) {

			// we got notice before registering, discard it
			if (user.getNick() == null) return;


			// CTCP
			if (msg.indexOf('\001') == 0) {
				msg = msg.substring(1, msg.length()-1);

				String[] ctcpParts = msg.split(" ", 2);

				if (ctcpParts.length == 2)
					onCtcpReply(target, user, ctcpParts[0].toUpperCase(), ctcpParts[1]);
				else
					onCtcpReply(target, user, ctcpParts[0].toUpperCase(), "");

			} else {
				if (target.equals(m_irc.getNick()) || target.equals("AUTH")) {

					// server notice
					if (user.getNick().equals(m_irc.getHost())) {
						handler.pushMessage(user.getNick(), "---", msg, Constants.MSGT_NOTICE);
					} else {
						try {
							handler.checkChannel(user.getNick(), Constants.CT_PRIVATE);
							handler.pushMessage(user.getNick(), user.getNick(), msg, Constants.MSGT_NOTICE);
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}

				} else {
					handler.pushMessage(user.getNick(), target, msg, Constants.MSGT_NOTICE);
				}
			}
		}

		@Override
		public void onPart(String chan, IRCUser user, String msg) {
			handler.m_nicklist.removeNick(chan, user.getNick());

			if (user.getNick().equals(m_irc.getNick())) {
				try {
					handler.removeChannel(chan);
					handler.m_nicklist.removeChannel(chan);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

			handler.pushMessage("---", chan, "PART:" + user.getNick() + ":" + msg, Constants.MSGT_EVENT);
		}

		@Override
		public void onPing(String arg0) {
			// TODO Auto-generated method stub
		}

		void onCtcpReply(String target, IRCUser user, String command, String msg) {
			//System.out.println("CTCP reply target: " + target + " CMD: [" + command + "] MSG: " + msg);

			if (command.equals("PING")) {
				float pingInterval = (float)(System.currentTimeMillis() - Long.parseLong(msg)) / 1000;

				handler.pushMessage(user.getNick(), target, String.format("PING_REPLY:%.2f", pingInterval), Constants.MSGT_EVENT);
				return;
			}

			if (target.equals(m_irc.getNick())) {

				try {
					handler.checkChannel(user.getNick(), Constants.CT_PRIVATE);
					handler.pushMessage(user.getNick(), user.getNick(), "CTCP_REPLY:" + command + ":" + msg, Constants.MSGT_EVENT);
				} catch (SQLException e) {
					e.printStackTrace();
				}

			} else {
				handler.pushMessage(user.getNick(), target, "CTCP_REPLY:" + command + " " + msg, Constants.MSGT_EVENT);
			}

		}

		void onCtcp(String target, IRCUser user, String command, String msg) {
			//System.out.println("CTCP target: " + target + " CMD: [" + command + "] MSG: " + msg);
			m_lastReceived = System.currentTimeMillis();

			if (command.equals("ACTION")) {
				if (target.equals(handler.m_irc.getNick())) {
					try {
						handler.checkChannel(user.getNick(), Constants.CT_PRIVATE);
						handler.pushMessage(user.getNick(), user.getNick(), msg, Constants.MSGT_ACTION);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					handler.pushMessage(user.getNick(), target, msg, Constants.MSGT_ACTION);
				}
				return;
			}

			if (command.equals("PING")) {
				m_irc.doNotice(user.getNick(), '\001' + command + ' ' + msg + '\001');
			}

			if (command.equals("VERSION")) {
				String version = handler.m_master.getVersion();
				String osName= System.getProperty("os.name");
				String osArch = System.getProperty("os.arch");

				String versionReply = "Tiny Tiny IRC/" + version + " " + osName + " " + osArch;

				m_irc.doNotice(user.getNick(), '\001' + command + ' ' + versionReply + '\001');
			}

			if (target.equals(handler.m_irc.getNick())) {
				handler.pushMessage(user.getNick(), "---", "CTCP:" + command + ":" + msg, Constants.MSGT_EVENT);
			} else {
				handler.pushMessage(user.getNick(), target, "CTCP:" + command + ":" + msg, Constants.MSGT_EVENT);
			}
		}

		@Override
		public void onPrivmsg(String target, IRCUser user, String msg) {
			m_lastReceived = System.currentTimeMillis();

			// CTCP
			if (msg.indexOf('\001') == 0) {
				msg = msg.substring(1, msg.length()-1);

				String[] ctcpParts = msg.split(" ", 2);

				if (ctcpParts.length == 2)
					onCtcp(target, user, ctcpParts[0].toUpperCase(), ctcpParts[1]);
				else
					onCtcp(target, user, ctcpParts[0].toUpperCase(), "");

			} else {
				try {
					if (target.equals(handler.m_irc.getNick())) {
						handler.checkChannel(user.getNick(), Constants.CT_PRIVATE);
						handler.pushMessage(user.getNick(), user.getNick(), msg, Constants.MSGT_PRIVATE_PRIVMSG);
					} else {
						handler.pushMessage(user.getNick(), target, msg, Constants.MSGT_PRIVMSG);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void onQuit(IRCUser user, String message) {
			Vector<String> chans = handler.m_nicklist.isOn(user.getNick());

			for (String chan : chans) {
				handler.pushMessage(user.getNick(), chan, "QUIT:" + message, Constants.MSGT_EVENT);
			}

			handler.m_nicklist.removeNick(user.getNick());
			handler.m_extnickinfo.remove(user.getNick());
		}

		// TODO: better implementation
		String parseCommand(String rawCmd) {
			rawCmd = rawCmd.substring(1);
			rawCmd = rawCmd.replace(' ', ':');
			return rawCmd;
		}

		@Override
		public void onRegistered() {

			handler.logger.info("[" + connectionId + "] Connected to IRC.");

			try {
				handler.setStatus(Constants.CS_CONNECTED);
				handler.syncNick();
				handler.syncServer();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			handler.pushMessage("---", "---", "CONNECT", Constants.MSGT_EVENT);

			/* Autojoin channels */
			for (String chan : m_autojoin) {
				handler.m_irc.doJoin(chan);
			}

			try {
				Vector<String> activeChans = handler.getChannels();

				for (String chan : activeChans)
					handler.m_irc.doJoin(chan);

				String[] connectCmd = handler.getConnectCmd();

				for (String cmd : connectCmd)
					if (cmd.length() > 0)
							handleCommand("", parseCommand(cmd));

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		@Override
		public void onReply(int num, String value, String msg) {
			m_lastReceived = System.currentTimeMillis();

			//System.out.println(num + " [" + value + "] " + msg);

			if (num == RPL_TOPIC) {
				String[] params = value.split(" ");

				try {
					handler.syncTopic(params[1], msg);
				} catch (SQLException e) {
					e.printStackTrace();
				}

				return;
			}

			if (num == RPL_TOPICINFO) {
				String[] params = value.split(" ");

				try {
					handler.syncTopic(params[1], params[2], Integer.parseInt(msg));
				} catch (Exception e) {
					e.printStackTrace();
				}

				return;
			}

			if (num == RPL_UNAWAY) {
				String nick = value;
				handler.m_extnickinfo.setAway(nick, false);

				if (nick.equals(m_irc.getNick())) {
					handler.pushMessage("---", "---", msg, Constants.MSGT_SYSTEM);
				}

				return;
			}

			if (num == RPL_NOWAWAY) {
				String nick = value;
				handler.m_extnickinfo.setAway(nick, true);

				if (nick.equals(m_irc.getNick())) {
					handler.pushMessage("---", "---", msg, Constants.MSGT_SYSTEM);
				}

				return;
			}

			if (num == 301) { /* AWAYREASON */
				//System.out.println("[AWAYREASON] " + value + "/" + msg);
				String[] nicks = value.split(" ");
				if (nicks.length == 2) {
					handler.m_extnickinfo.setAwayReason(nicks[1], msg);
				}

				return;
			}

			if (num == RPL_NAMREPLY) {
				String[] params = value.split(" ");
				String[] nicks = msg.split(" ");

				for (String nick : nicks) {
					m_nicklist.addNick(params[2], nick);
				}

				return;
			}

			if (num == RPL_ENDOFNAMES) {
				String[] params = value.split(" ");
				try {
					handler.checkChannel(params[1], Constants.CT_CHANNEL);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return;
			}

			if (num == RPL_WHOREPLY) {
				String[] params = value.split(" ");

				String realName = msg.substring(2);
				String nick = params[5];
				boolean isAway = params[6].indexOf('G') != -1;
				String ident = params[2];
				String host = params[3];
				String server = params[4];

				handler.m_extnickinfo.update(nick, ident, host, server, realName, isAway);

				return;
			}

			if (num == RPL_ENDOFWHO) {
				return;
			}

			handler.pushMessage(m_irc.getHost(), "---", num + " " + value + " " + msg, Constants.MSGT_SYSTEM);
		}

		@Override
		public void onTopic(String chan, IRCUser user, String topic) {
			try {
				int timestamp = (int)(System.currentTimeMillis() / 1000);

				handler.syncTopic(chan, user.getNick(), topic, timestamp);
				handler.pushMessage(user.getNick(), chan, "TOPIC:" + topic, Constants.MSGT_EVENT);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void unknown(String prefix, String command, String middle, String trailing) {
			m_lastReceived = System.currentTimeMillis();

			if (command.equals("PONG")) {
				//m_lastPingReceived = System.currentTimeMillis();

				float pingInterval = (float)(System.currentTimeMillis() - m_lastPingSent) / 1000;

				logger.info("[" + connectionId + "] Received server PONG, lag=" + pingInterval);

				handler.pushMessage("---", "---", "SERVER_PONG:" + pingInterval, Constants.MSGT_EVENT);

				return;
			}

			handler.pushMessage("---", "---", prefix + " " + command + " " + middle + " " + trailing,
				Constants.MSGT_SYSTEM);
		}
	}

	void syncExtInfo(String jsonValue) throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("UPDATE ttirc_connections SET userhosts = ? " +
				"WHERE id = ?");

		ps.setString(1, jsonValue);
		ps.setInt(2, m_connectionId);
		ps.execute();
		ps.close();
	}

	private String[] getConnectCmd() throws SQLException {
		PreparedStatement ps = m_conn.prepareStatement("SELECT connect_cmd FROM ttirc_connections " +
				"WHERE id = ?");

		ps.setInt(1, m_connectionId);
		ps.execute();

		ResultSet rs = ps.getResultSet();

		String[] rv = null;

		if (rs.next()) {
			rv = rs.getString("connect_cmd").split(";");
		}

		ps.close();

		return rv;
	}

	private String[] splitByLength(String str, int length) {
		int arrSize = (int) Math.ceil(str.length() / (double) length);

		String[] lines = new String[arrSize];

		if (str.length() > length) {
			int i = 0;
			while (str.length() > length) {
				lines[i] = str.substring(0, length);
				str = str.substring(length);
				++i;
			}

			if (str.length() > 0) lines[i] = str;
		} else {
			lines[0] = str;
		}

		return lines;
	}

	int getConnectionId() {
		return m_connectionId;
	}

	class JedisSubscriber extends JedisPubSub {
		@Override
		public void onMessage(String channel, String message) {
			checkMessages();
		}
	};
}
