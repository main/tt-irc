<?php
	define('CACHE_LIFETIME_MAX', 86400*1);

	define('PROXY_REQ_FAILED', 0);
	define('PROXY_REQ_PROGRESS', 1);
	define('PROXY_REQ_OK', 2);
	define('PING_INTERVAL', 20);

	define('PROXY_CONCUR_TRIES', 8); // seconds to wait before attempting another request

	set_include_path(get_include_path() . PATH_SEPARATOR .
		dirname(__FILE__) ."/include");

	ini_set('user_agent',
		'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');

	require_once "functions.php";
	require_once "sessions.php";
	require_once "db-prefs.php";
	require_once "sanity_check.php";
	require_once "version.php";
	require_once "config.php";
	require_once "prefs.php";
	require_once "users.php";

	global $redis;

	$dbh = DB::get();

	$dt_add = get_script_dt_add();

	$op = $_REQUEST["op"];

	header('Content-Type: text/json; charset=utf-8');

	if (!$_SESSION["uid"] || time() - $_SESSION["backend_sanity_check_last"] > 30) {

		if (!sanity_check())
			return;

		$_SESSION["backend_sanity_check_last"] = time();
	}

	if (!$_SESSION["uid"] && $op != "fetch-profiles" && $op != "login" && $op != "imgproxy") {
		print json_encode(array("error" => 6));
		return;
	} else if ($_SESSION["uid"]) {
		@$csrf_token = $_POST["csrf_token"];

		$csrf_ignore = [ "login", "urlmetadata", "imgproxy", "mediaredirect",
			"vidproxy", "emoticons", "emoticons_list", "logout", "embed", "evtsource" ];

		if (!in_array($op, $csrf_ignore) && !validate_csrf($csrf_token)) {
			print json_encode(array("error" => 6));
			return;
		}

		update_heartbeat();
	}

	switch ($op) {
	case "create-user":
		$login = strtolower($_REQUEST["login"]);
		$rv = array();

		if ($_SESSION["access_level"] >= 10) {

			$sth = $dbh->prepare("SELECT id FROM ttirc_users WHERE
				login = ?");
			$sth->execute([$login]);

			if (!$sth->fetch()) {
				$tmp_password = make_password();
				$salt = substr(bin2hex(get_random_bytes(125)), 0, 250);
				$pwd_hash = encrypt_password($tmp_password, $salt, true);

				$rv[0] = T_sprintf("Created user %s with password <b>%s</b>.",
					$login, $tmp_password);

				$sth = $dbh->prepare("INSERT INTO ttirc_users
					(login, pwd_hash, email, nick, realname, salt)
					VALUES
					(?, ?, ?, ?, ?, ?)");
				$sth->execute([$login, $pwd_hash, "$login@localhost", $login, $login, $salt]);

			} else {
				$rv[0] = T_sprintf("User %s already exists", $login);
			}

			$rv[1] = format_users();

			print json_encode($rv);
		}
		break;
	case "reset-password":
		$id = $_REQUEST["id"];

		if ($_SESSION["access_level"] >= 10) {
			$tmp_password = make_password();

			$login = get_user_login($id);

			$salt = substr(bin2hex(get_random_bytes(125)), 0, 250);
			$pwd_hash = encrypt_password($tmp_password, $salt, true);

			$sth = $dbh->prepare("UPDATE ttirc_users SET pwd_hash = ?, salt = ?
				WHERE id = ?");
			$sth->execute([$pwd_hash, $salt, $id]);

			print json_encode(array("message" =>
				T_sprintf("Reset password of user %s to <b>%s</b>.", $login,
					$tmp_password)));
		}

		break;
	case "delete-user":
		if ($_SESSION["access_level"] >= 10) {
			$ids = explode(",", $_REQUEST["ids"]);
			$ids_qmarks = arr_qmarks($ids);

			$sth = $dbh->prepare("DELETE FROM ttirc_users WHERE
				id in ($ids_qmarks) AND id != ?");
			$sth->execute(array_merge($ids, [$_SESSION["uid"]]));

			print format_users();
		}
		break;
	case "users":
		if ($_SESSION["access_level"] >= 10) {
			show_users();
		}
		break;
	case "part-channel":
		$last_id = (int) $_REQUEST["last_id"];
		$chan = $_REQUEST["chan"];
		$connection_id = $_REQUEST["connection"];

		if ($chan && valid_connection($connection_id)) {
			handle_command($connection_id, $chan, "/part");

			$sth = $dbh->prepare("DELETE FROM ttirc_channels WHERE LOWER(channel) = LOWER(?)
				AND connection_id = ?");
			$sth->execute([$chan, $connection_id]);
		}

		$lines = get_new_lines($last_id);
		$conn = get_conn_info();
		$chandata = get_chan_data();
		$params = get_misc_params();

		print json_encode(array($conn, $lines, $chandata, $params));
		break;
	case "query-user":
		$nick = trim($_REQUEST["nick"]);
		$last_id = (int) $_REQUEST["last_id"];
		$connection_id = $_REQUEST["connection"];

		if ($nick && valid_connection($connection_id)) {
			handle_command($connection_id, "", "/query $nick");
		}

		$lines = get_new_lines($last_id);
		$conn = get_conn_info();
		$chandata = get_chan_data();
		$params = get_misc_params();

		print json_encode(array($conn, $lines, $chandata, $params));
		break;

	case "send":
		$message = trim($_REQUEST["message"]);
		$last_id = (int) $_REQUEST["last_id"];
		$chan = $_REQUEST["chan"];
		$connection_id = $_REQUEST["connection"];
		$tab_type = $_REQUEST["tab_type"];
		@$uniqid = $_REQUEST["uniqid"];
		@$sender_kind = $_REQUEST["kind"];

		if (!$sender_kind)
			$sender_kind = "mobile";

		if ($message !== "" && valid_connection($connection_id)) {
			if (mb_strpos($message, "/") === 0) {
				handle_command($connection_id, $chan, $message);
			} else {

				$popcon_matches = array();
				preg_match_all("/(:[^ :]+:)/", $message, $popcon_matches);

				$emoticons_map = get_emoticons_map();

				if ($emoticons_map && count($popcon_matches[0]) > 0) {
					foreach ($popcon_matches[0] as $emoticon) {
						if (isset($emoticons_map[$emoticon])) {

							$sth = $dbh->prepare("SELECT id, times_used FROM ttirc_emoticons_popcon
								WHERE emoticon = ? AND owner_uid = ?");
							$sth->execute([$emoticon, $_SESSION["uid"]]);

							if ($row = $sth->fetch()) {
								$ref_id = $row['id'];
								$times_used = $row['times_used'];

								$sth = $dbh->prepare("UPDATE ttirc_emoticons_popcon SET times_used = times_used + 1
									WHERE id = ?");
								$sth->execute([$ref_id]);

							} else {
								$sth = $dbh->prepare("INSERT INTO ttirc_emoticons_popcon (emoticon, times_used, owner_uid)
									VALUES (?, 1, ?)");
								$sth->execute([$emoticon, $_SESSION['uid']]);
							}
						}
					}
				}

				update_last_message();

				if (is_instance($connection_id)) {

					#$lines = array_map("trim", explode("\n", $message));

					// this is a hard cap for incoming message length
					$message = mb_substr($message, 0, 32768);

					relay_message($connection_id, $chan, $message, MSGT_PRIVMSG, false, $sender_kind);

				} else {

					$lines = array_map("trim", explode("\n", $message));

					if ($tab_type == "P") {
						foreach ($lines as $line)
							if (mb_strlen($line) > 0)
								push_message($connection_id, $chan, $line, false,
									MSGT_PRIVATE_PRIVMSG);
					} else {
						$l = 0;

						foreach ($lines as $line) {
							if (mb_strlen($line) > 0) {
								push_message($connection_id, $chan, $line);
								++$l;

								if ($l > 4) break;
							}
						}
					}
				}

/*				$lines = explode("\n", wordwrap($message, 200, "\n"));

				foreach ($lines as $line) {
					push_message($connection_id, $chan, $line);
				} */
			}
		}

		$dup = [ "duplicate" => true ];
		print json_encode([$dup, [], $dup, $dup]);

		break;

	case "evtsource":
		cleanup_session_cache();

		header('Cache-Control: no-cache');
		header("Content-Type: text/event-stream");
		header('X-Accel-Buffering: no'); // nginx specific; disable cache for evtsource

		global $redis;

		@$bufsize = (int) $_REQUEST["bufsize"];
		@$uniqid = $_REQUEST["uniqid"];
		@$last_id = (int) $_REQUEST["last_id"];

		// if reconnecting, get last id from automatically provided header
		if (isset($_SERVER["HTTP_LAST_EVENT_ID"]))
			$last_id = (int) $_SERVER["HTTP_LAST_EVENT_ID"];

		if (!$bufsize) $bufsize = DEFAULT_BUFFER_SIZE;

		ob_end_clean();

		$started = time();
		while (time() - $started <= UPDATE_DELAY_MAX) {

			$prev_chanmod = 0;
			$prev_misc_params = 0;

			// if stream was restarted with new events pending, we shouldn't wait
			if (!has_new_lines($last_id)) {
				try {
					// we need to use a separate redis client to wait on subscription for some reason
					$sredis = new Credis_Client(REDIS_SERVER);

					$sredis->setReadTimeout(PING_INTERVAL);
					$sredis->subscribe("ttirc_user," . $_SESSION["uid"], function($sredis, $channel, $message)
																								use ($redis, &$last_id, $uniqid, $bufsize, &$prev_chanmod, &$prev_misc_params) {

						$sender_uniqid = false;

						/* if (preg_match("/;SENDER=(.*)/", $message, $matches)) {
							$sender_uniqid = $matches[1];
						} */

						if ($matches = explode(";SENDER=", $message)) {
							if (count($matches) > 1) $sender_uniqid = $matches[1];
						}

						$lines = get_new_lines($last_id, $bufsize);

						if (count($lines) == 0 && $sender_uniqid == $uniqid)
							return;

						print "event: update\n";
						print "retry: 1000\n";

						$conn = get_conn_info();

						$last_chanmod = $redis->get("ttirc_last_chanmod," . $_SESSION["uid"]);

						if ($last_chanmod && $prev_chanmod != $last_chanmod) {
							$chandata = get_chan_data();
							$prev_chanmod = $last_chanmod;
						} else {
							$chandata = ["duplicate" => true];
						}

						if (time() - $prev_misc_params > 60) {
							$params = get_misc_params($uniqid);
							$prev_misc_params = time();
						} else {
							$params = ["duplicate" => true];
						}

						foreach ($lines as $line)
							if ($line['id'] > $last_id)
								$last_id = $line['id'];

						if ($uniqid)
							mark_duplicate_objects($uniqid, $conn, $chandata, $params);

						print "id: $last_id\n";
						print "data:" . json_encode(array($conn, $lines, $chandata, $params));
						print "\n\n";

						flush();
					});
				} catch (CredisException $e) {
					print "event: ping\n";
					print "retry: 1000\n";
					print "id: $last_id\n";
					print "data:" . json_encode(["ts" => time()]);
					print "\n\n";

					flush();
				}
			}

			// what if there were no listeners ready when updates happened
			if (has_new_lines($last_id)) {
				print "event: update\n";
				print "retry: 1000\n";

				$lines = get_new_lines($last_id, $bufsize);
				$conn = get_conn_info();
				$chandata = get_chan_data();
				$params = get_misc_params($uniqid);

				foreach ($lines as $line)
					if ($line['id'] > $last_id)
						$last_id = $line['id'];

				if ($uniqid)
					mark_duplicate_objects($uniqid, $conn, $chandata, $params);

				print "id: $last_id\n";
				print "data:" . json_encode(array($conn, $lines, $chandata, $params));
				print "\n\n";

				flush();
			}
		}

		break;
	case "update":
		cleanup_session_cache();

		$last_id = (int) $_REQUEST["last_id"];
		$init = $_REQUEST["init"];
		@$bufsize = (int) $_REQUEST["bufsize"];
		@$uniqid = $_REQUEST["uniqid"];
		@$update_delay = isset($_REQUEST["update_delay"]) ? (int) $_REQUEST["update_delay"] : UPDATE_DELAY_MAX;

		if ($update_delay > UPDATE_DELAY_MAX)
			$update_delay = UPDATE_DELAY_MAX;

		if (!$bufsize) $bufsize = DEFAULT_BUFFER_SIZE;

		if (!$init && !has_new_lines($last_id)) {
			try {
				// we need to use a separate redis client to wait on subscription for some reason
				$sredis = new Credis_Client(REDIS_SERVER);

				$sredis->setReadTimeout(5);
				$sredis->subscribe("ttirc_user," . $_SESSION["uid"], function($sredis, $channel, $message) use ($init, $uniqid, $last_id, $bufsize) {

					$lines = get_new_lines($last_id, $bufsize);
					$conn = get_conn_info();
					$chandata = get_chan_data();
					$params = $init ? get_misc_params($uniqid) : ['duplicate' => true];

					if ($uniqid)
						mark_duplicate_objects($uniqid, $conn, $chandata, $params);

					print json_encode(array($conn, $lines, $chandata, $params));
					exit; // only one event per cycle

				});
			} catch (CredisException $e) {
				//
			}
		}

		$lines = get_new_lines($last_id, $bufsize);
		$conn = get_conn_info();
		$chandata = get_chan_data();
		$params = $init ? get_misc_params($uniqid) : ['duplicate' => true];

		if ($uniqid)
			mark_duplicate_objects($uniqid, $conn, $chandata, $params);

		print json_encode(array($conn, $lines, $chandata, $params));
		break;

	case "history":
		$chan = $_REQUEST["chan"];
		$connection_id = $_REQUEST["connection"];
		$offset = (int)$_REQUEST["offset"];

		$lines = get_history_lines($connection_id, $chan, $offset);

		$dup = [ "duplicate" => true ];

		print json_encode([$dup, $lines, $dup, $dup]);
		break;

	case "set-topic":
		$last_id = (int) $_REQUEST["last_id"];
		$topic = $_REQUEST["topic"];
		$chan = $_REQUEST["chan"];
		$connection_id = $_REQUEST["connection"];

		if ($topic !== false) {
			handle_command($connection_id, $chan, "/topic $topic");
		}

		$lines = get_new_lines($last_id);
		$conn = get_conn_info();
		$chandata = get_chan_data();
		$params = get_misc_params();

		print json_encode(array($conn, $lines, $chandata, $params));

		break;

	case "login":
		$login = $_REQUEST["user"];
		$password = $_REQUEST["password"];
		@$mobile = (int)$_REQUEST["mobile"];

		if (authenticate_user($login, $password)) {
			$_SESSION["csrf_token"] = bin2hex(get_random_bytes(16));
			$_SESSION["mobile"] = $mobile;

			print json_encode(array("sid" => session_id(), "version" => VERSION,
				"uniqid" => uniqid(), "csrf_token" => $_SESSION["csrf_token"]));
		} else {
			print json_encode(array("error" => 6));
		}

		break;

	case "init":

		$dbh = DB::get();

		$rv = [];

		$rv["max_id"] = $redis->get("ttirc_last_message_id");
		$rv["status"] = 1;
		$rv["theme"] = get_pref("USER_THEME");
		$rv["update_delay_max"] = UPDATE_DELAY_MAX;
		$rv["ping_interval"] = PING_INTERVAL;
		$rv["uniqid"] = uniqid();
		$rv["emoticons"] = [];
		$rv["emoticons_mtime"] = is_readable(EMOTICONS_MAP) ? filemtime(EMOTICONS_MAP) : -1;
		$rv["emoticons_favorite"] = get_favorite_emoticons();
		$rv["default_buffer_size"] = DEFAULT_BUFFER_SIZE;

		print json_encode($rv);

		break;
	case "prefs":
		main_prefs();
		break;
	case "prefs-conn-save":
		$title = $_REQUEST["title"];
		$autojoin = $_REQUEST["autojoin"];
		$connect_cmd = $_REQUEST["connect_cmd"];
		$encoding = $_REQUEST["encoding"];
		$nick = $_REQUEST["nick"];
		$auto_connect = bool_to_sql_bool($_REQUEST["auto_connect"]);
		$permanent = bool_to_sql_bool($_REQUEST["permanent"]);
		$connection_id = $_REQUEST["connection_id"];
		$visible = bool_to_sql_bool($_REQUEST["visible"]);
		$server_password = $_REQUEST["server_password"];
		$use_ssl = bool_to_sql_bool($_REQUEST["use_ssl"]);

		if (!$title) $title = __("[Untitled]");

		if (valid_connection($connection_id)) {

			$sth = $dbh->prepare("UPDATE ttirc_connections SET title = ?,
				autojoin = ?,
				connect_cmd = ?,
				auto_connect = ?,
				server_password = ?,
				visible = ?,
				use_ssl = ?,
				nick = ?,
				encoding = ?,
				permanent = ?
				WHERE id = ?");

			$sth->execute([$title, $autojoin, $connect_cmd, $auto_connect, $server_password,
				$visible, $use_ssl, $nick, $encoding, $permanent, $connection_id]);

			//print json_encode(array("error" => "Function not implemented."));
		}
		print json_encode(["status" => "OK"]);
		break;

	case "prefs-save":
		//print json_encode(array("error" => "Function not implemented."));

		$realname = $_REQUEST["realname"];
		$quit_message = $_REQUEST["quit_message"];
		$new_password = $_REQUEST["new_password"];
		$confirm_password = $_REQUEST["confirm_password"];
		$nick = $_REQUEST["nick"];
		$email = $_REQUEST["email"];
		$theme = $_REQUEST["theme"];
		$highlight_on = $_REQUEST["highlight_on"];
		$hide_join_part = bool_to_sql_bool($_REQUEST["hide_join_part"]);
		$disable_image_preview = bool_to_sql_bool($_REQUEST["disable_image_preview"]);

		$theme_changed = false;

		$_SESSION["prefs_cache"] = false;

		if (get_user_theme() != $theme) {
			set_pref("USER_THEME", $theme);
			$theme_changed = true;
		}

		set_pref("HIGHLIGHT_ON", $highlight_on);
		set_pref("DISABLE_IMAGE_PREVIEW", $disable_image_preview);

		$sth = $dbh->prepare("UPDATE ttirc_users SET realname = ?,
			quit_message = ?,
			email = ?,
			hide_join_part = ?,
			nick = ? WHERE id = ?");

		$sth->execute([$realname, $quit_message, $email, $hide_join_part, $nick, $_SESSION['uid']]);

		if ($new_password != $confirm_password) {
			print json_encode(array("error" => "Passwords do not match."));
			return;
		}

		if ($confirm_password == $new_password && $new_password) {
			$salt = substr(bin2hex(get_random_bytes(125)), 0, 250);
			$pwd_hash =  encrypt_password($new_password, $salt, true);

			$sth = $dbh->prepare("UPDATE ttirc_users SET pwd_hash = ?, salt = ?
				WHERE id = ?");
			$sth->execute([$pwd_hash, $salt, $_SESSION['uid']]);
		}

		if ($theme_changed) {
			print json_encode(array("message" => "THEME_CHANGED"));
			return;
		}
		break;
	case "prefs-edit-con":
		$connection_id = (int) $_REQUEST["id"];
		connection_editor($connection_id);
		break;
	case "prefs-customize-css":
		css_editor();
		break;
	case "prefs-save-css":
		$user_css = $_REQUEST["user_css"];

		set_pref("USER_STYLESHEET", $user_css);

		print json_encode(["status" => "OK"]);
		//print json_encode(array("error" => "Function not implemented."));
		break;
	case "create-server":
		$connection_id = (int) $_REQUEST["connection_id"];
		list($server, $port) = explode(":", $_REQUEST["data"]);

		if (valid_connection($connection_id)) {
			if ($server && $port) {
				$sth = $dbh->prepare("INSERT INTO ttirc_servers (server, port, connection_id)
					VALUES (?, ?, ?)");
				$sth->execute([$server, $port, $connection_id]);

				print_servers($connection_id);

			} else {

				$error = T_sprintf("Couldn't add server (%s:%d): Invalid syntax.",
					$server, $port);

				print json_encode(array("error" => $error));
			}
		}

		break;

	case "delete-server":
		$ids = explode(",", $_REQUEST["ids"]);
		$connection_id = (int) $_REQUEST["connection_id"];

		if (valid_connection($connection_id)) {
			$ids_qmarks = arr_qmarks($ids);

			$sth = $dbh->prepare("DELETE FROM ttirc_servers WHERE
				id in ($ids_qmarks) AND connection_id = ?");
			$sth->execute(array_merge($ids, [$connection_id]));

			print_servers($connection_id);
		}
		break;

	case "delete-connection":
		$ids = explode(",", $_REQUEST["ids"]);
		$ids_qmarks = arr_qmarks($ids);

		$sth = $dbh->prepare("DELETE FROM ttirc_connections WHERE
			id IN ($ids_qmarks) AND status = 0 AND owner_uid = ?");
		$sth = $sth->execute(array_merge($ids, [$_SESSION['uid']]));

		print_connections();

		break;
	case "create-connection":
		$title = trim(str_replace("_ttirc_instance", "", $_REQUEST["title"]));

		if ($title) {
			$sth = $dbh->prepare( "INSERT INTO ttirc_connections (enabled, title, owner_uid)
				VALUES (false, ?, ?)");
			$sth->execute([$title, $_SESSION['uid']]);
		}

		print_connections();
		break;

	case "toggle-connection":
		$connection_id = (int) $_REQUEST["connection_id"];
		$status = bool_to_sql_bool($_REQUEST["set_enabled"]);
		$server = get_random_server($connection_id);

		if (!$server) $status = 0;

		$sth = $dbh->prepare("UPDATE ttirc_connections SET enabled = ?
			WHERE id = ? AND owner_uid = ?");
		$sth->execute([$status, $connection_id, $_SESSION['uid']]);

		print json_encode(array("status" => $status));
		break;

	case "prefs-edit-notify":
		notification_editor();

		break;

	case "prefs-save-notify":
		$notify_events = json_encode($_REQUEST["notify_event"]);

		set_pref("NOTIFY_ON", $notify_events);

		print json_encode(["status" => "OK"]);
		break;

	case "preview":
		$url = htmlspecialchars($_REQUEST["url"]);

		header("Location: $url");

		break;

	case "emoticons":
		@$modified = (int)$_REQUEST['modified'];

		if (file_exists(EMOTICONS_MAP) &&
			(!$modified || $modified < filemtime(EMOTICONS_MAP))) {

			header("X-Map-Last-Modified: " . filemtime(EMOTICONS_MAP));

			print file_get_contents(EMOTICONS_MAP);
		} else {
			print json_encode([]);
		}

		break;

	case "emoticons_list":
		header('Content-Type: text/html; charset=utf-8');

		render_emoticons_full();
		break;

	case "embed":
		$url = htmlspecialchars($_REQUEST["url"]);

		header("Content-Type: text/html");

		print "<html><head>
			<title>Tiny Tiny IRC: $url</title>
			<style type='text/css'>
			body { background : #333;
				margin : 0px; padding : 0px;
				text-align : center;
				height : 100%; width : 100%; }
			video, img.fit { max-width : 100%; max-height : 100%;
				width : auto; height : auto;
				position : relative; top : 50%; transform: translateY(-50%); }
			img {
				cursor : pointer;
			}
			</style></head><body>";

		print javascript_tag("lib/bootstrap/js/jquery.js");

		if (preg_match("/\.(mp4|webm|gifv)/", $url, $matches)) {
			$type = $matches[1];
			$embed_url = $url;

			if ($type == "gifv") {
				$type = "mp4";
				$embed_url = str_replace(".gifv", ".mp4", $embed_url);
			}

			header("Content-type: text/html");

			print "<video class=\"\" autoplay=\"true\" controls=\"true\" loop=\"true\">";
			print "<source src=\"$embed_url\" type=\"video/$type\">";
			print "</video>";

			print "<script type='text/javascript'>
				$('video').height($(document).height() * 0.95);
			</script>";
		} else {
			print "<img class=\"fit\" src=\"$url\">";

			print "<script type='text/javascript'>
				$('img').click(function(e) {
					$(this).toggleClass('fit');
				});
			</script>";

		}

		print "</body></html>";

		break;

	case "vidproxy";
		$url = $_REQUEST["url"];

		if (preg_match("/\.(mp4|webm|gifv)/", $url, $matches)) {
			$type = $matches[1];
			$embed_url = $url;

			if ($type == "gifv") {
				$type = "mp4";
				$embed_url = str_replace(".gifv", ".mp4", $embed_url);
			}

			header("Content-type: text/html");

			$embed_url = htmlspecialchars("backend.php?op=imgproxy&url=" . urlencode($embed_url));

			print "<video class=\"\" autoplay=\"true\" controls=\"true\" loop=\"true\">";
			print "<source src=\"$embed_url\" type=\"video/$type\">";
			print "</video>";
		} else {
			header("Location: " . htmlspecialchars($url));
		}

		break;

	case "chanmute":
		$connection_id = (int) $_REQUEST["connection_id"];
		$channel = $_REQUEST["channel"];

		if ($channel != "---") {

			$sth = $dbh->prepare("SELECT id FROM ttirc_connections WHERE
				id = ? AND owner_uid = ?");
			$sth->execute([$connection_id, $_SESSION['uid']]);

			if ($sth->fetch()) {

				$sth = $dbh->prepare("UPDATE ttirc_channels SET muted = NOT muted
					WHERE connection_id = ? AND LOWER(channel) = LOWER(?) RETURNING muted");
				$sth->execute([$connection_id, $channel]);

				if ($row = $sth->fetch()) {
					print json_encode(["connection_id" => $connection_id,
						"channel" => $channel, "muted" => $row['muted']]);
				}
			}
		} else {

			$sth = $dbh->prepare("UPDATE ttirc_connections SET muted = NOT muted
				WHERE id = ? AND owner_uid = ? RETURNING muted");
			$sth->execute([$connection_id, $_SESSION['uid']]);

			if ($row = $sth->fetch()) {
				print json_encode(["connection_id" => $connection_id,
					"channel" => "---", "muted" => $row['muted'] ]);
			}

		}

		break;

	case "uploadandpost":
		$file = $_FILES['file'];
		$reason = "";

		if (!is_writable("cache/uploads")) {
			$reason = __("Cache not writable");
		} else if (!$file) {
			$reason = __("No file uploaded.");
		} else if ($file['size'] > 50000000) {
			$reason = __("File is too large.");
		} else {
			$new_file_name = 'cache/uploads/' . time() . '-' .
				preg_replace("/[^a-zа-я0-9-_\.]/iu", "_", basename($file['name']));

			$result = move_uploaded_file($file['tmp_name'], $new_file_name);

			if ($result) {
				chmod($new_file_name, 0644);

				$mime_type = (string)mime_content_type($new_file_name);
				$file_ext = pathinfo($new_file_name, PATHINFO_EXTENSION);

				if (!$file_ext) {

					if (strpos($mime_type, "image/") !== false)
						$file_ext = ".jpg";
					else if (strpos($mime_type, "video/") !== false)
						$file_ext = ".mp4";

					if (rename($new_file_name, $new_file_name . $file_ext))
						$new_file_name = $new_file_name . $file_ext;
				}

				$url = get_self_url_prefix() . "/$new_file_name";

				print json_encode(['upload_url' => $url, 'mime' => $mime_type]);

				return;
			} else {
				$reason = __("Error moving file.");
			}

		}

		print json_encode(["status" => "UPLOAD_FAILED", "reason" => $reason]);

		break;

	case "urlmetadata":
		if (defined('FETCH_URL_TITLES') && FETCH_URL_TITLES) {

			global $redis;

			$base_url = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
			$url = validate_url($_REQUEST["url"], true);

			$ip_addr = gethostbyname(parse_url($url, PHP_URL_HOST));

			if (!$ip_addr || strpos($ip_addr, "127.") === 0) {
				print json_encode([]);
				return false;
			}

			$key = 'ttirc_urlcache,' . sha1($url);
			$req_key = 'ttirc_urlcache-req,' . sha1($url);
			$nocache = (bool) $_REQUEST["nocache"];

			if (!$nocache && !$rv = $redis->get($key)) {
				$tries = 0;

				while ($tries < PROXY_CONCUR_TRIES && $rc = $redis->get($req_key)) {
					if (!$rc || $rc == 2)
						break;

					$tries++;
					sleep(1);
				}
			}

			if ($nocache || !$rv = $redis->get($key)) {

				$fetch_effective_url = resolve_redirects($url, 10);

				if (!validate_url($fetch_effective_url, true)) {
					print json_encode([]);
					return false;
				}

				$fetch_effective_ip_addr = gethostbyname(parse_url($fetch_effective_url, PHP_URL_HOST));

				if (!$fetch_effective_ip_addr || strpos($fetch_effective_ip_addr, "127.") === 0) {
					print json_encode([]);
					return false;
				}

				$rv = [ ];

				$options = ['http'=> [
					'protocol_version'=> 1.1,
					'header' => [
						'Connection: close',
		            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*\/*;q=0.8',
						'Range: bytes=0-32768' ],
					'timeout' => 10 ]
				];

				if (defined('_HTTP_PROXY')) {
					$options['http']['request_fulluri'] = true;
					$options['http']['proxy'] = _HTTP_PROXY;
				}

				$redis->set($req_key, PROXY_REQ_PROGRESS);
				$redis->expire($req_key, CACHE_LIFETIME_MAX);

				$ctx = stream_context_create($options);
				$data = file_get_contents($url, false, $ctx);

				if ($http_response_header) {
					foreach ($http_response_header as $header) {
						if (strstr($header, ": ") !== false) {
							list ($hkey, $hvalue) = explode(": ", $header);

							if (strtolower($hkey) == 'content-type') $rv['content-type'] = $hvalue;
						}
					}
				}

				if ($data) {

					$tmp = @gzdecode($data);
					if ($tmp) $data = $tmp;

					$data = mb_substr($data, 0, 32768);

					$tmpdoc = new DOMDocument("1.0", "UTF-8");

					if (@$tmpdoc->loadHTML($data)) {

						// this is the worst hack yet :(
						if (strtolower($tmpdoc->encoding) != 'utf-8') {
							$data = preg_replace("/<meta.*?charset.*?\/?>/i", "", $data);

							if ($tmpdoc->encoding)
								$data = mb_convert_encoding((string)$data, 'utf-8', $tmpdoc->encoding);
						}
					}

					$doc = new DOMDocument("1.0", "UTF-8");

					if (@$doc->loadHTML('<?xml encoding="UTF-8">' . $data)) {

						$xpath = new DOMXPath($doc);

						$m_title = $xpath->query("//meta[@property='og:title']")->item(0);
						$m_image = $xpath->query("//meta[@property='og:image']")->item(0);
						$m_descr = $xpath->query("//meta[@property='og:description']")->item(0);

						if ($m_title) {
							$rv['title'] = $m_title->getAttribute('content');
						} else {

							$node = $doc->getElementsByTagName('title')->item(0);

							if ($node) {
								$rv['title'] = preg_replace("/[\r\n\t]/", "", trim($node->nodeValue));
							}
						}

						if ($m_image)
							$rv['image'] = rewrite_relative_url($url, $m_image->getAttribute('content'));

						if ($m_descr)
							$rv['descr'] = preg_replace("/[\r\n\t]/", "", trim($m_descr->getAttribute('content')));

						foreach ($rv as $k => $v) {
							$rv[$k] = mb_substr($v, 0, 1024);
						}

						if ($rv['title']) {
							$redis->set($key, json_encode($rv));
							$redis->expire($key, CACHE_LIFETIME_MAX);
						}
					}

					$redis->set($req_key, PROXY_REQ_OK);
					$redis->expire($req_key, CACHE_LIFETIME_MAX);
				} else {
					$redis->set($req_key, PROXY_REQ_FAILED);
					$redis->expire($req_key, CACHE_LIFETIME_MAX);
				}
			} else {
				$rv = json_decode($rv, true);
			}

			print json_encode($rv);
		}

		break;

	case "mediaredirect":
		$url = $_REQUEST["url"];

		if ($_SESSION["uid"] && file_exists("cache/imgproxy/" . sha1($url) . ".webp")) {
			$url = "backend.php?op=imgproxy&url=" . urlencode($url);
		}

		header("Location: $url");
		break;

	case "imgproxy";

		global $redis;

		$base_url = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		$url = validate_url($_REQUEST["url"], true);

		$ip_addr = gethostbyname(parse_url($url, PHP_URL_HOST));

		if (!$ip_addr || strpos($ip_addr, "127.") === 0) {
			return false;
		}

		@$resize = (int) $_REQUEST['resize'];
		@$nocache = (bool) $_REQUEST['nocache'];
		@$force_stamp = (bool) $_REQUEST['stamp'];

		if (is_internal_req($base_url, $url)) {
			header("Location: $url");
			return;
		}

		/* special handling for android client, cid is a sha1()-hashed SID */

		if (!$_SESSION['uid']) {
			if (!@is_valid_cid($_REQUEST['cid'])) {
				header("Location: $url");
				return;
			}
		}

		$cache_key = "cache/imgproxy/" . sha1($url) . ($resize ? "-$resize" : "") . ".webp";
		$cache_key_fsize = "cache/imgproxy/" . sha1($url) . ".webp";

		$mem_key = 'ttirc_imgcache,' . sha1($cache_key);

		if (!file_exists($cache_key) || filesize($cache_key) == 0) {

			$tries = 0;

			while ($tries < PROXY_CONCUR_TRIES && $rc = $redis->get($mem_key)) {
				if (!$rc || $rc == 2)
					break;

				$tries++;
				sleep(1);
			}
		}

		if (!$nocache && file_exists($cache_key) && filesize($cache_key) > 0) {
			@touch($cache_key);

			$mimetype = (string)mime_content_type($cache_key);

			if ($mimetype == "application/octet-stream")
				$mimetype = "video/mp4";

			# block SVG because of possible embedded javascript (.....)
			$mimetype_blacklist = [ "image/svg+xml" ];

			/* only serve video and images, send everything else as text/plain */
			if (!preg_match("/(image|video)\//", $mimetype) || in_array($mimetype, $mimetype_blacklist)) {
				http_response_code(400);
				header("Content-type: text/plain");

				print "Stored file has disallowed content type ($mimetype)";
				return false;
			}

			header("Content-type: $mimetype");
			header("Cache-control: max-age=" . CACHE_LIFETIME_MAX);
			header("Last-Modified: " .
				gmdate("D, d M Y H:i:s \G\M\T", (int)filemtime($cache_key)));
			header("Expires: " .
				gmdate("D, d M Y H:i:s \G\M\T", (int)filemtime($cache_key)+CACHE_LIFETIME_MAX));

			$req_filename = basename($url);
			header("Content-Disposition: inline; filename=\"$req_filename\"");

			if (defined('_NGINX_XACCEL_PREFIX')) {
				header("X-Accel-Redirect: " . _NGINX_XACCEL_PREFIX . "/$cache_key");
			} else {
				readfile($cache_key);
			}
		} else {

			$fetch_effective_url = resolve_redirects($url, 15);

			if (!validate_url($fetch_effective_url, true)) {
				return false;
			}

			$fetch_effective_ip_addr = gethostbyname(parse_url($fetch_effective_url, PHP_URL_HOST));

			if (!$fetch_effective_ip_addr || strpos($fetch_effective_ip_addr, "127.") === 0) {
				return false;
			}

			$redis->set($mem_key, PROXY_REQ_PROGRESS);
			$redis->expire($mem_key, CACHE_LIFETIME_MAX);

			$options = ['http'=> [
				'protocol_version'=> 1.1,
				'header' => [
					'Connection: close',
				],
				'timeout' => 15 ]
			];

			if (defined('_HTTP_PROXY')) {
				$options['http']['request_fulluri'] = true;
				$options['http']['proxy'] = _HTTP_PROXY;
			}

			$ctx = stream_context_create($options);
			$data = file_get_contents($url, false, $ctx);

			if ($data) {

				$resp_ctype = "";

				if ($http_response_header) {
					foreach ($http_response_header as $header) {
						if (strstr($header, ": ") !== false) {
							list ($key, $value) = explode(": ", $header);

							if (strtolower($key) == 'content-type') $resp_ctype = $value;
						}
					}
				}

				if (!preg_match("/image|video/", $resp_ctype)) {

					$redis->set($mem_key, PROXY_REQ_OK);
					$redis->expire($mem_key, CACHE_LIFETIME_MAX);

					header("Location: $url");
					return;
				}

				$is_video = strpos($resp_ctype, "video/") !== false;

				if ($resize) {
					// might as well save unresized image since we have it already
					file_put_contents($cache_key_fsize, $data);

					if (defined('_FFMPEG_THUMBNAILS') && _FFMPEG_THUMBNAILS && $is_video)
						$data = ffmpeg_thumbnail($data);

					$data = make_thumbnail($data, $resize, $resize, $force_stamp || $is_video);
				}

				$redis->set($mem_key, PROXY_REQ_OK);
				$redis->expire($mem_key, CACHE_LIFETIME_MAX);

				if (file_put_contents($cache_key, $data)) {
					/* redirect back again so file is read from cache (if allowed) */

					parse_str($_SERVER['QUERY_STRING'], $query_parts);

					// just in case
					unset($query_parts['nocache']);

					header("Location:" . 'backend.php?' . http_build_query($query_parts));
				}

			} else {
				$redis->set($mem_key, PROXY_REQ_FAILED);
				$redis->expire($mem_key, CACHE_LIFETIME_MAX);
			}
		}
		break;

	case "save-last-read":
		$chan = $_REQUEST["chan"];
		$connection_id = $_REQUEST["connection"];
		$message_id = $_REQUEST["id"];
		$owner_uid = $_SESSION['uid'];
		$uniqid = $_REQUEST['uniqid'];

		$stored_id = (int) $redis->get("ttirc_last_read,$owner_uid,$connection_id,$chan");

		if ($message_id > $stored_id) {
			$redis->set("ttirc_last_read,$owner_uid,$connection_id,$chan", $message_id);
			$redis->publish("ttirc_user," . $owner_uid, "UPDATE;SENDER=$uniqid");
			print json_encode(["status" => "OK"]);
		} else {
			print json_encode(["status" => "REJECTED"]);
		}

		break;

	case "expirecaches":
		$files = glob("cache/uploads/*");

		foreach ($files as $file) {
			if (time() - filemtime($file) > CACHE_LIFETIME_MAX) {
				unlink($file);
			}
		}

		$files = array_merge(
			glob("cache/imgproxy/*.jpg"),
			glob("cache/imgproxy/*.webp")
		);

		foreach ($files as $file) {
			if (time() - filemtime($file) > CACHE_LIFETIME_MAX) {
				unlink($file);
			}
		}

		print json_encode(["status" => "OK"]);
		break;

	case "ping":
		print json_encode(["status" => "OK"]);
		break;

	case "logout":
		logout_user();
		header("Location: index.php");
		break;
	}
?>
