begin;

alter table ttirc_messages add column userhost varchar(120);

update ttirc_version set schema_version = 14;

commit;
