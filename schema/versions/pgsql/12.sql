begin;

alter table ttirc_emoticons_popcon drop column if exists last_ised;

update ttirc_version set schema_version = 12;

commit;
