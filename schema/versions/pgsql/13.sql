begin;

alter table ttirc_channels add column muted boolean;
update ttirc_channels set muted = false;
alter table ttirc_channels alter column muted set default false;
alter table ttirc_channels alter column muted set not null;

alter table ttirc_connections add column muted boolean;
update ttirc_connections set muted = false;
alter table ttirc_connections alter column muted set default false;
alter table ttirc_connections alter column muted set not null;

update ttirc_version set schema_version = 13;

commit;
