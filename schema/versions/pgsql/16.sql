begin;

CREATE OR REPLACE FUNCTION ttirc_notify_new_message() RETURNS trigger as $$ DECLARE BEGIN  PERFORM pg_notify('ttirc_messages', 'NEW_MESSAGE'); RETURN new; END; $$ language plpgsql;

CREATE TRIGGER ttirc_new_message_trigger AFTER INSERT ON ttirc_messages EXECUTE PROCEDURE ttirc_notify_new_message();

update ttirc_version set schema_version = 16;

commit;


