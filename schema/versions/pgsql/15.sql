begin;

alter table ttirc_users add column last_message timestamp;

update ttirc_version set schema_version = 15;

commit;
