self.addEventListener('notificationclick', function (event) {
	event.notification.close();

	event.waitUntil(clients.matchAll({includeUncontrolled: true, type: 'window'}).then((windows) => {
		for (let i = 0; i < windows.length; i++) {
			if (windows[i].url == event.notification.data) {
				console.log('focusing window', windows[i]);
				windows[i].focus();
				break;
			}
		}
	}));
}, false);

