Tiny Tiny IRC
=============

Ajax-powered web-based IRC client.

Wiki: https://git.tt-rss.org/git/tt-irc/wiki

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Licensed under GNU GPL version 3

Copyright (c) 2010 Andrew Dolgov (unless explicitly stated otherwise).

