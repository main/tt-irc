'use strict';

/* global __, Prefs */

function show_users() {
	show_spinner();

	$.post("backend.php", {op: 'users'}, function (resp) {
		infobox_callback2(resp);
		hide_spinner();
	}, 'html');
}

function create_user() {
	const login = prompt(__("Login for a new user:"));

	if (login) {

		show_spinner();

		$.post("backend.php", {op: 'create-user', login: login}, function (obj) {

			if (obj) {
				const message = obj[0];
				const data = obj[1];

				Prefs.miniError(message);

				$("#users-list").html(data);
				hide_spinner();
			}
		});

	}
}

function delete_user() {
	const rows = Prefs.get_selected_rows("#users-list");

	if (rows.length > 0) {
		if (confirm(__("Delete selected users?"))) {

			const ids = [];

			for (let i = 0; i < rows.length; i++) {
				ids.push(rows[i].getAttribute("user_id"));
			}

			const query = {op: 'delete-user', ids: ids.toString()};

			console.log(query);

			show_spinner();

			$.post("backend.php", query, function(resp) {
				$("#users-list").html(resp);
				hide_spinner();
			}, 'html');

		}
	} else {
		alert(__("Please select some users to delete."));
	}
}

function reset_user() {
	const rows = Prefs.get_selected_rows("#users-list");

	if (rows.length == 1) {
		if (confirm(__("Reset password of selected user?"))) {

			const id = rows[0].getAttribute("user_id");

			const query = {op: 'reset-password', id: id};

			console.log(query);

			show_spinner();

			$.post("backend.php", query,function (obj) {

				if (obj) {
					Prefs.miniError(obj.message);
				}

				hide_spinner();
			});

		}
	} else {
		alert(__("Please select one user to reset password."));
	}
}

function edit_user(id) {
	Prefs.miniError("Function not implemented");
}
