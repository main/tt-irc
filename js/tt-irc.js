'use strict';

/* global ko, PluginHost, __, localforage, hljs */
/* eslint-disable no-redeclare,no-useless-escape,no-undef */

let last_id = 0;
let last_old_id = 0;
const input_cache = [];
let input_autocomplete = [];
let input_autocomplete_index = 0;
let highlight_on = [];
let notify_events = [];
let master_running = 0;
let ping_interval = 0;
let last_update = 0;
let event_source = false;
/* exported theme */
let theme = "";
let hide_join_part = false;
let disable_image_preview = false;
let startup_date;
const id_history = [];
let uniqid;
let fetch_url_titles = false;
let has_touch = false;
let default_buffer_size = 0;
let log_last_scroll_top = 0;

let url_data_cache = {};
let image_embed_cache = {};

const ALLOW_EMOTICONS = get_cookie('ttirc_emoticons') != "false";
const ALLOW_EMBEDS = get_cookie("ttirc_noembed_mode") != "true";

const BUFFER_MAX_LINES = ALLOW_EMBEDS ? 150 : 300;
const MAX_HISTORY_LINES = 1024; // per-channel
const NOTIFY_MESSAGE_MAX_AGE = 120; // seconds

const DEVICE_KIND = window.matchMedia("only screen and (max-width: 760px)").matches ? "mobile" : "web";

/* eslint-disable no-unused-vars */

const MSGT_PRIVMSG = 0;
const MSGT_COMMAND = 1;
const MSGT_BROADCAST = 2;
const MSGT_ACTION = 3;
const MSGT_TOPIC = 4;
const MSGT_PRIVATE_PRIVMSG = 5;
const MSGT_EVENT = 6;
const MSGT_NOTICE = 7;
const MSGT_SYSTEM = 8;

const CS_DISCONNECTED = 0;
const CS_CONNECTING = 1;
const CS_CONNECTED = 2;

const CT_CHANNEL = 0;
const CT_PRIVATE = 1;

const KIND_UNDEFINED = 0;
const KIND_MESSAGE = 1;
const KIND_SYSTEM = 2;
const KIND_PREVIEW = 3;

/* eslint-enable no-unused-vars */

const IMG_UPLOAD_MAX_SIZE = 1280;

let initial = true;

const Connection = function(data) {
	const self = this;

	self.status = ko.observable(0);

	self.id = ko.observable(0);
	self.connection_id = ko.observable(0);
	self.active_nick = ko.observable("");
	self.active_server = ko.observable("");
	self.title = ko.observable("");
	self.userhosts = ko.observableArray([]).extend({deferred:true});
	self.channels = ko.observableArray([]);
	self.lines = ko.observableArray([]).extend({deferred:true});
	self.status = ko.observable(0);
	self.nicklist = ko.observableArray([]).extend({deferred:true});
	self.highlight = ko.observable(false);
	self.unread = ko.observable(0);
	self.type = ko.observable("S");
	self.muted = ko.observable(false);
	self.last_visible_id = ko.observable(-1);
	self.is_instance = ko.observable(0);
	self.last_read = ko.observable(0);
	self.rtt = ko.observable(0);
	self.dateMap = ko.observableArray();

	self.saveLastRead = function() {
		//
	};

	self.getHistory = function() {
		//
	};

	self.searchQuery = ko.observable("");

	self.displayLines = ko.computed(function() {
		return self.lines
			.sorted((b,a) => { return a.ts() - b.ts() })
			.slice(-BUFFER_MAX_LINES)

	});

	self.displaySearchResults = ko.computed(function() {
		if (self.searchQuery())
			return self.lines()
				.filter((msg) =>
					msg.message().match(self.searchQuery()) || msg.message_formatted().match(self.searchQuery()))
				.sort((a,b) => { return a.ts() - b.ts() });

	});

	self.update = async function(data) {
		self.id(data.id);
		self.connection_id(data.id);
		self.active_nick(data.active_nick);
		self.userhosts(data.userhosts);
		self.muted(data.muted);
		self.is_instance(data.is_instance);

		self.status(parseInt(data.status));
		self.active_server(data.active_server);
		self.title(data.title);

	};

	self.connected = ko.computed(function() {
		return self.status() == CS_CONNECTED;
	}, self);

	self.connecting = ko.computed(function() {
		return self.status() == CS_CONNECTING;
	}, self);

	self.selected = ko.computed(function() {
		return model.activeChannel() == self;
	});

	self.nickExists = function(nick) {
		for (let i = 0; i < self.channels().length; i++) {
			if (self.channels()[i].nicklist() &&
					self.channels()[i].type() == "C" &&
					this.channels()[i].nicklist().nickIndexOf(nick) !== -1)

				return true;
		}
	};

	self.topic = ko.computed({
		read: function() {
			let topic = "";

			switch (self.status()) {
			case CS_CONNECTING:
				topic = __("Connecting...");
				break;
			case CS_CONNECTED:
				topic = __("Connected to: ") + self.active_server();

				if (self.rtt())
					topic += " "  + __("(RTT: %s)").replace("%s", self.rtt());

				break;
			case CS_DISCONNECTED:

				if (!master_running)
					topic = __("Backend daemon is not running, external connections are not available.");
				else
					topic = __("Disconnected.");

				break;
			}

			return [topic];
		},
		write: function() {
			//
		},
		owner: self
	});

	self.topicDisabled = ko.computed(function() {
		return true;
	}, self);

	self.update(data);
};

const Message = function(data, chan) {
	const self = this;

	self.id = ko.observable(data.id);
	self.message_type = ko.observable(parseInt(data.message_type));
	self.sender = ko.observable(data.sender);
	self.channel = ko.observable(data.channel);
	self.connection_id = ko.observable(data.connection_id);
	self.incoming = ko.observable(data.incoming);
	self.message = ko.observable(data.message);
	self.message_formatted = ko.observable("").extend({deferred: true});
	self.message_formatted_mtime = ko.observable(0);
	self.ts = ko.observable(data.ts);
	self.sender_color = ko.observable(data.sender_color);
	self.is_hl = ko.observable(is_highlight(data.connection_id, data));
	self.chan = ko.observable(chan);
	self.userhost = ko.observable(data.userhost);
	self.sender_kind = ko.observable(data.sender_kind);
	self.is_lv = ko.computed(function() {
		const last_id = self.chan().last_visible_id();

		return self.id() == last_id;
	});

	self.formatted_ts = function(long_fmt) {
		let opts = {};

		if (long_fmt) {
			return self.ts();
		} else {
			return self.ts().toLocaleTimeString("en-GB", {timeStyle: 'short', hour12: false});
		}
	};

	/*if (emoticons_map && self.message()) {
		self.message(rewrite_emoticons(self.message()));
	}*/

	self.rewrite_url_titles = function(msg, allow_extended) {
		msg = $("<span>" + msg + "</span>");

		const links = msg.find("a").filter(':not([data-processed])');

		$.each(links, function(i, link) {
			link = $(link);

			//console.log('checking', link[0]);

			if (!link.attr('href') || is_media(link.attr('href'))) return;

			const json = url_data_cache[link.attr('href')];

			if (json) {
				link.attr('data-processed', 1);

				if (json.title) {
					const maxlen = Math.max(link.html().length, 100);

					link.attr('title', json.title);
					link.html(truncate_string(json.title, maxlen));
				}

				if (ALLOW_EMBEDS && allow_extended && (json.image || json.descr)) {
					const box = $("<div>")
						.attr("class", "extended-link")
						.attr("title", json.descr);

					let real_src;

					if (json.image && json.image.indexOf("data:") == 0)
						real_src = json.image;
					else if (json.image)
						real_src = "backend.php?op=imgproxy&resize="+IMAGE_THUMB+"&url=" +
							param_escape(json.image);
					else
						real_src = ""; //"images/icon-app.png";

					const thumb = $("<img>")
						.attr("class", "img-embed")
						.attr("loading", "lazy")
						.attr("referrerpolicy", "no-referrer")
						.css("background-image", "url(" +  real_src + ")");

					const thumb_link = $("<a>")
						.append(thumb)

					const main_link = $("<a>")
						.attr("class", "title")
						.html(truncate_string(json.title, 150))

					const og_link = $("<a>")
						.attr("class", "excerpt")
						.html(truncate_string(json.descr ?
							truncate_string(json.descr, 200) : link.attr("href"), 250))

					$.each([thumb_link, main_link, og_link], function(i, e) {
						const is_visited = sessionStorage["ttirc.visited." + link.attr("href").hashCode()] != undefined;

						e.attr("href", link.attr("href"))
							.attr("onclick", link.attr("onclick"))
							.attr("data-orig-url", link.attr("data-orig-url"))
							.attr('data-visited', is_visited)
							.attr("target", "_blank");
					});

					box
						.append(thumb_link)
						.append($("<div>")
							.attr("class", "contents")
							.append(main_link)
							.append($("<br>"))
							.append(og_link))

					link.replaceWith(box);

				}

			} else {
				window.requestIdleCallback(function() {
					fetch("backend.php?op=urlmetadata&url=" + param_escape(link.attr('href')),
						{credentials: 'same-origin'}).then(function(resp) {

						if (resp.ok) {
							resp.json().then(function(json) {
								url_data_cache[link.attr('href')] = json ? json : [];

								//self.rewrite_url_titles();
								self.message_formatted('');
								self.message.notifySubscribers();

								update_buffer(log_bottom_offset());
							});
						} else {
							url_data_cache[link.attr('href')] = [];
						}
					});
				}, {timeout: 250 + (2000 * Math.random())});

				return msg.html();
			}
		});

		return msg.html();
	};

	self.unixts = ko.computed(function() {
		return new Date(self.ts()).getTime() / 1000;
	});

	self.ident = ko.computed(function() {
		if (self.userhost()) {
			return self.userhost().split("@")[0];
		} else {
			return model.getNickIdent(self.connection_id(), self.sender());
		}
	});

	self.format = ko.computed(function() {

		//console.log('format', tmp, 'vs', self.message_formatted());

		try {

			if (self.message_formatted() && model.emoticons_mtime() == self.message_formatted_mtime()) {
				return self.message_formatted();
			} else {
				const nick_host = model.getNickHost(self.connection_id(), self.sender(), true);
				const nick_ext_info = nick_host ? nick_host + "\n" + self.sender_kind() : "";
				const ident = self.ident();

				let tmp_message = self.message();

				const m = tmp_message.match(/```([\s\S]+)```/m);

				if (m) {
					tmp_message = tmp_message
						.replace(m[0], "<pre>" + hljs.fixMarkup(hljs.highlightAuto(m[1].trim()).value) + "</pre>");

				} else {
					tmp_message = tmp_message.escapeHTML();
					tmp_message = rewrite_generic(tmp_message, ALLOW_EMBEDS, KIND_MESSAGE);

					if (tmp_message.length <= 2048) {

						tmp_message = rewrite_urls(tmp_message, ALLOW_EMBEDS && self.channel() != "---", self.connection_id(), self.channel());

						if (fetch_url_titles && self.channel() != "---" && tmp_message.indexOf("://") !== -1)
							tmp_message = self.rewrite_url_titles(tmp_message,
								[MSGT_PRIVMSG, MSGT_NOTICE, MSGT_PRIVATE_PRIVMSG].indexOf(self.message_type()) != -1);

						if (ALLOW_EMOTICONS)
							tmp_message = rewrite_emoticons(tmp_message, KIND_MESSAGE);

					}

					tmp_message = "<span class='contents'>" + tmp_message + "</span>";
				}

				let formatted_message;

				switch (self.message_type()) {
					case MSGT_ACTION:
							formatted_message = `<span class="timestamp" title="${self.formatted_ts(true)}">${self.formatted_ts()}</span>
								<span class="sys-icon">&#10033;</span><span class="action">${rewrite_emoticons(self.sender(), KIND_MESSAGE)} ${tmp_message}</span>`;

							break;
					case MSGT_NOTICE: {
							const sender_class = self.incoming() == true ? 'pvt-sender' : 'pvt-sender-out';

							formatted_message = `<span class="timestamp" title="${self.formatted_ts(true)}">${self.formatted_ts()}</span>
								<span class='lt'>&ndash;</span><span title="${nick_ext_info}"
									class="${sender_class}" style="color : ${colormap[self.sender_color()]}">
									${rewrite_emoticons(self.sender())}</span>
								<span class='gt'>&ndash;</span>
								<span class="message notice">${tmp_message}</span>`;

							}
							break;
					case MSGT_SYSTEM:
							formatted_message = `<span class="timestamp" title="${self.formatted_ts(true)}">${self.formatted_ts()}</span>
								<span class='sys-icon'>&#10070;</span>
								<span class='sys-message'>${tmp_message}</span>`;

							break;
					default:
						if (self.sender() != "---") {
							const mobile_icon = self.sender_kind() == "mobile" ? "<span class='glyphicon glyphicon-phone'></span>" : "";

							formatted_message = `<span class="timestamp" title="${self.formatted_ts(true)}">${self.formatted_ts()}</span>
								<span class="lt">&lt;</span><span title="${nick_ext_info}"
									class="sender" ident="${ident}" style="color : ${colormap[self.sender_color()]}">
									${rewrite_emoticons(self.sender())}</span>
								<span class="gt">&gt;</span>
								<span class="message" ident="${ident}">${tmp_message}</span> ${mobile_icon}
							`;

						} else {
							formatted_message = `<span class="timestamp" title="${self.formatted_ts(true)}">${self.formatted_ts()}</span>
								<span class='sys-icon'>&#10070;</span>
								<span class="sys-message">${tmp_message}</span>`;
						}

						break;
				}

				self.message_formatted(formatted_message);
				self.message_formatted_mtime(model.emoticons_mtime());

				return formatted_message;
			}

		} catch (e) {
			console.warn(e);
			try {
				return "[EF1: " + e + "]: " + self.id() + " " + self.message();
			} catch (ei) {
				return "[EF2: " + e + "]: " + ei;
			}
		}

	});
}

const Channel = function(connection_id, title, tab_type) {
	const self = this;

	self.title = ko.observable(title);
	self.type = ko.observable(tab_type);
	self.nicklist = ko.observableArray([]).extend({deferred:true});
	self.connection_id = ko.observable(connection_id);
	self.lines = ko.observableArray([]);
	self._topic = ko.observableArray([]);
	self.topicEventSynthesized = ko.observable(false);
	self.highlight = ko.observable(false);
	self.unread = ko.observable(0);
	self.muted = ko.observable(false);
	//self.can_be_muted = ko.observable(true);
	self.last_visible_id = ko.observable(-1);
	self.history_complete = ko.observable(false);
	self.history_requested = ko.observable(false);
	self.searchQuery = ko.observable("");
	self._last_read = ko.observable(0);
	self.dateMap = ko.observableArray();

	self.last_read = ko.computed({
		read: function() {
			return self._last_read();
		},
		write: function(value) {
			if (self._last_read() < value) {
				self._last_read(value);

				if (self.unread() > 0) {
					const last_row = self.displayLines().last();

					if (last_row && last_row.id() <= value) {
						model.new_messages(model.new_messages()-self.unread());
						self.unread(0);
						self.highlight(false);
					}
				}
			}
		},
		owner: self
	});

	self.displayLines = ko.computed(function() {

		return self.lines
			.sort((a,b) => { return a.ts() - b.ts() })
			.slice(-BUFFER_MAX_LINES)

	});

	self.displaySearchResults = ko.computed(function() {
		if (self.searchQuery()) {
			const query = self.searchQuery().toLowerCase();

			return self.lines()
				.filter((msg) => (msg.sender() && msg.sender().toLowerCase().match(query)) ||
					(msg.userhost() && msg.userhost().toLowerCase().match(query)) ||
					msg.message().toLowerCase().match(query) ||
					msg.message_formatted().toLowerCase().match(query));

		}
	});

	self.saveLastRead = function() {
		if (!model.window_active())
			return;

		const last_row = self.displayLines()
			.filter((msg) => { return msg.id() != 0 })
			.last();

		if (last_row && last_row.id() > self.last_read()) {
			console.log('saveLastRead last id=', last_row.id(), ' last=', self.last_read());
			self.last_read(last_row.id());

			const query = { "op": "save-last-read", connection: self.connection_id(), chan: self.title(), id: last_row.id(), uniqid: uniqid };
			$.post("backend.php", query);
		}

	};

	self.getHistory = function(offset, get_all) {
		if (self.history_requested() || self.history_complete())
			return;

		if (!get_all && self.lines().length >= BUFFER_MAX_LINES)
			return;

		self.history_requested(true);

		offset = offset || self.lines().filter((msg) => { return msg.id() != 0 }).length;

		console.log('sending history request for', self.title(), 'offset=', offset);

		const query = {op: "history", chan: self.title(), connection: self.connection_id(), offset: offset };

		$.post("backend.php", query, function (resp) {

			// what if we got a fatal error instead?
			if (!handle_error(resp, resp))
				return;

			console.log('got', resp[1].length, 'history lines; buf=', default_buffer_size);
			self.history_requested(false);

			const restore_height = $("#log").prop('scrollHeight') - $("#log").scrollTop();

			handle_update(resp).then(() => {
				$("#log").scrollTop($("#log").prop('scrollHeight') - restore_height);

				console.log('hist lines added');

				if (resp[1].length != default_buffer_size) {
					self.history_complete(true);
				} else if (get_all) {
					self.getHistory(0, get_all);
				}
			});
		});
	}

	self.topicDisabled = ko.computed(function() {
		return self.type() != "C";
	}, self);

	self.topic = ko.computed({
		read: function() {
			switch (self.type()) {
			case "P":
				{
					const nick_ext_info = model.getNickHost(self.connection_id(), self.title());
					let topic = __("Conversation with") + " " + self.title();

					if (nick_ext_info)
						topic = topic + " (" + nick_ext_info + ")";

					return [topic.escapeHTML()];
				}
			default:
				return self._topic();
			}
		},
		write: function(topic) {
			self.synthesizeTopicEvent(topic);
			self._topic(topic);
		},
		owner: self
	});

	self.synthesizeTopicEvent = function(topic, force) {
		if (self.type() != "P" && typeof topic == 'object' && (force || !self.topicEventSynthesized())) {
			self.topicEventSynthesized(true);

			if (topic[0].length > 0) {
				const line = new Object();

				line.message = __("Topic for %c is: %s").replace("%c", self.title()).
					replace("%s", topic[0].replace('<', '&lt;'));

				line.message_type = MSGT_SYSTEM;
				line.ts = new Date();
				line.id = last_id;
				line.force_display = 1;

				push_message(self.connection_id(), self.title(), line, MSGT_PRIVMSG, force);

				line.message = __("Topic for %c set by %n at %d").replace("%c", self.title()).
					replace("%n", topic[1]).
					replace("%d", topic[2]);

				line.message_type = MSGT_SYSTEM;
				line.ts = new Date();
				line.id = last_id;
				line.force_display = 1;

				push_message(self.connection_id(), self.title(), line, MSGT_PRIVMSG, force);
			}
		}

	};

	self.selected = ko.computed(function() {
		return model.activeChannel() == self;
	});

	self.offline = ko.computed(function() {
		if (self.type() == "P") {
			const conn = model.getConnection(self.connection_id());
			if (conn)
				return !conn.nickExists(self.title());
		} else {
			return false;
		}
	}, self);
};

const UploadAndPost = {
	asyncUpload: function(fd) {
		show_spinner();

		fd.append('csrf_token', __csrf_token);

		$.ajax({url: 'backend.php',
			type: 'POST',
			processData: false,
			contentType: false,
			cache: false,
			data: fd,
			xhr: function () {
				const xhr = $.ajaxSettings.xhr();
				/* xhr.onprogress = function e() {
					 // For downloads
					 if (e.lengthComputable) {
						  console.log(e.loaded / e.total);
					 }
				}; */
				xhr.upload.onprogress = function (e) {
					// For uploads
					if (e.lengthComputable) {
							console.log('uploading', e.loaded / e.total);

							const p = parseInt(e.loaded / e.total * 100);
							const msg = p < 100 ? __('Uploading (%p%)...').replace("%p", p) : "";

							// TODO: should this go via model instead?
							$("#input-prompt").attr('placeholder', msg);
					}
				};
				return xhr;
			},
			error: function(/*data*/) {
				hide_spinner();

				$("#input-prompt").attr('placeholder', '');

				model.notifyPopup(__("Upload failed"), __('POST request failed'));

			},
			success: function(data) {
				hide_spinner();

				$("#input-prompt").attr('placeholder', '');

				if (data.error) {
					alert(data.error);
				}

				if (data.upload_url) {
					const input = $("#input-prompt");

					input.val(input.val() + " " + data.upload_url.replace(" ", "%20"));
				} else if (data.reason) {
					model.notifyPopup(__("Upload failed"), data.reason);
				}
			}
			}, 'json');
	},
	handleForm: function() {
		const form = $("#upload_and_post")[0];
		const fd = new FormData(form);

		this.asyncUpload(fd);
		form.reset();
	},
	resizeAndUpload: function(orig_image) {
		const self = this;

		const reader = new FileReader();

		reader.onload = function (readerEvent) {

			const image = new Image();
			image.onload = function (/* imageEvent */) {

				const canvas = document.createElement('canvas');

				let width = image.width;
				let height = image.height;

				/* if (width <= IMG_UPLOAD_MAX_SIZE && height <= IMG_UPLOAD_MAX_SIZE)
					return self.upload(orig_image); */

				if (width > height) {
					if (width > IMG_UPLOAD_MAX_SIZE) {
						height *= IMG_UPLOAD_MAX_SIZE / width;
						width = IMG_UPLOAD_MAX_SIZE;
					}
				} else if (height > IMG_UPLOAD_MAX_SIZE) {
						width *= IMG_UPLOAD_MAX_SIZE / height;
						height = IMG_UPLOAD_MAX_SIZE;
				}

				canvas.width = width;
				canvas.height = height;

				canvas.getContext('2d').drawImage(image, 0, 0, width, height);

				canvas.toBlob((blob) => {

					blob.name = orig_image.name.replace(/\.[^/.]+$/, ".webp")

					console.log('resized blob', blob);

					self.upload(blob);

				}, 'image/webp', 0.9);
			}

			image.src = readerEvent.target.result;
		}

		console.log('original blob', orig_image);

		reader.readAsDataURL(orig_image);
	},
	upload: function(file) {
		const fd = new FormData();

		fd.append('file', file, file.name);
		fd.append('op', 'uploadandpost');

		this.asyncUpload(fd);
	},
	handleFiles: function(files) {
		for (const file of files) {
			if (file.type.indexOf('image') === 0) {
				return this.resizeAndUpload(file);
			} else {
				return this.upload(file);
			}
		}
	},
	handleClipboard: function(e) {
		const items = (e.clipboardData || e.originalEvent.clipboardData).items;

		for (const item of items) {
			if (item.type.indexOf('image') === 0) {
				return this.resizeAndUpload(item.getAsFile());
			}
		}
	},
};

function Model() {
	const self = this;

	self.window_active = ko.observable(true);
	self.new_messages = ko.observable(0);
	self.new_highlights = ko.observable(0);
	self.alerts_enabled = ko.observable(true);
	self.away_visible = ko.observable(false);
	self.emoticons_visible = ko.observable(false);

	self.connections = ko.observableArray([]);
	self._activeChannel = ko.observable("");
	self._activeConnectionId = ko.observable(0);
	self.emoticons_mtime = ko.observable(0);
	self.emoticons_cache = ko.observableArray([]);
	self.emoticons_map = ko.observableArray([]);

	self.networkError = ko.observable(false);

	self.getNickIdent = function(connection_id, nick) {
		nick = self.stripNickPrefix(nick);
		const conn = self.getConnection(connection_id);

		if (conn && conn.userhosts()[nick] && conn.userhosts()[nick][0]) {
			return conn.userhosts()[nick][0].replace("~", "");
		}

	};

	self.search = function() {
		const query = $("#searchBox .search_query").val();

		console.log('searching for', query);

		self.activeChannel().searchQuery(query);

		if (!query)
			window.setTimeout(function() {
				$("#log").scrollTop($("#log").prop('scrollHeight'));
			}, 10);

		return false;
	};

	self.clearSearch = function() {
		$("#searchBox .search_query").val("");

		self.search();

		return false;
	};

	self.showHistory = function() {
		$("#searchBox .search_query").val(".");

		self.activeChannel().getHistory(0, true);
		self.search();

		return false;
	};

	self.isUserAway = function(connection_id, nick) {
		nick = self.stripNickPrefix(nick);
		const conn = self.getConnection(connection_id);

		if (conn && conn.userhosts()[nick]) {
			return conn.userhosts()[nick][4];
		}

		return false;
	};

	self.saveLastVisibleId = function() {
		const chan = self.activeChannel();

		if (chan) {
			const last_row = chan.displayLines()
				.filter((msg) => { return msg.id() != 0 })
				.last();

			if (last_row) {
				const last_id = last_row.id();
				console.log(chan.title(), 'last visible id', last_id);
				chan.last_visible_id(last_id);
			}
		}
	};

	self.getInstanceConnection = function() {
		const conns = self.connections();
		for (let i = 0; i < conns.length; i++) {
			if (conns[i].is_instance()) {
				return conns[i]
			}
		}
	};

	self.getNickHost = function(connection_id, nick, nostatus) {
		const bnick = self.stripNickPrefix(nick);
		const conn = self.getConnection(connection_id);

		if (conn && conn.userhosts()[bnick]) {
			const uh = conn.userhosts()[bnick];

			let status_msg = __("Online");

			if (uh[5] && conn.is_instance())
				status_msg = uh[5];
			else if (uh[5])
				status_msg = __("Away:") + " " + uh[5] + "";
			else if (conn.is_instance() && !self.nickIsVoiced(nick))
				status_msg = __("Idle");


			return uh[0] + '@' +	uh[1] + " <" + uh[3] + ">" + (nostatus ? "" : "\n" + status_msg);
		}
	};

	self.stripNickPrefix = function(nick) {
		if (nick)
			return nick.replace(/^[\@\+]/, "");
		else
			return "";
	};

	self.nickIsOp = function(nick) {
		return nick[0] == '@';
	};

	self.nickIsVoiced = function(nick) {
		return nick[0] == '+';
	};

	self.cleanupChannels = async function(connection_id, titles) {
		const conn = self.getConnection(connection_id);

		if (conn) {
			let i = 0;
			while (i < conn.channels().length) {
				if (titles.indexOf(conn.channels()[i].title()) == -1)
					conn.channels.remove(conn.channels()[i]);

				i++;
			}
		}
	};

	self.cleanupConnections = async function(ids) {
		let i = 0;
		while (i < self.connections().length) {
			if (ids.indexOf(self.connections()[i].id()) == -1)
				self.connections.remove(self.connections()[i]);

			i++;
		}
	};

	self.getConnection = function(id) {
		for (let i = 0; i < self.connections().length; i++) {
			if (self.connections()[i].id() == id)
				return self.connections()[i];
		}
	};

	self.getChannel = function(id, title) {
		const conn = self.getConnection(id);
		if (conn) {
			if (title == "---")
				return conn;

			for (let i = 0; i < conn.channels().length; i++) {
				if (conn.channels()[i].title().toLowerCase() == title.toLowerCase())
					return conn.channels()[i];
			}
		}
	};

	self.activeConnection = ko.computed(function() {
		return self.getConnection(self._activeConnectionId());
	}, self);

	self.activeNick = ko.computed(function() {
		const conn = self.activeConnection();

		if (conn && conn.active_nick)
			return conn.active_nick();

	}, self);

	self.isAway = ko.computed(function() {
		const conn = self.activeConnection();

		if (conn && conn.active_nick)
			if (conn.userhosts && conn.userhosts()[conn.active_nick()])
				return conn.userhosts()[conn.active_nick()][4] == true;

	}, self);

	self.activeChannel = ko.computed({
		read: function() {
			return self.getChannel(self._activeConnectionId(), self._activeChannel());
		},
		write: function(connection_id, channel) {
			self._activeConnectionId(connection_id);
			self._activeChannel(channel);

			const chan = self.getChannel(connection_id, channel);

			if (chan) {
				chan.highlight(false);
				chan.unread(0);
				chan.saveLastRead();
				chan.searchQuery("");

				if ($("#log").prop('scrollHeight') == $('#log').height()) {
					chan.getHistory(0);
				}
			}
		},
		owner: self});

	self.connectBtnLabel = ko.computed(function() {
		const conn = self.activeConnection();

		if (conn && conn.status) {
			switch (conn.status()) {
			case CS_CONNECTING:
				return __("Connecting...");
			case CS_CONNECTED:
				return __("Disconnect");
			case CS_DISCONNECTED:
				return __("Connect");
			}

		} else {
			return __("Connect");
		}

	}, self);

	self.toggleConnection = ko.computed({
		read: function() {
			const conn = self.activeConnection();

			if (!master_running)
				return 0;

			if (conn && conn.is_instance())
				return 0;
			else
				return self.activeConnection;
		},
		write: function() {
			const conn = self.activeConnection();

			if (conn)
				toggle_connection(conn.id(),
					conn.connected() || conn.connecting() ? 0 : 1);
		},
		owner: self
	});

	self.activeTopicChanged = ko.observable();

	self.activeTopicFormatted = ko.computed(function() {
		const chan = self.activeChannel();

		self.activeTopicChanged();

		if (chan && typeof chan.topic() == 'object' && chan.topic()[0]) {
			let topic = rewrite_emoticons(rewrite_urls(chan.topic()[0].replace('<', '&lt;')),
					KIND_SYSTEM);

			if (fetch_url_titles && topic.indexOf("://") !== -1) {

				const msg = $("<span>" + topic + "</span>");
				const links = msg.find("a"); //.filter(':not([title])');

				$.each(links, function(i, link) {
					link = $(link);

					//console.log('checking', link[0]);

					if (!link.attr('href') || is_media(link.attr('href'))) return;

					const json = url_data_cache[link.attr('href')];

					if (json) {

						if (json.title) {
							const maxlen = Math.max(link.html().length, 100);

							link.attr('title', json.title);
							link.html(truncate_string(json.title, maxlen));
						}
					} else {

						window.requestIdleCallback(function() {
							fetch("backend.php?op=urlmetadata&url=" + param_escape(link.attr('href')),
								{credentials: 'same-origin'}).then(function(resp) {

								if (resp.ok) {
									resp.json().then(function(json) {
										url_data_cache[link.attr('href')] = json ? json : [];

										self.activeTopicChanged.notifySubscribers();
									});
								} else {
									url_data_cache[link.attr('href')] = [];
								}
							});
						}, {timeout: 250 + (2000 * Math.random())});

						return false;
					}

					topic = msg.html();

				});

			}

			return topic;
		}

	}, self);

	self.activeTopic = ko.computed(function() {
		const chan = self.activeChannel();

		if (chan)
			return chan.topic()[0];

	}, self);

	self.activeTopicDisabled = ko.computed(function() {
		if (self.networkError())
			return true;

		const conn = self.activeConnection();

		if (conn && conn.status() != CS_CONNECTED)
			return true;

		const chan = self.activeChannel();

		if (chan)
			return chan.topicDisabled();
		else
			return true;

	}, self);

	self.activeStatus = ko.computed(function() {
		return self.activeConnection() && self.activeConnection().status() || CS_DISCONNECTED;
	}, self);

	self.inputEnabled = ko.computed(function() {
		return !self.networkError() && self.activeStatus() == 2;
	}, self);

	self.documentTitle = ko.computed(function() {

		const conn = self.activeConnection();
		let title = "Tiny Tiny IRC";

		if (conn) {
			title = __("Tiny Tiny IRC [%a @ %b / %c]");

			if (!self.window_active() && self.new_messages() && self.alerts_enabled()) {
				if (self.new_highlights()) {
					title = "[*"+self.new_messages()+"] " + title;
				} else {
					title = "["+self.new_messages()+"] " + title;
				}

				self.faviconBadge(self.new_messages(), self.new_highlights() > 0);

			} else {
				self.faviconBadge(0, false);
			}

			if (self.activeChannel()) {
				title = title.replace("%a", conn.active_nick());
				title = title.replace("%b", conn.title());
				title = title.replace("%c", self.activeChannel().title());
			} else {
				title = "Tiny Tiny IRC";
			}
		}

		return title;
	}, self);

	self.faviconBadge = function(number, is_hl) {
		const canvas = document.createElement('canvas');
		canvas.width = 16;
		canvas.height = 16;
		if (canvas.getContext) {

			const link = $("#favicon")[0];

			const ctx = canvas.getContext('2d');
			const img = new Image();
			img.src = 'images/favicon.png';
			img.onload = function() {
				if (number > 0) {
					if (number > 99) number = 99;

					ctx.fillStyle = is_hl ? "#EFD09D" : "#88b0f0";
					ctx.fillRect(0, 0, 16, 16);
					ctx.fillStyle = is_hl ? "#fff" : "#ECF4FF";
					ctx.fillRect(1, 1, 14, 14);
					ctx.fillStyle = is_hl ? "#D3BC9A" : '#88B0F0';
					ctx.font = 'bold 10px sans-serif';
					ctx.textAlign = 'center';
					ctx.fillText(number, 8, 12);
				} else {
					ctx.drawImage(img, 0, 0);
				}

				link.type = 'image/x-icon';
				link.href = canvas.toDataURL("image/x-icon");

			};
		}
	}

	self.updateEmoticons = function(mtime, allow_reload) {
		localforage.getItem("ttirc.emoticons-cache").then(function(obj) {
			if (!obj || obj.mtime != mtime) {
				console.log('updating emoticons...');

				fetch("emoticons/emoticons.json.gz?" + mtime).then(function(resp) {
					return resp.arrayBuffer();
				}).then(function(buf) {
					const gunzip = new Zlib.Gunzip(new Uint8Array(buf));
					const dec = gunzip.decompress();

					if (dec) {
						const json = JSON.parse(new TextDecoder("utf-8").decode(dec));
						let tmp_map = {};

						$.each(Object.keys(json), function(i, k) {
							tmp_map = $.extend(tmp_map, json[k]);
						});

						self.emoticons_cache([]);
						self.emoticons_map(tmp_map);
						self.emoticons_mtime(mtime);

						console.log('emoticons updated');

						localforage.setItem("ttirc.emoticons-cache",
								{mtime: mtime, map: self.emoticons_map(), fullmap: json})
							.then(function() {
								if (allow_reload) window.location.reload();
							});
					}
				});

			} else {
				console.log('using cached emoticons');

				self.emoticons_map(obj.map);

				//self.emoticons_mtime(mtime);
			}
		});
	};

	self.backendPing = function() {
		const ts = new Date().getTime();

		$.post("backend.php", {op: "ping"}, (resp) => {
			const nts = new Date().getTime();
			const instance = model.getInstanceConnection();

			if (instance)
				instance.rtt((nts - ts) / 1000);

			self.networkError(false);
			handle_error(resp, resp);
		}).fail((err) => {
			console.error('ping error', err);
			self.networkError(true);
			console.error("ping failed", err);
		})
	}

	self.notifyPopup = function(title, msg) {
		const m = $("#notifyBox").modal()

		m.find(".modal-body").html(msg);
		m.find(".modal-title").html(title);
		m.show();
	};
}

let model; /* assigned in init() */

/* eslint-disable prefer-const,no-unused-vars */

let colormap = [ "#00CCCC", "#000000", "#0000CC", "#CC00CC", "#606060",
		"green", "#00CC00", "maroon", "navy", "olive",
		"purple", "red", "#909090", "teal", "#ff6400" ]

let colormap_dark = ["#6bc8c8", "#b7edd6", "#9494f5", "#ce7dce", "#a3a3a3",
		"#8fec88", "#7bb57b", "#c96d6d", "#6cacf3", "#cdcd6e",
		"#e097e7", "#f5a2a2", "#909090", "#a3ebee", "#e48f6f"];

/* eslint-enable prefer-const,no-unused-vars */

/*const commands = [ "/join", "/part", "/nick", "/query", "/quote", "/msg",
	"/op", "/deop", "/voice", "/devoice", "/ping", "/notice", "/away",
	"/ctcp", "/clear" ];*/

function init_second_stage(params) {

	console.log("init_second_stage");

	if ('serviceWorker' in navigator) {
			navigator.serviceWorker
           .register('worker.js?' + (new Date().getTime()))
           .then(function() {
				console.log("service worker registered");
			});
	}

	if (!handle_error(params, params)) return false;

	if (!params || params.status != 1) {
		return fatal_error(14, __("The application failed to initialize."),
			params);
	}

	last_old_id = params.max_id;
	ping_interval = params.ping_interval;
	theme = params.theme;
	uniqid = params.uniqid;
	/* params.emoticons is an empty array now */
	default_buffer_size = params.default_buffer_size;

	if (params.emoticons_mtime)
		model.updateEmoticons(params.emoticons_mtime, true);

	$("#overlay").hide();

	$('#searchBox').on('shown.bs.modal', function() {
		$("#searchBox .search_query")
			.val(model.activeChannel().searchQuery())
			.focus();
	});

	$('#searchBox').on('hidden.bs.modal', function() {
		model.activeChannel().searchQuery("");
	});

	$("#input-prompt")
		.val("")
		.on('blur', function() {
			$(this).removeClass('expanded');
		})
		.on('keydown', function(e) {

			const tab = get_selected_tab();
			const elem = $(this);

			if (e.keyCode == 38 && !e.ctrlKey) {

				if (e.altKey) {
					elem.addClass('expanded').focus();
					return false;
				}

				if (tab && !elem.hasClass('expanded')) {
					const key = tab.getAttribute('connection_id') + ":" + tab.getAttribute('channel');

					if (input_cache[key]) {
						if (input_cache[key]['offset'] > -input_cache[key]['lines'].length)
							--input_cache[key]['offset'];

						const o = input_cache[key]['lines'].length + input_cache[key]['offset'];

						if (input_cache[key]['lines'][o]) {
							elem.val(input_cache[key]['lines'][o])
							elem[0].setSelectionRange(elem.val().length, elem.val().length);
						}
					}
					return false;
				}
			}

			if (e.keyCode == 40 && !e.ctrlKey) {
				if (e.altKey) {
					elem.removeClass('expanded');
					return false;
				}

				if (tab && !elem.hasClass('expanded')) {
					const key = tab.getAttribute('connection_id') + ":" + tab.getAttribute('channel');

					if (input_cache[key]) {

						if (input_cache[key]['offset'] < -1) {
							++input_cache[key]['offset'];

							const o = input_cache[key]['lines'].length + input_cache[key]['offset'];

							if (input_cache[key]['lines'][o]) {
								elem.val(input_cache[key]['lines'][o]);
								elem[0].setSelectionRange(elem.val().length, elem.val().length);
							}

						} else {
							elem.val('');
							input_cache[key]['offset'] = 0;
						}
					}
					return false;
				}
			}

			if (e.keyCode == 9 && input_autocomplete.length > 0) {

				if (input_autocomplete_index > input_autocomplete.length - 1)
					input_autocomplete_index = 0;

				let v = elem.val();

				const tmp = v.split(" ");
				const query = tmp[tmp.length-1];

				v = v.substring(0, v.lastIndexOf(query)) + input_autocomplete[input_autocomplete_index];
				elem.val(v);

				++input_autocomplete_index;
			}

			return hotkey_handler(e);
		})
		.on('keyup', function(e) {
			const val = $(this).val();

			if (e.keyCode != 9)
				window.requestIdleCallback(function () {
					update_emoticon_popup(val);
				});
		})
		.on('keypress', function(e) {
			return send(e);
		})
		.on('paste', function(e) {
			const elem = $(this);

			UploadAndPost.handleClipboard(e);

			setTimeout(function() {
				const hooks = PluginHost.get(PluginHost.HOOK_REWRITE_SEND);
				let str = elem.val();

				for (let i = 0; i < hooks.length; i++) {
					str = hooks[i](str);
				}

				elem.val(str.trim());

				if (elem.val().split("\n").length > 1) elem.addClass("expanded");
			}, 150);

		})
		.focus();

	$("#topic-input")
		.on('click', function() {
			const tab = get_selected_tab();
			if (!tab || $(this).hasClass("disabled")) return;

			$("#topic-input").hide();

			$("#topic-input-real").show();

			setTimeout(function() {
				$("#topic-input-real")
					.val($("#topic-input").attr('title'))
					.addClass('expanded')
					.focus();
			}, 10);
		});

	$("#topic-input-real")
		.on('keypress', function(e) {

			if ((e.keyCode == 13 || e.keyCode == 10) && e.ctrlKey) {
				const elem = $(this);

				const tab = get_selected_tab();
				const topic = elem.val();

				let channel = tab.getAttribute("channel");
				const connection_id = tab.getAttribute("connection_id")

				if (tab.getAttribute("tab_type") == "S") channel = "---";

				const chan = model.getChannel(connection_id, channel);

				if (chan)
					chan.topic(topic);

				const query = {op: 'set-topic', topic: topic, chan: channel,
					connection: connection_id, last_id: last_id};

				console.log(query);

				show_spinner();

				$.post("backend.php", query, function(resp) {
					hide_spinner();
					handle_update(resp);
					elem.blur();
				});

			}
		})
		.on('blur', function() {
			$(this)
				.removeClass('expanded')
				.hide();
			$("#topic-input").show();
		});

	localforage.getItem("ttirc.alerts-disabled").then(function(alerts_disabled) {
		model.alerts_enabled(!alerts_disabled);
	});

	update_filter_muted();

	$(document).on('keydown', function(e) {
		return hotkey_handler(e);
	});

	$("#log").on("swiperight", function() {
		$("#tabs").addClass("active");
	});

	$("#log").on("swipeleft", function() {
		$("#sidebar").addClass("active");
	});

	$("#log").click(function() {
		$("#tabs").removeClass("active");
		$("#sidebar").removeClass("active");
	});

	$("#log").on("scroll", function(/*e*/) {

		if (log_last_scroll_top != 0 && $("#log").scrollTop() < log_last_scroll_top) {
			if ($("#log").scrollTop() <= $("#log").height() / 3) {
				//console.log("onscroll: requesting history");
				model.activeChannel().getHistory(0);
			}
		}

		log_last_scroll_top = $("#log").scrollTop();

	});

	$(window).on('touchstart', function touch_detect() {
		has_touch = true;

		$(window).off('touchstart', touch_detect);
	});

	$(window).on('online', function() {
		console.log("we're online, restarting event source");
		model.backendPing();
		init_event_source();
	});

	$(window).on('offline', function() {
		console.log("we're offline");

		event_source.close();
		model.networkError(true);

	});

	hide_spinner();

	$.post("backend.php",
		{op: 'update', last_id: last_id, uniqid: uniqid,
			init: true, bufsize: default_buffer_size}, function(resp) {

			console.log('update', resp);

			return handle_update(resp).then(function() {
				PluginHost.run(PluginHost.HOOK_INIT_COMPLETE);
				init_event_source();
			});

	}).error(function() {
		PluginHost.run(PluginHost.HOOK_INIT_COMPLETE);

		init_event_source();
	});

}

function init_dnd_upload() {
	$(document).on('dragenter', function(e) {
		if (event_contains_files(e)) {
			$("#dnd_overlay").fadeIn();

			e.stopPropagation();
			e.preventDefault();
		}
	});

	$('#dnd_overlay').on('click', function() {
		$("#dnd_overlay").fadeOut();
	});

	$('#dnd_overlay').on('dragleave', function(e) {

		$("#dnd_overlay").fadeOut();

		e.stopPropagation();
		e.preventDefault();
	});

	$(document).on('dragover', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});

	$(document).on('drop', function (e) {
		e.stopPropagation();
		e.preventDefault();

		$("#dnd_overlay").fadeOut();

		UploadAndPost.handleFiles(e.originalEvent.dataTransfer.files);
	});
}


/* exported init */
function init() {
	console.log('init');

	show_spinner();

	hljs.configure({tabReplace: "&nbsp;", useBR: 1});

	$.ajaxPrefilter(function(options, originalOptions, jqXHR) {

		if (originalOptions.type !== 'post' || options.type !== 'post') {
			return;
		}

		const datatype = typeof originalOptions.data;

		if (datatype == 'object')
			options.data = $.param($.extend(originalOptions.data, {"csrf_token": __csrf_token}));
		else if (datatype == 'string')
			options.data = originalOptions.data + "&csrf_token=" + param_escape(__csrf_token);

	});

	window.onerror = function(message, filename, lineno, colno, error) {
		report_error(message, filename, lineno, colno, error);
	};

	model = new Model();

	ko.applyBindings(model, $('#html-root')[0]);

	// url data cache maintenance
	window.setInterval(function() {
		window.requestIdleCallback(function() {
			const keys = Object.keys(url_data_cache);

			if (keys.length > 64) {

				for (let i = 0; i < 16; i++) {
					const key = keys[parseInt(Math.random()*keys.length)]

					if (key)
						delete url_data_cache[key];
				}
			}

			sessionStorage["ttirc.url_data_cache"] = JSON.stringify(url_data_cache);
		});

	}, 60*1000);

	// image cache maintenance
	window.setInterval(function() {
		window.requestIdleCallback(function() {
			const keys = Object.keys(image_embed_cache);

			if (keys.length > 64) {

				for (let i = 0; i < 16; i++) {
					const key = keys[parseInt(Math.random()*keys.length)]

					if (key)
						delete image_embed_cache[key];
				}
			}

			sessionStorage["ttirc.image_embed_cache"] = JSON.stringify(image_embed_cache);
		});

	}, 60*1000);

	window.setInterval(function() {
		model.backendPing();
	}, 60*1000);

	window.setInterval(function() {
		if (new Date().getTime() - last_update > ping_interval * 3 * 1000) {
			model.networkError(true);
			console.log("evtsource timeout detected; restarting.")
			init_event_source();
		}
	}, 60*1000);

	window.setInterval(function() {
		$.post("backend.php", {op: "expirecaches"}, function(resp) {
			console.log('expirecaches', resp);
		});
	}, 60*60*3*1000);

	/* remove old-style persistent cache items (localstorage instead of sessionstorage) */
	window.requestIdleCallback(() => {
		localforage.removeItem('ttirc.url_data_cache');
		localforage.removeItem('ttirc.image_embed_cache');
	});

	try {
		url_data_cache = JSON.parse(sessionStorage["ttirc.url_data_cache"]);
		image_embed_cache = JSON.parse(sessionStorage["ttirc.image_embed_cache"]);
	} catch (e) {
		console.warn(e);

		url_data_cache = {};
		image_embed_cache = {};
	}

	$(window).on("focus", function() {
		set_window_active(true);
	});

	$(window).on("blur", function() {
		set_window_active(false);
	});

	$.post("backend.php", {op: "init"}, function (resp) {
		init_second_stage(resp);
		init_dnd_upload();
	});
}

async function handle_update(rv) {
	console.log('handle_update: start', rv);

	if (!rv) {
		console.log("received null object from server, will try again.");
		model.networkError(true);
		return true;
	} else {
		model.networkError(false);
	}

	//console.log(rv);

	if (!handle_error(rv, rv)) return false;

	const conn_data = rv[0];
	const lines = rv[1];
	const chandata = rv[2];
	const params = rv[3];

	const handle_params = async (params) => {
		if (params && !params.duplicate) {
			highlight_on = params.highlight_on;

			/* we can't rely on PHP mb_strtoupper() since it sucks cocks */

			for (let i = 0; i < highlight_on.length; i++) {
				highlight_on[i] = highlight_on[i].toUpperCase();
			}

			notify_events = params.notify_events;
			hide_join_part = params.hide_join_part;
			disable_image_preview = params.disable_image_preview;
			uniqid = params.uniqid;
			fetch_url_titles = params.fetch_url_titles;
			master_running = params.master_running;

			if (params.emoticons_mtime)
				model.updateEmoticons(params.emoticons_mtime);
		}
	};

	const handle_initial = async () => {
		// on initial processing some private tabs userlists may not be
		// assigned completely because user information was not yet
		// parsed while tabs were created, so we need to do this one more time
		await handle_chan_data(chandata);

		localforage.getItem("ttirc.current-channel").then(function(item) {
			if (item) {
				item = item.split(":");

				const tab = find_tab(item[0], item[1]);

				if (tab) change_tab(tab);
			}
		});

		startup_date = new Date();

		initial = false;
	};

	const handle_lines = async (lines) => {
		let lines_added = 0;

		for (let i = 0; i < lines.length; i++) {

			try {

				//console.log('line', lines[i]);

				const chan = lines[i].channel;
				const connection_id = lines[i].connection_id;

				//lines[i].message = "(" + lines[i].id + ") " + lines[i].message;

				lines[i].ts = lines[i].ts.replace(/\-/g, '/');
				lines[i].ts = new Date(Date.parse(lines[i].ts));

				if (lines[i].message_type == MSGT_EVENT) {
					if (handle_event(connection_id, lines[i]))
						++lines_added;

				} else if (lines[i].message_type != MSGT_COMMAND) {
					if (push_message(connection_id, chan, lines[i], lines[i].message_type))
						++lines_added;
				}
			} catch (e) {
				console.error("error while processing line", lines[i], e);
			}

			if (last_id < lines[i].id) last_id = lines[i].id;
		}

		return lines_added;
	};

	const handle_buffer = async (lines_added) => {
		if (!get_selected_tab()) {
			change_tab(get_all_tabs()[0]);
		}

		if (lines_added != 0) {
			const scroll_offset = log_bottom_offset();

			update_buffer(scroll_offset);

			if (model.activeChannel() && model.window_active())
				model.activeChannel().saveLastRead();
		}

		console.log('handle_update: done; added=', lines_added);

		return lines_added;
	};

	await handle_params(params);
	await handle_conn_data(conn_data);
	await handle_chan_data(chandata);

	if (initial)
		await handle_initial();

	await handle_buffer(
		await handle_lines(lines));

}

function init_event_source() {
	try {
		if (event_source) {
			console.log("init_event_source: closing existing event source");
			event_source.close();
		}
	} catch (e) {
		console.warn(e);
	}

	last_update = new Date().getTime();

	const url = "backend.php?" + $.param({op: "evtsource", last_id: last_id, uniqid: uniqid, bufsize: default_buffer_size});

	console.log('init_event_source', url);

	event_source = new EventSource(url);

	event_source.addEventListener('ping', (e) => {
		model.networkError(false);

		console.log('evt:ping', JSON.parse(e.data));
		last_update = new Date().getTime();
	});

	event_source.addEventListener('update', (e) => {
		model.networkError(false);

		const data = JSON.parse(e.data);
		last_update = new Date().getTime()
		console.log('evt:update', data);

		if (model.window_active())
			window.requestIdleCallback(function () {
				return handle_update(data);
			});
		else
			return handle_update(data);

	});

	event_source.onerror = (e) => {
		// this does not necessarily mean a network error - eventsource
		// returns this when server closed this particular session (limited by wait timeout)

		console.warn('evt:onerror', e);
		model.backendPing();
	};
}

function get_selected_tab() {
	return $("#tabs-list li.selected")[0];
}

function get_all_tabs(connection_id) {
	let tabs;
	const rv = [];

	if (connection_id) {
		tabs = $("#tabs-" + connection_id + " li[id*=tab-]");

		const conntab = $("#tab-" + connection_id)[0];
		if (conntab) rv.push(conntab);
	} else {
		tabs = $("#tabs-list li[id*=tab-]");
	}

	$.each(tabs, function(i, t) {
		rv.push(t);
	});

	return rv;
}

function get_visible_tabs() {
	return $.grep(get_all_tabs(),
		function(e) { if ($(e).is(":visible")) return e })
}

function toggle_away_visible() {
	model.away_visible(!model.away_visible());
}

/* exported toggle_chanmute */
function toggle_chanmute() {

	const tab = get_selected_tab();

	if (!tab) return;

	$.post("backend.php", { op: 'chanmute',
		connection_id: tab.getAttribute("connection_id"),
		channel: tab.getAttribute("channel")}, function (data) {

			console.log(data);

			hide_spinner();

			const chan = model.getChannel(data.connection_id, data.channel);

			if (chan) chan.muted(data.muted);

	});

	show_spinner();

}

/* exported toggle_filter_muted */
function toggle_filter_muted() {
	localforage.getItem("ttirc.filter-muted").then(function(filter) {
		filter = !filter;

		localforage.setItem("ttirc.filter-muted", filter).then(function() {
			update_filter_muted();
		});
	});
}

// TODO: rework via model
function update_filter_muted() {
	localforage.getItem("ttirc.filter-muted").then(function(filter) {

		const elem = $("#filter-muted-prompt");

		if (filter) {
			$('body').addClass("filter_muted");

			elem.removeClass("text-muted");
			elem.addClass("text-success");
		} else {
			$('body').removeClass("filter_muted");

			elem.addClass("text-muted");
			elem.removeClass("text-success");

		}
	});
}

// scroll_offset determines if we scroll buffer to bottom
// custom value may be passed to test pre-update scroll position instead of post-update one
function update_buffer(scroll_offset) {

	const log = $("#log");

	if (scroll_offset == undefined)
		scroll_offset = log.prop('scrollHeight') - log.prop('clientHeight') - log.scrollTop();

	const scroll_to_bottom = scroll_offset <= 512;

	if (scroll_to_bottom) window.setTimeout(function() {
		log.scrollTop(log.prop('scrollHeight'));
	}, 100);

	update_filter_muted();
}

/*function send_topic(evt) {

	if (e.keyCode == 13 && e.ctrlKey) {
		var tab = get_selected_tab();

		if (!tab || elem.disabled) return;

		var topic = elem.value;

		var channel = tab.getAttribute("channel");
		var connection_id = tab.getAttribute("connection_id")

		if (tab.getAttribute("tab_type") == "S") channel = "---";

		var chan = model.getChannel(connection_id, channel);

		if (chan)
			chan.topic(topic);

		var query = {op: 'set-topic', topic: topic, chan: channel,
			connection: connection_id, last_id: last_id};

		console.log(query);

		show_spinner();

		$.post("backend.php", query, function(resp) {
			hide_spinner();
			handle_update(resp);
			elem.blur();
		});

	}

}*/

function send(e) {

	const key = e.keyCode;
	const elem = $("#input-prompt");

	if ((key == 13 || key == 10) && (e.ctrlKey || !elem.hasClass('expanded'))) {

		if (e.ctrlKey && !elem.hasClass("expanded")) {

			if (elem.val() != "") elem.val(elem.val() + "\n");

			elem.addClass("expanded").focus();
			return true;
		}

		const tab = get_selected_tab();

		if (!tab) return;

		let channel = model.activeChannel();

		if (channel) {
			channel.unread(0);
			channel.highlight(false);
		}

		const dst_channel = tab.getAttribute("channel");

		if (tab.getAttribute("tab_type") == "S") channel = "---";

		if (elem.val().trim() == "/clear") {
			model.activeChannel().lines.removeAll();

		} else {
			let message = elem.val();

			const hooks = PluginHost.get(PluginHost.HOOK_REWRITE_SEND);
			for (let i = 0; i < hooks.length; i++) {
				message = hooks[i](message);
			}

			show_spinner();

			const query = {op: 'send', message: message, chan: dst_channel, kind: DEVICE_KIND,
				connection: tab.getAttribute("connection_id"), send_only: true,
				uniqid: uniqid, last_id: last_id, tab_type: tab.getAttribute("tab_type")};

			console.log(query);

			elem.removeClass('expanded');

			$.post("backend.php", query, function(resp) {
				hide_spinner();
				handle_update(resp);
			});
		}

		window.setTimeout(function() {
			$("#log").scrollTop($("#log").prop('scrollHeight'));
		}, 100);

		$("#log").scrollTop($("#log").prop('scrollHeight'));

		push_cache(tab.getAttribute('connection_id'), dst_channel, elem.val());
		elem.value = '';
		//console.log(query);

		set_window_active(true);

		window.setTimeout(function() {
			elem.val('');
		}, 5);

		return false;
	}

}

function handle_error(obj, resp) {
	if (obj && obj.error) {
		return fatal_error(obj.error, obj.errormsg, resp);
	}
	return true;
}

function change_tab(elem, event) {

	if (!elem) return;

	if (event) event.stopPropagation();

	model.saveLastVisibleId();

	$("#tabs").removeClass("active");

	console.log("changing tab to " + elem.id);

	if (!initial) {
		localforage.setItem("ttirc.current-channel",
			elem.getAttribute("connection_id") + ":" + elem.getAttribute("channel"));
	}

	const channel = elem.getAttribute("channel");
	const connection_id = elem.getAttribute("connection_id");

	model.activeChannel(connection_id, channel);

	$("#log").scrollTop($("#log").prop('scrollHeight'));

	window.requestIdleCallback(function() {
		update_buffer();
	});

	if (!has_touch) $("#input-prompt").focus();

	PluginHost.run(PluginHost.HOOK_TAB_SWITCHED, elem);
}

function toggle_connection(connection_id, set_enabled) {

	const query = {op: 'toggle-connection', set_enabled: set_enabled,
		connection_id: connection_id};

	console.log(query);

	show_spinner();

	$.post("backend.php", query, function () {
		hide_spinner();
	});
}

async function handle_conn_data(conndata) {
	if (conndata != "") {
		if (conndata.duplicate) return;

		const valid_ids = [];

		for (let i = 0; i < conndata.length; i++) {

			const conn = model.getConnection(conndata[i].id);

			if (conn) {
				await conn.update(conndata[i]);
			} else {
				model.connections.push(new Connection(conndata[i]));
			}

			valid_ids.push(conndata[i].id);

		}

		await model.cleanupConnections(valid_ids);
	}
}

async function handle_chan_connection_entry(chandata, connection_id, chan) {
	const conn = model.getConnection(connection_id);

	let tab_type = "P";

	switch (parseInt(chandata[connection_id][chan].chan_type)) {
	case 0:
		tab_type = "C";
		break;
	case 1:
		tab_type = "P";
		break;
	}

	if (conn) {
		let channel = model.getChannel(connection_id, chan);

		if (channel) {
			channel.title(chan);
			channel.type(tab_type);
		} else {
			channel = new Channel(connection_id, chan, tab_type);
			conn.channels.push(channel);
		}

		if (chandata[connection_id][chan]["muted"] != channel.muted())
			channel.muted(chandata[connection_id][chan]["muted"]);

		if (chandata[connection_id][chan]["topic"] != channel.topic())
			channel.topic(chandata[connection_id][chan]["topic"]);

		const last_read = chandata[connection_id][chan]["last_read"];

		if (last_read > 0 && channel.last_read() < last_read) {
			console.log(chan, 'setting last read to', last_read);
			channel.last_read(last_read);
		}

		if (tab_type == "C") {
			if (!channel.nicklist() || !channel.nicklist().equals(chandata[connection_id][chan]["users"]))
				channel.nicklist(chandata[connection_id][chan]["users"]);
		} else if (tab_type == "P") {
			const tmp_nicklist = ['@' + conn.active_nick()];

			if (conn.nickExists(chan))
				tmp_nicklist.push(chan);

			if (!channel.nicklist() || !channel.nicklist().equals(tmp_nicklist))
				channel.nicklist(tmp_nicklist);
		}
	}
}

async function handle_chan_data(chandata) {
	if (chandata != "") {
		if (chandata.duplicate) return;

		for (const connection_id in chandata) {

			const conn = model.getConnection(connection_id);

			if (!conn) continue;

			const valid_channels = [];

			for (const chan in chandata[connection_id]) {
				await handle_chan_connection_entry(chandata, connection_id, chan);

				valid_channels.push(chan);
			}

			if (conn.channels() && !conn.channels().equals(valid_channels)) {
				await model.cleanupChannels(connection_id, valid_channels);

				conn.channels.sort(function(a, b) {
					return a.title().localeCompare(b.title());
				});
			}
		}
	}
}

function send_command(command) {

	const tab = get_selected_tab();

	if (tab) {

		let channel = tab.getAttribute("channel");

		if (tab.getAttribute("tab_type") == "S") channel = "---";

		const query = {op: 'send', message: command,
			channel: channel, connection: tab.getAttribute("connection_id"),
			last_id: last_id};

		console.log(query);

		show_spinner();

		$.post("backend.php", query, function (resp) {
			hide_spinner();
			handle_update(resp);
		});
	}
}

/* exported change_nick */
function change_nick() {
	const nick = prompt("Enter new nickname:");

	if (nick) send_command("/nick " + nick);
}

/* exported join_channel */
function join_channel() {
	const channel = prompt("Channel to join:");

	if (channel) send_command("/join " + channel);
}

/* exported close_tab */
function close_tab(elem) {

	if (!elem) return;

	const tab = elem.parentNode;

	if (tab && confirm(__("Close this tab?"))) {

		const query = {op: 'part-channel', chan: tab.getAttribute("channel"),
			connection: tab.getAttribute("connection_id"),
			last_id: last_id};

		console.log(query);

		show_spinner();

		$.post("backend.php", query, function (resp) {
			handle_update(resp);
			hide_spinner();
		});
	}
}

/* exported query_user */
function query_user(elem) {

	if (!elem) return;

	const tab = get_selected_tab();
	const nick = elem.getAttribute("nick");
	const pr = __("Start conversation with %s?").replace("%s", nick);

	if (tab && confirm(pr)) {

		const query = {op: 'query-user', nick: nick,
			connection: tab.getAttribute("connection_id"),
			last_id: last_id};

		console.log(query);

		show_spinner();

		$.post("backend.php", query, function (resp) {
			handle_update(resp);
			hide_spinner();
		});

	}
}

function handle_event(connection_id, line) {
	if (!line.message) return;

	const params = line.message.split(":", 4);

	//console.log('handle_event', params, 'chan', line.channel);

	console.log(line);

//		console.log("handle_event " + params[0]);

	switch (params[0]) {
	case "ERROR":
		line.message = __("Error: %s (%d)").
			replace("%d", params[1]).
			replace("%s", params[2]);

		return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);

	case "CONNECTION_ERROR":
		line.message = __("Error connecting to %s:%p (%e)").
			replace("%s", params[1]).
			replace("%p", params[2]).
			replace("%e", params[3]);

		line.sender = "---";

		return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
	case "SERVER_PONG": {
		const conn = model.getConnection(connection_id);

		if (conn)
				conn.rtt(params[1]);

/*		line.message = __("Received server pong: %ds").replace("%d", params[1]);
		line.sender = "---";

		push_message(connection_id, line.channel, line, MSGT_PRIVMSG); */

		}
		break;
	case "AUTOAWAY_USER_IDLE":
		line.message = __("Automatically setting away: no users connected.");

		return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
	case "DISCONNECT_USER_IDLE":
		line.message = __("Disconnecting from server: no users connected.");

		return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
	case "DISCONNECT_PING_TIMEOUT":

		line.message = __("Disconnecting from server: ping timeout.");

		return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
	case "TOPIC":
		{
			const topic = line.message.replace("TOPIC:", "");

			line.message = __("%u has changed the topic to: %s").replace("%u", line.sender);
			line.message = line.message.replace("%s", topic);
			line.sender = "---";

			return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
		}
	case "MODE":
		{
			const mode = params[1];
			const subject = params[2];

			let msg_type;

			if (mode) {
				line.message = __("%u has changed mode [%m] on %s").replace("%u",
						line.sender);
				line.message = line.message.replace("%m", mode);
				line.message = line.message.replace("%s", subject);
				line.sender = "---";

				msg_type = MSGT_PRIVMSG;
			} else {
				line.sender = "---";

				line.message = __("%u has changed mode [%m]").replace("%u",
						line.channel);
				line.message = line.message.replace("%m", subject);

				msg_type = MSGT_BROADCAST;
			}

			return push_message(connection_id, line.channel, line, msg_type, hide_join_part);
		}
	case "KICK":
		{
			const nick = params[1];
			const message = params[2];

			line.message = __("%u has been kicked from %c by %n (%m)").replace("%u", nick);
			line.message = line.message.replace("%c", line.channel);
			line.message = line.message.replace("%n", line.sender);
			line.message = line.message.replace("%m", message);
			line.sender = "---";

			return push_message(connection_id, line.channel, line, MSGT_PRIVMSG);
		}
	case "PART":
		{
			const nick = params[1];
			const message = params[2];

			line.message = __("%u has left %c (%m)").replace("%u", nick);
			line.message = line.message.replace("%c", line.channel);
			line.message = line.message.replace("%m", message);

			return push_message(connection_id, line.channel, line, MSGT_PRIVMSG, hide_join_part);
		}
	case "JOIN":
		{
			const nick = params[1];
			const host = params[2];

			line.message = __("%u (%h) has joined %c").replace("%u", nick);
			line.message = line.message.replace("%c", line.channel);
			line.message = line.message.replace("%h", host);
			line.message_type = MSGT_SYSTEM;

			push_message(connection_id, line.channel, line, MSGT_PRIVMSG, hide_join_part);

			const conn = model.getConnection(connection_id);

			if (conn && conn.active_nick() == nick) {
				const chan = model.getChannel(connection_id, line.channel);

				if (chan /*&& chan.lines().length >= 10*/) {
					chan.synthesizeTopicEvent(chan._topic(), true);
				}
			}

			return true;
		}
	case "QUIT":
		{
			const quit_msg = line.message.replace("QUIT:", "");

			line.message = __("%u has quit IRC (%s)").replace("%u", line.sender);
			line.message = line.message.replace("%s", quit_msg);
			line.message_type = MSGT_SYSTEM;

			return push_message(connection_id, line.channel, line, MSGT_PRIVMSG, hide_join_part);
		}
	case "DISCONNECT":
		line.message = __("Connection terminated.");

		if (last_id > last_old_id && notify_events[3])
			notify("Disconnected from server.");

		return push_message(connection_id, '---', line);
	case "CONNECTION_THROTTLED":
		line.message = __("Connection queued, please wait...");

		return push_message(connection_id, '---', line);
	case "REQUEST_CONNECTION":
		line.message = __("Requesting connection...");

		return push_message(connection_id, '---', line);
	case "CONNECTING":
		{
			const server = params[1];
			const port = params[2];

			line.message = __("Connecting to %s:%d...").replace("%s", server);
			line.message = line.message.replace("%d", port);

			return push_message(connection_id, '---', line);
		}
	case "PING_REPLY":
		{
			const args = params[1];

			line.message = __("Ping reply from %u: %d second(s).").replace("%u",
					line.sender);
			line.message = line.message.replace("%d", args);
			line.message_type = MSGT_SYSTEM;

			const tab = get_selected_tab();

			if (!tab) get_all_tabs()[0];

			if (tab) {
				const chan = tab.getAttribute("channel");
				line.channel = chan;
			}

			return push_message(connection_id, line.channel, line);
		}
	case "NOTICE":
		{
			const message = params[1];

			line.message = message;
			line.message_type = MSGT_NOTICE;

			if (line.channel != "---")
				return push_message(connection_id, line.sender, line);
			else
				return push_message(connection_id, line.channel, line);
		}
	case "CTCP":
		{
			const command = params[1];
			const args = params[2];

			line.message = __("Received CTCP %c (%a) from %u").replace("%c", command);
			line.message = line.message.replace("%a", args);
			line.message = line.message.replace("%u", line.sender);
			line.message_type = MSGT_SYSTEM;

			return push_message(connection_id, '---', line);
		}
	case "CTCP_REPLY":
		{
			const command = params[1];
			const args = params[2];

			line.message = __("CTCP %c reply from %u: %a").replace("%c", command);
			line.message = line.message.replace("%a", args);
			line.message = line.message.replace("%u", line.sender);
			line.message_type = MSGT_SYSTEM;

			return push_message(connection_id, line.channel, line);
		}
	case "PING":
		{
			const args = params[1];

			line.message = __("Received ping (%s) from %u").replace("%s", args);
			line.message = line.message.replace("%u", line.sender);
			line.message_type = MSGT_SYSTEM;

			return push_message(connection_id, '---', line, MSGT_BROADCAST);
		}
	case "CONNECT":
		line.message = __("Connection established.");

		if (last_id > last_old_id && notify_events[3])
			notify("Connected to server.");

		return push_message(connection_id, '---', line);
	case "UNKNOWN_CMD":
		line.message = __("Unknown command: /%s.").replace("%s", params[1]);
		return push_message(connection_id, "---", line, MSGT_PRIVMSG);
	case "NICK":
		{
			const new_nick = params[1];
			const chan = model.getChannel(connection_id, line.sender);

			if (chan) chan.title(new_nick);

			line.message = __("%u is now known as %n").replace("%u", line.sender);
			line.message = line.message.replace("%n", new_nick);
			line.message_type = MSGT_SYSTEM;

			return push_message(connection_id, line.channel, line, MSGT_PRIVMSG, hide_join_part);
		}
	default:
		PluginHost.run(PluginHost.HOOK_HANDLE_EVENT, line);
		break;
	}
}

function push_message(connection_id, channel, message, message_type, no_tab_hl) {
	if (!model.getConnection(connection_id)) return;

	if (no_tab_hl == undefined) no_tab_hl = false;

	if (!message_type) message_type = MSGT_PRIVMSG;

	if (message.id > 0 && id_history.indexOf(message.id) != -1 && !message.force_display)
		return false; // dupe

	id_history.push(message.id);

	if (id_history.length > MAX_HISTORY_LINES + 128)
		id_history = id_history.splice(-MAX_HISTORY_LINES);

	if (message_type != MSGT_BROADCAST) {
		const chan = model.getChannel(connection_id, channel);

		if (chan && chan.lines) {

			chan.lines.push(new Message(message, chan));

			const ts_short = message.ts.getYear() + '-' + message.ts.getMonth() + '-' + message.ts.getDate();

			if (message.id && chan.dateMap().indexOf(ts_short) == -1) {
				chan.dateMap().push(ts_short);

				const dchmsg = {
					id: 0,
					sender: "---",
					message_type: MSGT_SYSTEM,
					ts: new Date(message.ts),
					channel: channel,
					incoming: true,
					connection_id: connection_id,
				};

				dchmsg.ts.setHours(0, 0, 0);

				dchmsg.message = __("Date changed to %d").replace("%d",
						dchmsg.ts.toLocaleString('en-US',
							{weekday: "long", month: "long", day: "numeric"}));

				chan.lines.push(new Message(dchmsg, chan));
			}

			if (chan.lines().length > MAX_HISTORY_LINES)
				while (chan.lines().length > MAX_HISTORY_LINES - 64)
					chan.lines.shift();

		}

		const tab = find_tab(connection_id, channel);

		if (new Date().getTime() - new Date(message.ts).getTime() < NOTIFY_MESSAGE_MAX_AGE*1000) {
			if (!no_tab_hl && tab && (get_selected_tab() != tab || !model.window_active())) {

				if (notify_events[1] || notify_events[4]) {
					const is_hl = notify_events[4] || is_highlight(connection_id, message);

					if (is_hl) {
						let msg = __("(%c) %n: %s");

						msg = msg.replace("%c", message.channel);
						msg = msg.replace("%n", message.sender);
						msg = msg.replace("%s", message.message);

						if (message.sender && message.channel && message.sender != "---" &
								message.sender != model.getConnection(connection_id).active_nick()) {

							notify(msg, channel);
						}
					}
				}

				if (notify_events[2] && tab.getAttribute("tab_type") == "P" && message.id > last_old_id) {
					let msg = __("%n: %s");

					msg = msg.replace("%n", message.sender);
					msg = msg.replace("%s", message.message);

					if (message.sender && message.sender != model.getConnection(connection_id).active_nick()) {
						notify(msg);
					}

				}

				if (message_type == MSGT_PRIVMSG && !no_tab_hl && notify_events[4] && tab.getAttribute("tab_type") == "C" && message.id > last_old_id) {
					let msg = __("(%c) %n: %s");

					msg = msg.replace("%n", message.sender);
					msg = msg.replace("%s", message.message);
					msg = msg.replace("%c", message.channel);

					if (message.sender && message.channel &&
							message.sender != model.getConnection(connection_id).active_nick()) {

						notify(msg);
					}
				}

			}
		}

		// do not highlight system tab on server PONG
		if (message.sender == "---" && message.message.indexOf("pong") != -1)
			no_tab_hl = true;

		if (!no_tab_hl && message.ts > startup_date)
			highlight_tab_if_needed(connection_id, channel, message);

	} else {
		const tabs = get_all_tabs(connection_id);

		for (let i = 0; i < tabs.length; i++) {

			if (tabs[i].getAttribute("tab_type") == "C") {
				const chan = model.getChannel(connection_id, tabs[i].getAttribute("channel"));

				if (chan && chan.lines) {
					chan.lines.push(new Message(message, chan));
				}
			}
		}
	}

	return true;
}

function set_window_active(active) {
	console.log("set_window_active: " + active);

	model.window_active(active);

	if (active) {
		$('body').addClass("active");

		model.new_messages(0);
		model.new_highlights(0);

		const tab = get_selected_tab();

		if (tab) {

			const channel = model.activeChannel();

			if (channel) {
				channel.unread(0);
				channel.highlight(false);
				channel.saveLastRead();
			}

		}

		notifications_close();

		$("#input-prompt").focus();

	} else {
		$('body').removeClass("active");

		model.saveLastVisibleId();
	}

	PluginHost.run(PluginHost.HOOK_WINDOW_ACTIVE, active);
}

/* exported emoticons_popup */
function emoticons_popup() {

	const w = 90 * 9 + 60;

	const left = screen.width/2 - w/2;
	const top = screen.height/2 - 200;

	window.open("backend.php?op=emoticons_list",
		"_ttirc_emhelp",
		"width="+w+",height=400,resizable=yes,status=no,location=no,menubar=no,directories=no,scrollbars=yes,toolbar=no,left=" + left + ",top=" + top);

	return false;
}

/* exported url_clicked */
function url_clicked(elem, event) {
	if (navigator.userAgent && navigator.userAgent.match("MSIE"))
		return true;

	if (!is_media(elem.href.toLowerCase()))
		return true;

	if (event.ctrlKey)
		return true;

	const override = $(elem).attr('data-url-override');

	if (disable_image_preview || DEVICE_KIND == "mobile") {
		if (override) {
			window.open(override);
			return false;
		}
		return true;
	}

	event.stopPropagation();

	const width = screen.width/2;
	const height = width * 3/4;

	const left = screen.width/2 - width/2;
	const top = screen.height/2 - height/2;

	const w = window.open("",
		"_ttirc_preview",
		"width="+width+",height="+height+",resizable=yes,status=no,location=no,menubar=no,directories=no,scrollbars=yes,toolbar=no,left=" + left + ",top=" + top);

	if (override && override.match("backend.php")) {
		const timestamp = parseInt(new Date().getTime()/1000);
		sessionStorage["ttirc.visited." + elem.href.hashCode()] = timestamp;

		const kd = lookup_ko_data(elem);

		if (kd && kd.message) kd.message.notifySubscribers();
	}

	w.opener = null;
	w.location = override ? override : elem.href;

	return false;

}

function hotkey_handler(e) {

	const keycode = e.keyCode;
	//const keychar = String.fromCharCode(keycode);

	if (keycode == 27) { // escape
		$("#infoBox").modal('hide');
		$("#main").removeClass("fade");
		$("#dnd_overlay").fadeOut();
		$("#input-prompt").blur();
		$("#topic-input-real").blur();
	}

	if ($(".modal").is(":visible")) {
		console.log("hotkeys: modal is visible");
		return;
	}

	//console.log('KC', keycode, e.ctrlKey, e.altKey, e.shiftKey);

	if (keycode == 70 && e.ctrlKey) {
		$("#searchBox").modal().show();
		return false;
	}

	if (keycode == 70 && e.altKey && e.shiftKey) {
		toggle_filter_muted();
		return false;
	}

	if (keycode == 77 && e.altKey && e.shiftKey) {
		toggle_chanmute();
		return false;
	}

	if (keycode == 77 && e.altKey) {
		toggle_alerts();
		return false;
	}

	if (keycode == 79 && e.altKey && e.shiftKey) {
		toggle_away_visible();
		return false;
	}

	if (keycode == 80 && e.altKey) {
		Prefs.show();
		return false;
	}

	if (keycode == 85 && e.altKey && e.shiftKey) {
		$("#upload_and_post input[type='file']").click();
		return false;
	}

	if (keycode == 74 && e.altKey) {
		join_channel();
		return false;
	}

	if (keycode == 88 && e.altKey && e.ctrlKey) {
		window.location.href = "backend.php?op=logout";
		return false;
	}

	if (keycode == 69 && e.altKey && e.shiftKey) {
		emoticons_popup();
		return false;
	}

	if (keycode == 69 && e.altKey) {
		toggle_emoticons();
		return false;
	}

	if (keycode == 38 && e.ctrlKey) {
		console.log("moving up...");

		const tabs = get_visible_tabs();
		const tab = get_selected_tab();

		if (tab) {
			for (let i = 0; i < tabs.length; i++) {
				if (tabs[i] == tab) {
					change_tab(tabs[i-1]);
					return false;
				}
			}
		}

		return false;
	}

	if (keycode == 40 && e.ctrlKey) {
		console.log("moving down...");

		const tabs = get_visible_tabs();
		const tab = get_selected_tab();

		if (tab) {
			for (let i = 0; i < tabs.length; i++) {
				if (tabs[i] == tab) {
					change_tab(tabs[i+1]);
					return false;
				}
			}
		}

		return false;
	}

	if (keycode == 76 && e.ctrlKey) {
		if (model.activeChannel())
			model.activeChannel().lines.removeAll();

		return false;
	}

	if (keycode == 9) {
		$("#input-prompt").focus();
		return false;
	}

	if (PluginHost.run(PluginHost.HOOK_GLOBAL_HOTKEY, e))
		return true;

	return true;

}

function push_cache(connection_id, channel, line) {

	if (line.length == 0) return;

	const key = connection_id + ":" + channel;

	if (!input_cache[key]) {
		input_cache[key] = [];
		input_cache[key]['lines'] = [];
	}

	input_cache[key]['offset'] = 0;

	for (let i = 0; i < input_cache.length; i++) {
		if (input_cache[key]['lines'][i] == line) return;
	}

	input_cache[key]['lines'].push(line);

	while (input_cache.length > 100)
		input_cache[key]['lines'].shift();

}

function is_highlight(connection_id, message) {
	const message_text = message.message.toUpperCase();

	if (message.message_type == MSGT_SYSTEM)
		return false;

	if (message.sender == "---" || message.sender == model.getConnection(connection_id).active_nick())
		return false;

	if (message.id <= last_old_id)
		return false;

	if (message_text.indexOf("://") != -1)
		return false;

	const hl = highlight_on.slice(0);

	if (model.getConnection(connection_id))
		hl.push(model.getConnection(connection_id).active_nick())

	for (let i = 0; i < hl.length; i++) {
		const r = new RegExp("(^| )" + RegExp.escape(hl[i].toUpperCase()) + "([,:\.\; ]|$)");

		if (hl[i].length > 0 && r.exec(message_text.toUpperCase()))
			return true;
	}

	if (PluginHost.run(PluginHost.HOOK_IS_HIGHLIGHT, [connection_id, message]))
		return true;

	return false;
}

function highlight_tab_if_needed(connection_id, channel, message) {
	console.log("highlight_tab_if_needed " + connection_id + " " + channel + " " + message.sender);

	const chan = model.getChannel(connection_id, channel);
	let is_hl = false;
	const muted = chan ? chan.muted() : false;
	const nick = model.getConnection(connection_id).active_nick();

	if (!message.incoming || nick == message.sender) return;

	if (chan && !muted && (chan != model.activeChannel() || !model.window_active())) {
		if (message.id < chan.last_read())
			return;

		if (chan.type() != "S") {
			if (is_highlight(connection_id, message)) {
				chan.highlight(true);
				model.new_highlights(model.new_highlights()+1);
				model.new_messages(model.new_messages()+1);
				is_hl = true;
			}
		}

		chan.unread(chan.unread()+1);
	}

	if (!model.window_active() && !muted && !is_hl) model.new_messages(model.new_messages()+1);
}

function find_tab(connection_id, channel) {

	//console.log("find_tab : " + connection_id + ", " + channel);

	if (channel == '---') {
		return $("#tab-" + connection_id)[0];
	} else {
		return $("#tabs-list li").filter(function() {
			return $(this).attr('connection_id') == connection_id && $(this).attr('channel') == channel
		} )[0];
	}
}

/* exported inject_text */
function inject_text(str) {

	const i = $("#input-prompt");

	const caret = i[0].selectionStart;
	const text = i.val();

	i.val(text.substring(0, caret) + str + text.substring(caret))
		.focus();
}

/*
function input_filter_cr(elem, event) {
	if (elem.value.length > 30)
		while (elem.value[elem.value.length-1] == '\n')
			elem.value = elem.value.substr(0, elem.value.length - 1);

}*/

function update_emoticon_popup(str) {
	const tmp = str.split(" ");
	const query = tmp[tmp.length-1];

	input_autocomplete = [];
	input_autocomplete_index = 0;

	if (query && query.length > 2 && query[0] == ":") {
		//$(".emoticons-popup").html(word).show();

		const list = $(".emoticons-popup");

		list.html("");

		let found = 0;
		$.each(Object.keys(model.emoticons_map()), function(i, k) {
			if (k.indexOf(query) == 0) {
				++found;

				input_autocomplete.push(k);

				const li = $("<li>")
					.on("click", function() {
						const emot_name = $(this).find("span").html();
						const input = $("#input-prompt");
						let v = input.val();

						v = v.substring(0, v.lastIndexOf(query)) + emot_name;
						input.val(v);

					})
					.append($("<img>")
						.attr("src", "emoticons/" + model.emoticons_map()[k].file))
					.append($("<span>")
						.html(k))


				list.append(li);
			}

			if (found == 5)
				return false;
		});

		if (found > 0)
			list.fadeIn();
		else
			list.hide();

	} else {
		$(".emoticons-popup").hide();
	}

}

function toggle_emoticons() {
	model.emoticons_visible(!model.emoticons_visible());
}

/* exported toggle_alerts */
function toggle_alerts() {
	model.alerts_enabled(!model.alerts_enabled());
	localforage.setItem("ttirc.alerts-disabled", !model.alerts_enabled());
}

function event_contains_files(event) {
	let temp = (event.originalEvent || event).dataTransfer;

	return temp && (temp = temp.types) && temp[0] === 'Files';
}

/* exported log_bottom_offset */
function log_bottom_offset() {
	const log = $("#log");
	return log.prop('scrollHeight') - log.prop('clientHeight') - log.scrollTop();
}
