'use strict';

/* global ko, PluginHost, __ */
/* eslint-disable no-redeclare,no-useless-escape */

const EMBED_MAX = 2048;
const EMBED_SCALED_MAX = 250; /* corresponds to default.less */
const IMAGE_THUMB = 250;

let spinner_refs = 0;

RegExp.escape = function(str) {
	return String(str).replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
};

String.prototype.hashCode = function() {
	return this.split("").reduce((a,b) =>
		{
			a=((a<<5)-a)+b.charCodeAt(0);
			return a & a
		}, 0);
}

/* add method to remove element from array */
Array.prototype.remove = function(s) {
	for (let i=0; i < this.length; i++) {
		if (s == this[i]) this.splice(i, 1);
	}
}

Array.prototype.nickIndexOf=function(s) {
	for (let i = 0; i < this.length; i++) {
		const tmp = this[i].replace(/^[@+]/, "");

		if (tmp == s || this[i] == s) return i;
	}
	return -1;
}

Array.prototype.last = function(){
	return this[this.length - 1];
};

Array.prototype.equals = function (array) {
	// if the other array is a falsy value, return
	if (!array)
		return false;

	// compare lengths - can save a lot of time
	if (this.length != array.length)
		return false;

	for (let i = 0, l=this.length; i < l; i++) {
		// Check if we have nested arrays
		if (this[i] instanceof Array && array[i] instanceof Array) {
			// recurse into the nested arrays
			if (!this[i].equals(array[i]))
				return false;
		}
		else if (this[i] != array[i]) {
			// Warning - two different object instances will never be equal: {x:20} != {x:20}
			return false;
		}
	}
	return true;
}

Object.defineProperty(Array.prototype, "equals", {enumerable: false});

String.prototype.escapeHTML=function() {
	const entityMap = {
		'<': '&lt;',
		'>': '&gt;',
	};

  return this.replace(/[<>]/g, function (s) {
    return entityMap[s];
  });
}

String.prototype.safeReplace = function(regex, repfunc, nocheck) {

	if (!nocheck && !this.match(regex))
		return this;

	let tmp = document.createElement('span');
	tmp.innerHTML = this;
	tmp = $(tmp);

	$.each(tmp.contents(), function (i, e) {

		if (e.nodeType == 3) {
			let str = e.nodeValue;

			let matches;
			/* eslint-disable no-cond-assign */
			while (matches = regex.exec(str)) {
			/* eslint-enable no-cond-assign */
				const match = matches[0];
				const idx = str.indexOf(match);

				const prefix = str.substr(0, idx);
				const suffix = str.substr(idx + match.length);

				const repl = typeof repfunc == 'function' ? repfunc(matches) : repfunc;
				let repl_node;

				if (repl != null && match != repl) {
					if (repl.indexOf("<") != -1) {
						repl_node = $("<span>").append(repl)[0];

						/*if (repl_node.firstChild)
							repl_node = repl_node.firstChild;*/

					} else {
						repl_node = document.createTextNode(repl);
					}
				} else {
					repl_node = document.createTextNode(match);
				}

				const e_prefix = document.createTextNode(prefix);
				const e_suffix = document.createTextNode(suffix);

				e.parentNode.insertBefore(e_suffix, e);
				e.parentNode.insertBefore(repl_node, e_suffix);
				e.parentNode.insertBefore(e_prefix, repl_node);
				e.parentNode.removeChild(e);

				e = e_suffix;
				str = e.nodeValue;

			}

		}
	});

	return tmp.html();
}

function report_error(message, filename, lineno, colno, error) {
	try {

		const ebc = $("#errorBox").modal().find('.modal-body');

		if (filename && lineno && colno)
			message += "<br>(" + filename + ":" + lineno + "," + colno + ")";

		ebc.html("<p class='alert alert-danger'>" + message + "</p>");

		if (error) {
			ebc.html(ebc.html() + "<div><b>Stack trace:</b></div>" +
				"<textarea class='form-control' readonly=\"1\">" + error.stack + "</textarea>");
		}

	} catch (e) {
		console.error(message, filename, lineno, colno, error, e);
	}
}

function param_escape(arg) {
	if (typeof encodeURIComponent != 'undefined')
		return encodeURIComponent(arg);
	else
		return escape(arg);
}

/* exported param_unescape */
function param_unescape(arg) {
	if (typeof decodeURIComponent != 'undefined')
		return decodeURIComponent(arg);
	else
		return unescape(arg);
}

/* exported getURLParam */
function getURLParam(name){
	try {
		const results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return decodeURIComponent(results[1].replace(/\+/g, " ")) || 0;
	} catch (e) {
		return 0;
	}
}

/* exported fatal_error */
function fatal_error(code, message, resp) {
	console.error('fatal_error', code, message, resp);

	// lost redis or database connection, could be temporary
	if (code == 15 || code == 16) {
		model.networkError(true);
	} else if (code == 6) {
		window.location.href = "index.php";
	} else if (code == 5) {
		window.location.href = "update.php";
	} else {

		if (!message) message = "Unknown error";

		message += " (code " + code + ")";

		report_error(message, '', 0, 0, null);
	}
}

/* exported infobox_callback2 */
function infobox_callback2(resp) {
	$("#infoBox").modal().find(".modal-content").html(resp);
}

/* exported show_spinner */
function show_spinner() {
	$("#spinner").show();

	++spinner_refs;
}

/* exported hide_spinner */
function hide_spinner() {
	if (spinner_refs > 0) spinner_refs--;

	if (!spinner_refs)
		$("#spinner").hide();
}

/* exported set_cookie */
function set_cookie(name, value, lifetime, path, domain, secure) {

	let d = false;

	if (lifetime) {
		d = new Date();
		d.setTime(d.getTime() + (lifetime * 1000));
	}

	console.log("setCookie: " + name + " => " + value + ": " + d);

	int_set_cookie(name, value, d, path, domain, secure);

}

function int_set_cookie(name, value, expires, path, domain, secure) {
	document.cookie= name + "=" + escape(value) +
		((expires) ? "; expires=" + expires.toGMTString() : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}

/*function del_cookie(name, path, domain) {
	if (getCookie(name)) {
		document.cookie = name + "=" +
		((path) ? ";path=" + path : "") +
		((domain) ? ";domain=" + domain : "" ) +
		";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	}
}*/

function get_cookie(name) {

	const dc = document.cookie;
	const prefix = name + "=";
	let begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else {
		begin += 2;
	}
	let end = document.cookie.indexOf(";", begin);
	if (end == -1) {
		end = dc.length;
	}
	return unescape(dc.substring(begin + prefix.length, end));
}

/* exported truncate_string */
function truncate_string(s, length) {
	if (!length) length = 30;
	let tmp = s.substring(0, length);
	if (s.length > length) tmp += "&hellip;";
	return tmp;
}

/* exported truncate_middle */
function truncate_middle(s, maxlen, suffix = "&hellip;") {
    if (s.length <= maxlen) return s;

    const sepLen = suffix.length,
        charsToShow = maxlen - sepLen,
        frontChars = Math.ceil(charsToShow/2),
        backChars = Math.floor(charsToShow/2);

    return s.substr(0, frontChars) + suffix + s.substr(s.length - backChars);
}

function rewrite_emoticon(str, index, title) {

	const cached = model.emoticons_cache()[str];

	if (cached)
		return cached;

	const emot = model.emoticons_map()[str];

	if (emot) {

		//var title = emot[4] ? emot[4] : str;

		if (!title) title = str;

		let res = $(document.createElement('img'))
			.attr('alt', title)
			.attr('title', title)
			.attr('src', 'emoticons/' + emot.file)
			.attr('tmid', index + ":" + str)
			.attr('orig-height', emot.height)
			.attr("loading", "lazy")
			.attr('class', 'img-emoticon')
			.css('height', emot.height + "px");

		// onload function
		if (emot.onload)
			res.attr('onload', emot.onload);

		// fixed width container span
		if (emot.box_width && emot.box_width > 0) {

			res = $(document.createElement('span'))
				.css('text-align', 'center')
				.css('display', 'inline-block')
				.css('width', emot.box_width + 'px')
				.append(res);
		}

		res = $("<span>").append(res).html();

		if (title == str) model.emoticons_cache()[str] = res;

		return res;
	}

	return str;
}

/* global ALLOW_EMOTICONS */
/* exported rewrite_emoticons */
function rewrite_emoticons(str, kind) {
	if (!str) return "";
	if (!ALLOW_EMOTICONS) return str;

	let tmp = str;

	const hooks = PluginHost.get(PluginHost.HOOK_REWRITE_EMOTICONS);
	for (let i = 0; i < hooks.length; i++) {
		tmp = hooks[i](tmp, kind);
	}

	let index = 0;

	tmp = tmp.safeReplace(/:([^ :]+):/, function(match) {
		return rewrite_emoticon(match[0], ++index);
	});

	return tmp;
}

/* exported rewrite_generic */
function rewrite_generic(s, allow_embed) {

	const hooks = PluginHost.get(PluginHost.HOOK_REWRITE_GENERIC);
	for (let i = 0; i < hooks.length; i++) {
		s = hooks[i](s, allow_embed);
	}

	return s;
}

/* exported rewrite_urls */
function rewrite_urls(s, allow_embed, connection_id, channel) {
	try {
		if (s.indexOf("://") == -1) return s;

		const lines = s.split("\n");

		for (let i = 0; i < lines.length; i++)
			lines[i] = rewrite_urls_real(lines[i], allow_embed, connection_id, channel);

		return lines.join("\n");
	} catch (e) {
		console.warn(e);
	}

	return s;
}

function rewrite_urls_real(s, allow_embed, connection_id, channel) {
	try {

		const hooks = PluginHost.get(PluginHost.HOOK_REWRITE_URLS);
		for (let i = 0; i < hooks.length; i++) {
			s = hooks[i](s, allow_embed);
		}

		if (s) {

			const tokens = s.split(/\s+/);
			let escaped_str = "";

			const img_proxy = get_cookie("ttirc_imgproxy") == "true";
			//const force_embed = localStorage["ttirc_force_embed"] == "true";

			$.each(tokens, function(i, u) {
				const m = u.match(/(([a-z]+):\/\/[(,_)'\-a-zA-Zа-яА-Я0-9@:\!%_\+\*.~#?&\/=\-\[\]]+)/i);

				if (m) {

					let url = m[1];
					let is_img = is_image(u);
					const is_vid = is_video(u);
					const yt_vid = is_youtube(u);

					if (img_proxy) {
						if (is_img) {
							url = "backend.php?op=imgproxy&url=" + encodeURIComponent(url);
						} else if (is_vid) {
							url = "backend.php?op=vidproxy&url=" + encodeURIComponent(url);
						}
					} else if (is_media) {
						url = "backend.php?op=mediaredirect&url=" + encodeURIComponent(url);
					}

					const is_visited = sessionStorage["ttirc.visited." + m[1].hashCode()] != undefined;

					if (is_img) {
						const hooks = PluginHost.get(PluginHost.HOOK_CHECK_IMAGE_EMBED);

						for (let i = 0; i < hooks.length; i++) {
							is_img = hooks[i](m[1], s);

							if (!is_img) break;
						}
					}

					let res;

					if (allow_embed && (is_img || is_vid || yt_vid)) {
						res = $("<span>");
						let real_src;

						if (m[1].indexOf("data:") == 0)
							real_src = m[1];
						else
							real_src = "backend.php?op=imgproxy&resize="+IMAGE_THUMB+"&url=" +
								param_escape(yt_vid ? "https://img.youtube.com/vi/" + yt_vid + "/hqdefault.jpg" :  m[1]);

						if (yt_vid) real_src += "&stamp=1";

						const img = $("<img>")
									.addClass('img-embed img-thumbnail img-responsive')
									.attr("referrerpolicy", "no-referrer")
									.css("image-rendering", "auto")
									.attr('title', m[1])
									.attr('src', real_src);

						const cachekey = connection_id + ":" + channel + ":" + real_src.hashCode();

						if (image_embed_cache[cachekey] == null) {

							img.attr('onload', 'img_check_size(this)')
								.attr('onerror', 'img_load_failed(this)')
								.css("display", "none");

							res.append($("<span>").html(m[1]));
							res.append($("<img>")
								.addClass('img-spinner')
								.attr("src", "images/bars.svg?1"));

						} else {

							//if (yt_vid) console.log(real_src, cachekey, image_embed_cache[cachekey]);

							let wh = $.extend({}, image_embed_cache[cachekey]);

							if (wh.w <= EMBED_MAX && wh.h <= EMBED_MAX) {
								wh = wh_scale(wh, EMBED_SCALED_MAX);

								img.css("max-width", wh.w + "px")
									.css("max-height", wh.h + "px");
							} else {
								img.attr('src', null)
									.css("display", "none");

								res.append($("<span>").html(m[1]));
							}

						}

						res.append(img);
					} else {
						res = m[1];
					}

					escaped_str += u.replace(m[1],
						$("<a onclick=\"return url_clicked(this, event)\">")
							.attr('target', '_blank')
							.attr('data-url-override', url)
							.attr('rel', 'noopener noreferrer')
							.attr('href', m[1])
							.attr('data-visited', is_visited)
							.html(res)
							.prop('outerHTML'));

				} else {
					escaped_str += u;
				}

				escaped_str += " ";
			});

			return escaped_str;
		}

	} catch (e) {
		console.warn("rewrite_urls failed for: " + s, e);
	}

	return s;
}

/* exported notifications_close */
function notifications_close() {
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.ready.then(function(registration) {
			registration.getNotifications().then(function(notifications) {
				let notification;
				/* eslint-disable no-cond-assign */
				while (notification = notifications.shift()) {
					notification.close();
				}
				/* eslint-enable no-cond-assign */
			});
		});
	}
}

/* global model */
/* exported notify */
function notify(msg) {
	if (model.alerts_enabled() && typeof Notification != "undefined" &&
			Notification.permission == "granted") {

		if ('serviceWorker' in navigator) {

			navigator.serviceWorker.ready.then(function(registration) {
				registration.showNotification(__('Tiny Tiny IRC'), {
					body: strip_tags(msg),
					data: window.location.href,
					icon: 'images/icon-hires.png' });

				registration.getNotifications().then(function(notifications) {
					while (notifications.length > 1) {
						notifications.shift().close();
					}
				});
			});
		}
	}
}

function strip_tags(str) {	// Strip HTML and PHP tags from a string
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	return str.replace(/<\/?[^>]+>/gi, '');
}

function is_image(src) {
	if (src) {

		if (src.toLowerCase().match("\.(jpe?g|gif|png|bmp|webp)"))
			return true;

		const udc = url_data_cache[src];

		if (udc && udc['content-type'] && udc['content-type'].indexOf('image/') != -1)
			return true;
	}

	return PluginHost.run(PluginHost.HOOK_IS_IMAGE_URL, src);
}

function is_video(src) {
	if (src) {

		if (src.toLowerCase().match("\.(gifv|mp4|webm)"))
			return true;

		const udc = url_data_cache[src];

		if (udc && udc['content-type'] && udc['content-type'].indexOf('video/') != -1)
			return true;
	}

	return false;
}

function is_youtube(src) {
	if (src) {
		const m = src.match(/youtube.com.*?v=([^&]+)|youtu.be\/([^&]+)/);

		if (m) return m[1] || m[2];
	}

	return false;
}

function is_media(src) {
	return is_image(src) || is_video(src) || is_youtube(src);
}

/*
function vid_ended(vid) {
	try {
		const repeats = $(vid).attr('data-repeats');

		if (repeats > 0) {
			$(vid).attr('data-repeats', repeats - 1);
			vid.play();
		}
	} catch (e) {
		console.warn(e);
	}
} */

/* exported img_load_failed */
function img_load_failed(img) {
	try {
		const ctx = lookup_ko_data(img);

		if (!ctx)
			return;

		const cache_key = ctx.connection_id() + ":" + ctx.channel() + ":" + $(img).attr('src').hashCode();

		if (image_embed_cache[cache_key] != null)
			return;

		image_embed_cache[cache_key] = [];

		if (ctx.message) {
			ctx.message_formatted('');
			ctx.message.notifySubscribers();
		}

	} catch (e) {
		console.warn(e);
	}
}

// https://github.com/knockout/knockout/issues/2477

/* exported lookup_ko_data */
function lookup_ko_data(elem) {
	let ctx = ko.dataFor(elem);

	if (!ctx) {
		while (elem = elem.parentNode) {
			ctx = ko.dataFor(elem);

			if (ctx) break;
		}
	}

	return ctx;
}

/* exported img_check_size */
function img_check_size(img) {

	try {
		const ctx = lookup_ko_data(img);

		if (!ctx)
			return;

		const cache_key = ctx.connection_id() + ":" + ctx.channel() + ":" + $(img).attr('src').hashCode();

		// we need to allow multiple updates because on initial update one image url may refer to different
		// Message objects (i.e. posted twice):

		/* if (image_embed_cache[cache_key] != null) - DISABLED
			return; */

//		console.log(cache_key, $(img).attr('src'), img.width, img.height);

		image_embed_cache[cache_key] = {w: img.width, h: img.height};

		if (ctx.message) {
			ctx.message_formatted('');
			ctx.message.notifySubscribers();
		}

		if (!image_embed_cache[cache_key].upd_done) {
			window.setTimeout(function() {
				image_embed_cache[cache_key].upd_done = 1;

				/* global update_buffer, log_bottom_offset */
				update_buffer(log_bottom_offset());
			}, 100);
		}

	} catch (e) {
		console.warn(e);
	}
}

function wh_scale(wh, limit) {
	if (wh.w > limit || wh.h > limit) {
		if (wh.w >= wh.h) {
			wh.h = limit / wh.w * wh.h;
			wh.w = limit;
		} else {
			wh.w = limit / wh.h * wh.w;
			wh.h = limit;
		}
	}

	return wh;
}
