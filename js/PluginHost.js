// based on http://www.velvetcache.org/2010/08/19/a-simple-javascript-hooks-system

/* exported PluginHost */
const PluginHost = {
	HOOK_REWRITE_SEND: 1,
	HOOK_REWRITE_EMOTICONS: 2,
	HOOK_REWRITE_EMOTICON: 3,
	HOOK_IS_IMAGE_URL: 4,
	HOOK_REWRITE_URLS: 5,
	HOOK_TAB_SWITCHED: 6,
	HOOK_HANDLE_EVENT: 7,
	HOOK_GLOBAL_HOTKEY: 8,
	HOOK_IS_HIGHLIGHT: 9,
	HOOK_INIT_COMPLETE: 10,
	HOOK_PRE_FORMAT_LINE: 11,
	HOOK_CHECK_IMAGE_EMBED: 12,
	HOOK_WINDOW_ACTIVE: 13,
	HOOK_REWRITE_GENERIC: 14,
	hooks: [],
	register: function (name, callback) {
		if (typeof(this.hooks[name]) == 'undefined')
			this.hooks[name] = [];

		this.hooks[name].push(callback);
	},
	get: function (name) {
		//console.warn('PluginHost::get ' + name);

		if (typeof(this.hooks[name]) != 'undefined')
			return this.hooks[name];
		else
			return [];
	},
	run: function (name, args) {
		//console.warn('PluginHost::run ' + name);

		if (typeof(this.hooks[name]) != 'undefined')
			for (let i = 0; i < this.hooks[name].length; i++)
				if (!this.hooks[name][i](args)) break;
	}
};

/* PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED,
		function (args) { console.log('ARTICLE_RENDERED: ' + args); return true; });

PluginHost.register(PluginHost.HOOK_ARTICLE_RENDERED_CDM,
		function (args) { console.log('ARTICLE_RENDERED_CDM: ' + args); return true; }); */

