	'use strict';

/* global __, show_spinner, infobox_callback2, hide_spinner */

const Prefs = {
	// TODO: unfuck this
	get_selected_rows: function(elem) {
		const rv = [];

		if (elem) {
			const boxes = $(elem).find("input");

			for (let i = 0; i < boxes.length; i++) {
				if (boxes[i].type == "checkbox" && boxes[i].checked) {
					const row_id = boxes[i].getAttribute("row_id");
					rv.push($('#' + row_id)[0]);
				}
			}
		}
		return rv;
	},
	show: function() {
		show_spinner();

		$.post("backend.php", {op: 'prefs'}, function(resp) {
			infobox_callback2(resp);
			hide_spinner();
		}, 'html');
	},
	miniError: function(msg) {
		const elem = $("#mini-notice");

		if (msg) {
			elem.html(msg).show();
		} else {
			elem.hide();
		}
	},
	save: function(callback) {
		const query = $("#prefs_form").serialize();

		console.log(query);

		$.post("backend.php", query, function (data) {

			let obj;
			try {
				obj = JSON.parse(data);
			} catch (e) { }

			if (obj) {
				if (obj.error) {
					alert(obj.error);
				} else if (obj.message == "THEME_CHANGED") {
					window.location.reload();
				} else if (callback) {
					callback(obj);
				}
			} else if (callback) {
				callback();
			} else {
				$("#infoBox").modal('hide');
			}

			hide_spinner();

		}, 'text');

		return false;
	},
	Connections: {
		Servers: {
			delete: function() {
				const rows = Prefs.get_selected_rows("#servers-list");

				if (rows.length > 0) {
					if (confirm(__("Delete selected servers?"))) {

						const connection_id = document.forms['prefs_conn_form'].connection_id.value;

						const ids = [];

						for (let i = 0; i < rows.length; i++) {
							ids.push(rows[i].getAttribute("server_id"));
						}

						const query = {op: 'delete-server', ids: ids.toString(),
							connection_id: connection_id};

						console.log(query);

						show_spinner();

						$.post("backend.php", query, function (resp) {
							$("#servers-list").html(resp);
							hide_spinner();
						}, 'html');

					}
				} else {
					alert(__("Please select some servers to delete."));
				}
			},
			create: function() {
				const data = prompt(__("Server:Port (e.g. irc.example.org:6667):"));

				if (data) {

					const connection_id = document.forms['prefs_conn_form'].connection_id.value;
					const query = {op: 'create-server', data: data, connection_id: connection_id};

					console.log(query);

					show_spinner();

					$.post("backend.php", query, function (data) {

						let obj;
						try {
							obj = JSON.parse(data);
						} catch (e) { }

						if (obj && obj.error) {
							alert(obj.error);
						} else {
							$("#servers-list").html(data);
						}
						hide_spinner();
					}, 'text');
				}
			}
		},
		create: function() {
			const title = prompt(__("Title for a new connection:"));

			if (title) {

				show_spinner();

				$.post("backend.php", {op: 'create-connection', title: title}, function (resp) {
					$("#connections-list").html(resp);
					hide_spinner();
				}, 'html');

			}
		},
		edit: function(id) {
			Prefs.save(function (/*obj*/) {
				$.post("backend.php", {op: 'prefs-edit-con', id: id}, function (resp) {
					infobox_callback2(resp);
				}, 'html');
			});
		},
		save: function(callback) {
			const query = $("#prefs_conn_form").serialize();

			$.post("backend.php", query, function (obj) {

				if (obj && obj.error) {
					Prefs.miniError(obj.error);
				} else if (callback) {
					callback(obj);
				} else {
					Prefs.show();
				}

				hide_spinner();
			});
		},
		delete: function() {
			const rows = Prefs.get_selected_rows("#connections-list");

			if (rows.length > 0) {
				if (confirm(__("Delete selected connections? Active connections will not be deleted."))) {

					const ids = [];

					for (let i = 0; i < rows.length; i++) {
						ids.push(rows[i].getAttribute("connection_id"));
					}

					const query = {op: 'delete-connection', ids: ids.toString()};

					console.log(query);

					show_spinner();

					$.post("backend.php", query, function(resp) {
						$("#connections-list").html(resp);
						hide_spinner();
					}, 'html');

				}
			} else {
				alert(__("Please select some connections to delete."));
			}
		},
	},
	CustomizeCSS: {
		show: function() {
			Prefs.save(function (/*obj*/) {
				$.post("backend.php", {op: 'prefs-customize-css'}, function (resp) {
					infobox_callback2(resp);
				}, 'html');
			});
		},
		save: function(callback) {
			const query = $("#prefs_css_form").serialize();

			$.post("backend.php", query, function (obj) {

				if (obj && obj.error) {
					Prefs.miniError(obj.error);
				} else if (callback) {
					callback(obj);
				} else {
					window.location.reload();
				}

				hide_spinner();
			});
		}
	},
	Notifications: {
		enable: function() {
			if (typeof Notification != "undefined") {
				if (Notification.permission != "granted") {
					Notification.requestPermission();
				} else {
					Prefs.miniError("Desktop notifications are already enabled.");
				}
			}
		},
		configure: function() {
			if (typeof Notification != "undefined") {
				Prefs.save(function (/*obj*/) {
					$.post("backend.php", {op: 'prefs-edit-notify'}, function (resp) {
						infobox_callback2(resp);
					}, 'html');
				});
			} else {
				alert(__("Your browser doesn't seem to support desktop notifications."));
			}
		},
		save: function(callback) {
			const query = $("#prefs_notify_form").serialize();

			$.post("backend.php", query, function (obj) {

				if (obj && obj.error) {
					Prefs.miniError(obj.error);
				} else if (callback) {
					callback(obj);
				} else {
					Prefs.show();
				}

				hide_spinner();
			});
		},
	},
};
