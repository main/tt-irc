<?php
	function css_editor() {
		$user_css = get_pref("USER_STYLESHEET");
	?>
	<div class="modal-header">
		<button type="button" data-dismiss="modal" class="close">&times;</button>
		<h4 class="modal-title"><?php echo __("Customize Theme") ?></h4>
	</div>
	<div class="modal-body">
		<div class="alert alert-info" id="mini-notice" style='display : none'>&nbsp;</div>

		<form class="form" id="prefs_css_form" onsubmit="return false;">

		<input type="hidden" name="op" value="prefs-save-css"/>

		<div class="alert alert-info"><?php echo T_sprintf("You can override colors, fonts and layout of your currently selected theme with custom CSS declarations here.") ?></div>

		<textarea name="user_css" rows="10" class="form-control"><?php echo $user_css ?></textarea>

		</form>
	</div>

	<div class="modal-footer">
		<button class="btn btn-primary" type="submit" onclick="Prefs.CustomizeCSS.save()">
			<span class='glyphicon glyphicon-saved'> </span>
			<?php echo __('Save & reload') ?></button>
		<button class="btn btn-default" type="submit" onclick="Prefs.show()">
			<span class='glyphicon glyphicon-remove'> </span>
			<?php echo __('Go back') ?></button></div>
	</div>

	</div>
	<?php

	}

	function print_servers($id) {

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT ttirc_servers.*,
				status,active_server
			FROM ttirc_servers,ttirc_connections
			WHERE connection_id = ? AND
			connection_id = ttirc_connections.id AND
			owner_uid = ?");
		$sth->execute([$id, $_SESSION['uid']]);

		while ($line = $sth->fetch()) {

			$id = $line['id'];

			if ($line['status'] != CS_DISCONNECTED &&
					$line['server'] . ':' . $line['port'] == $line['active_server']) {
				$connected = __("(connected)");
			} else {
				$connected = '';
			}

			print "<li class='row' id='S-$id' server_id='$id'>";
			print "<div class='checkbox'><label>";
			print "<input type='checkbox'
				row_id='S-$id'>";
			print $line['server'] . ":" . $line['port'] . " $connected";
			print "</label></div>";
			print "</li>";
		}
	}

	function notification_editor() {

		$notify_on = json_decode(get_pref("NOTIFY_ON"));

		if (!is_array($notify_on)) $notify_on = array();

		$nev_checked = array();

		foreach ($notify_on as $no) {
			$nev_checked[$no] = "checked";
		}
	?>

	<div class="modal-header">
		<button type="button" data-dismiss="modal" class="close">&times;</button>
		<h4 class="modal-title"><?php echo __("Notifications") ?></h4>
	</div>

	<div class="modal-body">
		<div id="mini-notice" class="alert alert-info" style='display : none'>&nbsp;</div>

		<div class="alert alert-info"><?php echo T_sprintf("Desktop notifications are only shown for events happening in background channels or when your Tiny Tiny IRC window is unfocused.") ?></div>


		<form class="form" id="prefs_notify_form" onsubmit="return false;">

		<input type="hidden" name="op" value="prefs-save-notify"/>

		<h5><?php echo __('Notify events') ?></h5>

		<div class="form-group">
			<div class="col-sm-6">

				<div class="checkbox"><label for="n_highlight">
					<input name="notify_event[]" <?php echo $nev_checked[1] ?>
						id="n_highlight" type="checkbox" value="1">
					<?php echo __('Channel highlight') ?>
				</label></div>

				<div class="checkbox"><label for="n_privmsg">
					<input name="notify_event[]" <?php echo $nev_checked[2] ?>
						id="n_privmsg" type="checkbox" value="2">
					<?php echo __('Private message') ?>
					</label></div>

				<div class="checkbox"><label for="n_connstat">
					<input name="notify_event[]" <?php echo $nev_checked[3] ?>
						id="n_connstat" type="checkbox" value="3">
					<?php echo __('Connection status change') ?>
				</label></div>

				<div class="checkbox"><label for="n_chanmsg">
					<input name="notify_event[]" <?php echo $nev_checked[4] ?>
						id="n_chanmsg" type="checkbox" value="4">
					<?php echo __('Channel message') ?>
				</label></div>
			</div>
		</div>

		</form>
	</div>

	<div class="modal-footer">
		<div style='float : left'>
			<button class="btn btn-info" type="submit" onclick="Prefs.Notifications.enable()">
			<span class='glyphicon glyphicon-flash'> </span>
				<?php echo __('Enable') ?>
			</button></div>

			<button class="btn btn-primary" type="submit" onclick="Prefs.Notifications.save()">
				<span class='glyphicon glyphicon-saved'> </span>
				<?php echo __('Save') ?>
			</button>
			<button class="btn btn-default" type="submit" onclick="Prefs.show()">
				<span class='glyphicon glyphicon-remove'> </span>
				<?php echo __('Go back') ?>
			</button></div>
		</div>

	</div>
	<?php

	}

	function connection_editor($id) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT * FROM ttirc_connections
			WHERE id = ? AND owner_uid = ?");
		$sth->execute([$id, $_SESSION['uid']]);

		$line = $sth->fetch();

		$auto_connect_checked = $line['auto_connect'] ? 'checked' : '';
		$visible_checked = $line['visible'] ? 'checked' : '';
		$permanent_checked = $line['permanent'] ? 'checked' : '';
		$use_ssl_checked = $line['use_ssl'] ? 'checked' : '';

	?>
	<div class="modal-header">
		<button type="button" data-dismiss="modal" class="close">&times;</button>
		<h4 class="modal-title"><?php echo __("Edit Connection") ?></h4>
	</div>
	<div class="modal-body">
		<div class="alert alert-info" id="mini-notice" style='display : none'>&nbsp;</div>

		<form class="form form-horizontal" id="prefs_conn_form" onsubmit="return false;">

		<input type="hidden" name="connection_id" value="<?php echo $id ?>"/>
		<input type="hidden" name="op" value="prefs-conn-save"/>

 		<div class="form-group">
			<label class='col-sm-4 control-label'><?php echo __('Title:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="title" required="1" size="30" value="<?php echo $line['title'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class='col-sm-4 control-label'><?php echo __('Server password:') ?></label>
			<div class="col-sm-6">
				<input class="form-control" name="server_password" size="30" type="password"
					value="<?php echo $line['server_password'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Nickname:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nick" size="30" value="<?php echo $line['nick'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class='col-sm-4 control-label'><?php echo __('Favorite channels:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="autojoin" size="30" value="<?php echo $line['autojoin'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class='col-sm-4 control-label'><?php echo __('Connect command:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="connect_cmd" size="30" value="<?php echo $line['connect_cmd'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class='col-sm-4 control-label'><?php echo __('Character set:') ?></label>
			<div class="col-sm-6">
				<?php print_select('encoding', $line['encoding'], get_iconv_encodings()) ?>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4"></div>
			<div class="col-sm-6">

				<div class="checkbox"><label for="pr_visible">
					<input name="visible" <?php echo $visible_checked ?>
						id="pr_visible" type="checkbox" value="1">
					<?php echo __('Enable connection') ?>
				</label></div>

				<div class="checkbox"><label for="pr_auto_connect">
					<input name="auto_connect" <?php echo $auto_connect_checked ?>
						id="pr_auto_connect" type="checkbox" value="1">
					<?php echo __('Automatically connect') ?>
				</label></div>

				<div class="checkbox"><label for="pr_permanent">
					<input name="permanent" <?php echo $permanent_checked ?>
						id="pr_permanent" type="checkbox" value="1">
					<?php echo __('Stay connected permanently') ?>
				</label></div>

				<div class="checkbox"><label for="pr_use_ssl">
					<input name="use_ssl" <?php echo $use_ssl_checked ?>
						id="pr_use_ssl" type="checkbox" value="1">
					<?php echo __('Connect using SSL') ?>
				</label></div>
			</div>
		</div>

		<button type="submit" style="display : none" onclick="Prefs.Connections.save()"></button>

		<h5><?php echo __('Servers') ?></h5>

		<ul id="servers-list" class="list-unstyled scrollable panel panel-default">
			<?php print_servers($id); ?>
		</ul>

		</form>

	</div>

	<div class="modal-footer">
			<div style='float : left'>
			<button class="btn btn-default" onclick="Prefs.Connections.Servers.create()"><?php echo __('Add server') ?></button>
			<button class="btn btn-danger" onclick="Prefs.Connections.Servers.delete()"><?php echo __('Delete') ?></button>
			</div>
			<button class="btn btn-primary" type="submit" onclick="Prefs.Connections.save()"><?php echo __('Save') ?></button>
			<button class="btn btn-default" type="submit" onclick="Prefs.show()"><?php echo __('Go back') ?></button></div>
		</div>
	</div>

	<?php
	}

	function print_connections() {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT * FROM ttirc_connections
			WHERE title != '_ttirc_instance' AND owner_uid = ?");
		$sth->execute([$_SESSION['uid']]);

		while ($line = $sth->fetch()) {

			$id = $line['id'];

			if ($line["status"] != "0") {
				$connected = __("(active)");
			} else {
				$connected = "";
			}

			print "<li class='row' id='C-$id' connection_id='$id'>";
			print "<div class='checkbox'><label>";
			print "<input type='checkbox'
				row_id='C-$id'>";
			print "<a href=\"#\" title=\"".__('Click to edit connection')."\"
				onclick=\"Prefs.Connections.edit($id)\">".
				$line['title']." $connected</a>";
			print "</label></div>";
			print "</li>";
		}
	}

	function main_prefs() {

	$_SESSION["prefs_cache"] = false;

	$dbh = DB::get();

	$sth = $dbh->prepare("SELECT * FROM ttirc_users WHERE id = ?");
	$sth->execute([$_SESSION['uid']]);

	$row = $sth->fetch();

	$realname = $row["realname"];
	$nick = $row["nick"];
	$email = $row["email"];
	$quit_message = $row["quit_message"];

	$hide_join_part_checked = $row['hide_join_part'] ? 'checked' : '';

	$highlight_on = get_pref("HIGHLIGHT_ON");
	$disable_image_preview_checked = get_pref("DISABLE_IMAGE_PREVIEW") ? "checked" : "";
?>

	<div class="modal-header">
		<button type="button" data-dismiss="modal" class="close">&times;</button>
		<h4 class="modal-title"><?php echo __("Preferences") ?></h4 class="modal-title"></div>
	<div class="modal-body">

		<form class="form-horizontal" id="prefs_form" onsubmit="return false;">

		<div id="mini-notice" class="alert alert-info" style='display : none'>&nbsp;</div>

		<input type="hidden" name="op" value="prefs-save"/>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Real name:') ?></label>
			<div class="col-sm-6">
			<input class="form-control" type="text" name="realname" required="1" size="30" value="<?php echo $realname ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Nickname:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nick" placeholder="<?php echo $_SESSION['name'] ?>" required="1" size="30" value="<?php echo $nick ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('E-mail:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="email" placeholder="<?php echo $_SESSION['name'] ?>@localhost" required="1" size="30" value="<?php echo $email ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Quit message:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="quit_message" size="30" value="<?php echo $quit_message ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Highlight on:') ?></label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="highlight_on" size="30" value="<?php echo $highlight_on ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Theme:') ?></label>
			<div class="col-sm-6">
				<div class="input-group">
					<?php print_theme_select(); ?>
					<a class="input-group-addon btn btn-default" href="#" onclick="Prefs.CustomizeCSS.show()">
						<?php echo __("Customize") ?></a>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4"></div>
			<div class="col-sm-6">
				<a class="btn btn-info" href="#" onclick="Prefs.Notifications.configure()">
					<span class='glyphicon glyphicon-flash'> </span>
					<?php echo __('Desktop notifications') ?></a>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4"></div>
			<div class="col-sm-6">
				<div class="checkbox">
					<label><input name="hide_join_part" <?php echo $hide_join_part_checked ?>
						id="pr_hide_join_part" type="checkbox" value="1">
					<?php echo __('Do not highlight tabs on auxiliary messages') ?></label></div>

				<div class="checkbox">
					<label><input name="disable_image_preview" <?php echo $disable_image_preview_checked ?>
						id="pr_disable_image_preview" type="checkbox" value="1">
					<?php echo __('Disable popup image viewer') ?></label></div>

			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label"><?php echo __('Change password:') ?></label>

			<div class="col-sm-6">
				<input placeholder="<?php echo __("New password") ?>" autocomplete="off" type="password" class="form-control" name="new_password" type="password" size="30" value="">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4"> </label>
			<div class="col-sm-6">
				<input placeholder="<?php echo __("Confirm password") ?>" autocomplete="off" type="password" class="form-control" name="confirm_password" type="password" size="30" value="">
			</div>
		</div>

		<h5><?php echo __('Connections') ?></h5>

		<ul class="scrollable panel panel-default list-unstyled" id="connections-list"><?php print_connections() ?></ul>

	</form>

	</div>

	<div class="modal-footer">
		<div style='float : left'>
			<button class="btn btn-default" onclick="Prefs.Connections.create()">
				<span class='glyphicon glyphicon-plus-sign'> </span>
				<?php echo __('Create connection') ?></button class="btn">
			<button class="btn btn-danger" onclick="Prefs.Connections.delete()">
			<span class='glyphicon glyphicon-remove-sign'> </span>
				<?php echo __('Delete') ?></button class="btn">
		</div>
		<button class="btn btn-primary" type="submit" onclick="Prefs.save()">
			<span class='glyphicon glyphicon-ok'> </span>
			<?php echo __('Save') ?></button class="btn">
		<button class="btn btn-default" data-dismiss="modal">
			<span class='glyphicon glyphicon-remove'> </span>
			<?php echo __('Close') ?></button>
	</div>

<?php } ?>
