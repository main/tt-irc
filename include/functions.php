<?php
	if (defined('E_DEPRECATED')) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
	} else {
		error_reporting(E_ALL & ~E_NOTICE);
	}

	mb_internal_encoding("UTF-8");

	require_once "config.php";
	require_once "version.php";
	require_once "message_types.php";
	require_once "classes/db.php";
	require_once "db-prefs.php";

	require_once "lib/credis/Client.php";

	$redis = new Credis_Client(REDIS_SERVER);

	$ERROR_MESSAGES = [
		0 => "Unknown error",
		5 => "Incorrect database schema version.",
		6 => "Request not authorized.",
		15 => "Unable to connect to Redis server.",
		16 => "Unable to connect to database server.",
	];

	define('SINGLE_USER_MODE', false);
	define('EMOTICONS_MAP', 'emoticons/emoticons.json');
	define('DEFAULT_BUFFER_SIZE', 128);
	define('KEEP_MESSAGES_MAX', 1024);

	if (DB_TYPE == 'mysql') {
		define('DB_KEY_FIELD', 'param');
	} else {
		define('DB_KEY_FIELD', 'key');
	}

	if (DB_TYPE == "pgsql") {
		define('SUBSTRING_FOR_DATE', 'SUBSTRING_FOR_DATE');
	} else {
		define('SUBSTRING_FOR_DATE', 'SUBSTRING');
	}

	define('URL_REGEXP', "/(([a-z]+):\/\/[(,_)'\-a-zA-Z0-9@:\!%_\+.~#?&\/=]+)/i");

	function message_id($redis) {
		return $redis->incr("ttirc_last_message_id");
	}

	function get_translations() {
		$tr = array(
					"auto"  => "Detect automatically",
					"en_US" => "English",
					"es_MX" => "Spanish (Mexican)",
					"fr_FR" => "Français",
					"xx_XX" => "Bork bork bork!");

		return $tr;
	}

	if (ENABLE_TRANSLATIONS == true) { // If translations are enabled.
		require_once "lib/accept-to-gettext.php";
		require_once "lib/gettext/gettext.inc.php";

		function startup_gettext() {

			# Get locale from Accept-Language header
			$lang = al2gt(array_keys(get_translations()), "text/html");

			if (defined('_TRANSLATION_OVERRIDE_DEFAULT')) {
				$lang = _TRANSLATION_OVERRIDE_DEFAULT;
			}

			if ($_COOKIE["ttirc_lang"] && $_COOKIE["ttirc_lang"] != "auto") {
				$lang = $_COOKIE["ttirc_lang"];
			}

			/* In login action of mobile version */
			if ($_POST["language"] && defined('MOBILE_VERSION')) {
				$lang = $_POST["language"];
				$_COOKIE["ttirc_lang"] = $lang;
			}

			if ($lang) {
				if (defined('LC_MESSAGES')) {
					_setlocale(LC_MESSAGES, $lang);
				} else if (defined('LC_ALL')) {
					_setlocale(LC_ALL, $lang);
				} else {
					die("can't setlocale(): please set ENABLE_TRANSLATIONS to false in config.php");
				}

				if (defined('MOBILE_VERSION')) {
					_bindtextdomain("messages", "../locale");
				} else {
					_bindtextdomain("messages", "locale");
				}

				_textdomain("messages");
				_bind_textdomain_codeset("messages", "UTF-8");
			}
		}

		startup_gettext();

	} else { // If translations are enabled.
		function __($msg) {
			return $msg;
		}
		function startup_gettext() {
			// no-op
			return true;
		}
	} // If translations are enabled.

	function validate_nick($nick) {
		$tmp = strip_prefix(trim(mb_substr(strip_tags($nick), 0, 119)));
		$tmp = preg_replace("/([#:\n]|---)/", "", $tmp);

		return $tmp;
	}

	function update_last_chanmod($owner_uid) {
		global $redis;

		$redis->incr("ttirc_last_chanmod,$owner_uid");
	}

	function is_instance($connection_id) {
		$dbh = DB::get();

		if (isset($_SESSION['is_instance_cache']) && isset($_SESSION['is_instance_cache'][$connection_id]))
			return $_SESSION['is_instance_cache'][$connection_id];

		$sth = $dbh->prepare("SELECT id FROM ttirc_connections WHERE
			id = ? AND title = '_ttirc_instance'");
		$sth->execute([$connection_id]);

		if (!isset($_SESSION['is_instance_cache']))
			$_SESSION['is_instance_cache'] = [];

		if ($sth->fetch()) {
			$_SESSION['is_instance_cache'][$connection_id] = true;
			return true;
		} else {
			$_SESSION['is_instance_cache'][$connection_id] = false;
			return false;
		}
	}

	function login_sequence($mobile = false) {

		$_SESSION["prefs_cache"] = array();
		$dbh = DB::get();

		if (!SINGLE_USER_MODE) {

			$login_action = $_POST["login_action"];

			# try to authenticate user if called from login form
			if ($login_action == "do_login") {
				$login = $_POST["login"];
				$password = $_POST["password"];
				$remember_me = $_POST["remember_me"];

				if (authenticate_user($login, $password)) {
					$_POST["password"] = "";

					$_SESSION["language"] = $_POST["language"];
					$_SESSION["ref_schema_version"] = get_schema_version(true);
					$_SESSION["bw_limit"] = !!$_POST["bw_limit"];
					$_SESSION["csrf_token"] = bin2hex(get_random_bytes(16));

					header("Location: " . $_SERVER["REQUEST_URI"]);
					exit;

					return;
				} else {
					// start an empty session to deliver login error message
					@session_start();

					if (!isset($_SESSION["login_error_msg"])) $_SESSION["login_error_msg"] = __("Incorrect username or password");
				}
			}

			if (!$_SESSION["uid"] || !validate_session()) {
				header('Cache-Control: public');
				render_login_form();
			} else {
				/* bump login timestamp */

				if (get_schema_version() >= 3) {
					$sth = $dbh->prepare("UPDATE ttirc_users SET last_login = NOW() WHERE id = ?");
					$sth->execute([$_SESSION['uid']]);
				}

				$_SESSION["last_login_update"] = time();

				if ($_SESSION["language"] && SESSION_COOKIE_LIFETIME > 0) {
					setcookie("ttirc_lang", $_SESSION["language"],
						time() + SESSION_COOKIE_LIFETIME);
				}

				/* Enable automatic connections */

				$sth = $dbh->prepare("UPDATE ttirc_connections SET enabled = true
					WHERE auto_connect = true AND owner_uid = ?");
				$sth->execute([$_SESSION['uid']]);

				initialize_user_prefs($_SESSION["uid"]);
			}

		} else {
			return authenticate_user("admin", null);
		}
	}

	function render_login_form() {
		logout_user();
		require_once "login_form.php";
		exit;
	}

	function print_select($id, $default, $values, $attributes = "") {
		print "<select class=\"form-control\" name=\"$id\" id=\"$id\" $attributes>";
		foreach ($values as $v) {
			if ($v == $default)
				$sel = " selected";
			 else
			 	$sel = "";

			print "<option$sel>$v</option>";
		}
		print "</select>";
	}

	function print_select_hash($id, $default, $values, $attributes = "") {
		print "<select name=\"$id\" id='$id' $attributes>";
		foreach (array_keys($values) as $v) {
			if ($v == $default)
				$sel = 'selected="selected"';
			 else
			 	$sel = "";

			print "<option $sel value=\"$v\">".$values[$v]."</option>";
		}

		print "</select>";
	}

	function encrypt_password($pass, $salt = '', $mode2 = false) {
		if ($salt && $mode2) {
			return "MODE2:" . hash('sha256', $salt . $pass);
		} else if ($salt) {
			return "SHA1X:" . sha1("$salt:$pass");
		} else {
			return "SHA1:" . sha1($pass);
		}
	} // function encrypt_password

	function authenticate_user($login, $password, $force_auth = false) {

		$dbh = DB::get();

		if (!SINGLE_USER_MODE) {

			$pwd_hash1 = encrypt_password($password);
			$pwd_hash2 = encrypt_password($password, $login);

			$lsth = false;

			if (get_schema_version() > 6) {
				$sth = $dbh->prepare("SELECT salt FROM ttirc_users WHERE
					login = ?");
				$sth->execute([$login]);
				$row = $sth->fetch();

				$salt = $row['salt'];

				if ($salt == "") {

					// verify and upgrade password to new salt base
					$sth = $dbh->prepare("SELECT id,login,access_level,pwd_hash
		            FROM ttirc_users WHERE
						login = ? AND (pwd_hash = ? OR pwd_hash = ?)");
					$sth->execute([$login, $pwd_hash1, $pwd_hash2]);

					if ($row = $sth->fetch()) {
						// upgrade password to MODE2

						$salt = substr(bin2hex(get_random_bytes(125)), 0, 250);
						$pwd_hash = encrypt_password($password, $salt, true);

						$sth = $dbh->prepare("UPDATE ttirc_users SET
							pwd_hash = ?, salt = ? WHERE login = ?");
						$sth->execute([$pwd_hash, $salt, $login]);

						$lsth = $dbh->prepare("SELECT id,login,access_level,pwd_hash
			            FROM ttirc_users WHERE
							login = ? AND pwd_hash = ?");
						$lsth->execute([$login, $pwd_hash]);

					} else {
						return false;
					}

				} else {

					$pwd_hash = encrypt_password($password, $salt, true);

					$lsth = $dbh->prepare("SELECT id,login,access_level,pwd_hash
			         FROM ttirc_users WHERE
						login = ? AND pwd_hash = ?");
					$lsth->execute([$login, $pwd_hash]);
				}

			} else {

				$lsth = $dbh->prepare("SELECT id,login,access_level,pwd_hash
	            FROM ttirc_users WHERE
					login = ? AND (pwd_hash = ? OR pwd_hash = ?)");

				$lsth->execute([$login, $pwd_hash1, $pwd_hash2]);
			}

			if ($lsth && $row = $lsth->fetch()) {

				session_start();
				session_regenerate_id(true);

				$_SESSION["uid"] = $row["id"];
				$_SESSION["name"] = $row["login"];
				$_SESSION["access_level"] = $row["access_level"];
				$_SESSION['http_ua_hash'] = sha1($_SERVER['HTTP_USER_AGENT']);
				$_SESSION['remote_addr_hash'] = sha1($_SERVER['REMOTE_ADDR']);

				$sth = $dbh->prepare("UPDATE ttirc_users SET last_login = NOW() WHERE id = ?");
				$sth->execute([$_SESSION['uid']]);

				$_SESSION["pwd_hash"] = $row["pwd_hash"];

				initialize_user_prefs($_SESSION["uid"]);

				return true;
			}

			return false;

		} else {

			@session_start();

			$_SESSION["uid"] = 1;
			$_SESSION["name"] = "admin";

			initialize_user_prefs($_SESSION["uid"]);

			return true;
		}
	}

	function get_schema_version($nocache = false) {
		if (!$_SESSION["schema_version"] || $nocache) {

			$dbh = DB::get();
			$row = $dbh->query("SELECT schema_version FROM ttirc_version")->fetch();
			$version = $row['schema_version'];

			$_SESSION["schema_version"] = $version;
			return $version;
		} else {
			return $_SESSION["schema_version"];
		}
	}

	function get_script_dt_add() {
		return time();
	}

	function theme_image($filename) {
		$theme_path = get_user_theme_path();

		if ($theme_path && is_file($theme_path.$filename)) {
			return $theme_path."/".$filename;
		} else {
			return $filename;
		}
	}

	function get_user_theme_params() {
		$theme_name = get_user_theme();

		if ($theme_name) {
			if (is_dir("themes.local/$theme_name")) {
				return parse_ini_file("themes.local/$theme_name/theme.ini");
			} else {
				return parse_ini_file("themes/$theme_name/theme.ini");
			}
		} else {
			return false;
		}
	}

	function get_user_theme() {
		$theme_name = get_pref("USER_THEME");
		if (is_dir("themes.local/$theme_name")) {
			return $theme_name;
		} else if (is_dir("themes/$theme_name")) {
			return $theme_name;
		} else {
			return '';
		}
	}

	function get_user_theme_path() {
		$theme_name = get_pref("USER_THEME");

		if ($theme_name && is_dir("themes.local/$theme_name")) {
			$theme_path = "themes.local/$theme_name";
		} else if ($theme_name && is_dir("themes/$theme_name")) {
			$theme_path = "themes/$theme_name";
		} else {
			$theme_name = '';
		}

		return $theme_path;
	}

	function get_all_themes() {
		$themes = array_filter(array_merge(glob("themes/*"), glob("themes.local/*")), "is_dir");

		asort($themes);

		$rv = array();

		foreach ($themes as $t) {
			if (is_file("$t/theme.ini")) {
				$ini = parse_ini_file("$t/theme.ini", true);
				if ($ini['theme']['version'] && !$ini['theme']['disabled']) {
					$entry = array();
					$entry["path"] = $t;
					$entry["base"] = basename($t);
					$entry["name"] = $ini['theme']['name'];
					$entry["version"] = $ini['theme']['version'];
					$entry["author"] = $ini['theme']['author'];
					$entry["options"] = $ini['theme']['options'];
					array_push($rv, $entry);
				}
			}
		}

		return $rv;
	}

	function logout_user() {

		session_destroy();

		if (isset($_COOKIE[session_name()])) {
		   setcookie(session_name(), '', time()-42000, '/');
		}

		session_commit();
	}

	function format_warning($msg, $id = "") {
		return "<div class=\"alert alert-warning\" id=\"$id\">$msg</div>";
	}

	function format_notice($msg, $id = "") {
		return "<div class=\"alert alert-info\" id=\"$id\">$msg</div>";
	}

	function format_error($msg, $id = "") {
		return "<div class=\"alert alert-danger\" id=\"$id\">$msg</div>";
	}

	function print_notice($msg) {
		print format_notice($msg);
	}

	function print_warning($msg) {
		print format_warning($msg);
	}

	function print_error($msg) {
		print format_error($msg);
	}

	function T_sprintf() {
		$args = func_get_args();
		return vsprintf(__(array_shift($args)), $args);
	}

	function _debug($msg) {
		$ts = strftime("%H:%M:%S", time());
		if (function_exists('posix_getpid')) {
			$ts = "$ts/" . posix_getpid();
		}
		print "[$ts] $msg\n";
	} // function _debug

	function get_nick($connection_id) {
		global $connection_nick_cache;

		$dbh = DB::get();

		if (isset($connection_nick_cache[$connection_id])) {
			return $connection_nick_cache[$connection_id];
		}

		$sth = $dbh->prepare("SELECT active_nick FROM ttirc_connections
			WHERE id = ?");

		$sth->execute([$connection_id]);

		if ($row = $sth->fetch()) {
			$connection_nick_cache[$connection_id] = $row['active_nick'];

			return $connection_nick_cache[$connection_id];
		} else {
			return '?UNKNOWN?';
		}
	}

	function instance_userhosts($connection_id) {
		$dbh = DB::get();

		$sth = $dbh->query("SELECT DISTINCT login, realname,
			active_nick,
			heartbeat < NOW() - interval '15 minute' AS is_away
			FROM ttirc_users u, ttirc_connections cs WHERE
				cs.owner_uid = u.id AND cs.title = '_ttirc_instance'");

		$rv = [];

		while ($line = $sth->fetch()) {
			$rv[$line['active_nick']] = [
				$line["login"],
				"local",
				"local",
				$line["realname"],
				$line['is_away'],
				$line['is_away'] ? __('Offline') : '',
			];
		}

		return $rv;
	}

	function instance_nicklist($channel) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT
			active_nick,
			access_level >= 5 AS is_op,
			(heartbeat < NOW() - interval '15 minutes') AS is_away,
			(last_message >= NOW() - interval '15 minutes') AS is_voiced
			FROM ttirc_connections cs, ttirc_users u
			WHERE title = '_ttirc_instance' AND
			u.id = cs.owner_uid AND
			(SELECT id FROM ttirc_channels WHERE LOWER(channel) = LOWER(?) AND connection_id = cs.id) > 0
			ORDER BY active_nick");
		$sth->execute([$channel]);

		$rv = [];

		while ($line = $sth->fetch()) {

			if ($line['is_op'])
				$line["active_nick"] = '@' . $line["active_nick"];
			else if (!$line["is_away"] && $line['is_voiced'])
				$line["active_nick"] = '+' . $line["active_nick"];

			array_push($rv, $line["active_nick"]);
		}

		return sort_nicklist($rv);
	}

	function sync_channel_topic($connection_id, $chan) {
		$dbh = DB::get();

		$sth = $dbh->prepare("UPDATE ttirc_channels
			SET topic = ex.topic, topic_owner = ex.topic_owner, topic_set = ex.topic_set
				FROM (SELECT topic, topic_owner, topic_set
					FROM ttirc_channels ch, ttirc_connections cs
					WHERE LOWER(channel) = LOWER(:chan)
					AND cs.title = '_ttirc_instance'
					AND cs.id = ch.connection_id
					AND ch.connection_id != :connection_id LIMIT 1) AS ex
			WHERE connection_id = :connection_id AND LOWER(channel) = LOWER(:chan)");

		$sth->execute([':chan' => $chan, ':connection_id' => $connection_id]);

	}

	function relay_others($except_connection_id, $channel, $message,
		$message_type = MSGT_PRIVMSG, $sender = false) {

		if ($message === "") return;

		$dbh = DB::get();

		$sth = false;

		if ($channel[0] == "#") {

			if (!$sender) {
				if ($channel != "---") {
					$sender = get_nick($except_connection_id);
				} else {
					$sender = "---";
				}
			}

			$userhost = mb_substr($_SESSION['name'] . '@local', 0, 119);

			/* $sth = $dbh->prepare("INSERT INTO ttirc_messages
					(incoming, connection_id, channel, sender, message, message_type, ts, userhost)
						SELECT ?, cs.id, ?, ?, ?, ?, NOW(), ? FROM ttirc_connections cs, ttirc_channels ch
							WHERE title = '_ttirc_instance' AND ch.connection_id = cs.id
								AND LOWER(channel) = LOWER(?) AND cs.id != ?");

			$sth->execute([true, $channel, $sender, $message, $message_type, $userhost,
				$channel, $except_connection_id]); */

			global $redis;

			$sth = $dbh->prepare("SELECT cs.id, cs.owner_uid FROM ttirc_connections cs, ttirc_channels ch
						WHERE title = '_ttirc_instance' AND ch.connection_id = cs.id AND LOWER(channel) = LOWER(?) AND cs.id != ?");

			$res = $sth->execute([$channel, $except_connection_id]);

			$message_id = message_id($redis);

			$pipe = $redis->pipeline();

			while ($row = $sth->fetch()) {
				$pipe->rpush("ttirc_messages,".$row['id'], json_encode([
					"id" => $message_id,
					"sender" => $sender,
					"message" => $message,
					"message_type" => $message_type,
					"userhost" => $userhost,
					"ts" => time(),
					"channel" => $channel
				]));

				$pipe->set("ttirc_last_message,".$row['owner_uid'], $message_id);

				$pipe->publish("ttirc_connection," . $row["id"], "UPDATE");
				$pipe->publish("ttirc_user," . $row["owner_uid"], "UPDATE");
			}

			$pipe->exec();
		}
	}

	function relay_message($connection_id, $channel, $message,
		$message_type = MSGT_PRIVMSG, $sender = false, $sender_kind = false) {

		$dbh = DB::get();

		if ($message === "") return;

		if (!$sender) {
			if ($channel != "---") {
				$sender = get_nick($connection_id);
			} else {
				$sender = "---";
			}
		}

		if ($channel == "---" || $channel[0] == "#") {

			if (is_instance($connection_id)) {
				$userhost = mb_substr($_SESSION['name'] . '@local', 0, 119);
			} else {
				$userhost = null;
			}

			$sth = false;

			global $redis;

			/* if ($channel == "---") {
				$sth = $dbh->prepare("INSERT INTO ttirc_messages
					(incoming, connection_id, channel, sender, message, message_type, ts, userhost)
						SELECT ?, cs.id, ?, ?, ?, ?, NOW(), ? FROM ttirc_connections cs
							WHERE title = '_ttirc_instance' AND LOWER(active_nick) = LOWER(?)");
			} else {

				$sth = $dbh->prepare("INSERT INTO ttirc_messages
					(incoming, connection_id, channel, sender, message, message_type, ts, userhost)
						SELECT ?, cs.id, ?, ?, ?, ?, NOW(), ? FROM ttirc_connections cs, ttirc_channels ch
							WHERE title = '_ttirc_instance' AND ch.connection_id = cs.id AND LOWER(channel) = LOWER(?)");
			}

			$sth->execute([true, $channel, $sender, $message, $message_type, $userhost, $channel]); */

			if ($channel == "---") {
				$owner_uid = connection_owner($connection_id);
				$message_id = message_id($redis);

				$redis->rpush("ttirc_messages,$connection_id", json_encode([
					"id" => $message_id,
					"sender" => $sender,
					"message" => $message,
					"message_type" => $message_type,
					"userhost" => $userhost,
					"ts" => time(),
					"channel" => $channel,
					"incoming" => true,
					"sender_kind" => $sender_kind
				]));

				$redis->set("ttirc_last_message,$owner_uid", $message_id);

				$redis->publish("ttirc_connection,$connection_id", "UPDATE");
				$redis->publish("ttirc_user,$owner_uid", "UPDATE");
			} else {

				$sth = $dbh->prepare("SELECT cs.id, cs.owner_uid FROM ttirc_connections cs, ttirc_channels ch
							WHERE title = '_ttirc_instance' AND ch.connection_id = cs.id AND LOWER(channel) = LOWER(?)");

				$sth->execute([$channel]);

				$message_id = message_id($redis);

				$pipe = $redis->pipeline();

				while ($row = $sth->fetch()) {
					$pipe->rpush("ttirc_messages,".$row['id'], json_encode([
						"id" => $message_id,
						"sender" => $sender,
						"message" => $message,
						"message_type" => $message_type,
						"userhost" => $userhost,
						"ts" => time(),
						"channel" => $channel,
						"incoming" => true,
						"sender_kind" => $sender_kind
					]));

					$pipe->set("ttirc_last_message,".$row['owner_uid'], $message_id);

					$pipe->publish("ttirc_connection," . $row["id"], "UPDATE");
					$pipe->publish("ttirc_user," . $row["owner_uid"], "UPDATE");
				}

				$pipe->exec();
			}

		} else {

			$sth = $dbh->prepare("SELECT id FROM ttirc_connections WHERE
				title = '_ttirc_instance' AND LOWER(active_nick) = LOWER(?) LIMIT 1");
			$sth->execute([$channel]);

			if ($row = $sth->fetch()) {

				$dst_conn_id = (int) $row['id'];
				$dst_has_chan = false;

				push_message($connection_id, $channel, $message,
					false, $message_type,
					$sender, $sender_kind);

				$sth = $dbh->prepare("SELECT cs.id FROM ttirc_channels ch, ttirc_connections cs
					WHERE	ch.connection_id = cs.id
					AND cs.id = ?
					AND LOWER(ch.channel) = LOWER(?)");
				$sth->execute([$dst_conn_id, $sender]);

				if (!$sth->fetch()) {

					$sth = $dbh->prepare("INSERT INTO ttirc_channels
						(channel, connection_id, chan_type) VALUES
						(?, ?, ?) RETURNING id");
					$sth->execute([$sender, $dst_conn_id, CT_PRIVATE]);

					if ($row = $sth->fetch()) {
						$dst_has_chan = $row['id'];

						update_last_chanmod(connection_owner($dst_conn_id));
					}

				} else {
					$dst_has_chan = true;
				}

				if ($dst_has_chan) {
					push_message($dst_conn_id,
						$sender, $message, true, $message_type, $sender, $sender_kind);
				}

			} else {
				push_message($connection_id, "---",
					"No such nick: $channel", true, MSGT_SYSTEM);
			}
		}

	}

	function handle_command($connection_id, $channel, $message) {

		$dbh = DB::get();

		list ($command, $arguments) = explode(" ", $message, 2);

		$command = mb_substr(trim(mb_strtolower($command)), 1);
		$arguments = trim($arguments);

		if ($command == "j") $command = "join";

		if ($command == "me") {
			$command = "action";

			if (is_instance($connection_id)) {

				relay_message($connection_id, $channel,
					$arguments, MSGT_ACTION);

			} else {
				push_message($connection_id, $channel,
					"$arguments", true, MSGT_ACTION);
			}
		}

		if ($command == "notice") {
			list ($nick, $message) = explode(" ", $arguments, 2);
			push_message($connection_id, $channel,
				"$message", false, MSGT_NOTICE, $nick);
		}

		switch ($command) {
		case "query":

			$dbh->beginTransaction();

			$sth = $dbh->prepare("SELECT id FROM ttirc_channels WHERE
				LOWER(channel) = LOWER(?) AND connection_id = ?");
			$sth->execute([strip_tags($arguments), $connection_id]);

			if (!$sth->fetch()) {
				$sth = $dbh->prepare("INSERT INTO ttirc_channels
					(channel, connection_id, chan_type) VALUES
					(?, ?, ?)");
				$sth->execute([$arguments, $connection_id, CT_PRIVATE]);

				$dbh->commit();

				update_last_chanmod(connection_owner($connection_id));

			} else {
				$dbh->commit();
			}

			break;
		case "part":

			if (!$arguments) $arguments = $channel;

			$sth = $dbh->prepare("SELECT
					chan_type, title = '_ttirc_instance' AS is_instance
				FROM ttirc_channels ch, ttirc_connections cs WHERE
					ch.connection_id = cs.id AND
					LOWER(channel) = LOWER(?) AND
					connection_id = ?");

			$sth->execute([$arguments, $connection_id]);

			if ($row = $sth->fetch()) {
				$chan_type = $row['chan_type'];
				$is_instance = $row['is_instance'];

				if ($chan_type == CT_PRIVATE || $is_instance) {

					$sth = $dbh->prepare("DELETE FROM ttirc_channels WHERE
						LOWER(channel) = LOWER(?) AND connection_id = ?");
					$sth->execute([$arguments, $connection_id]);

					if ($is_instance) {

						relay_others($connection_id, $arguments,
							"PART:" . get_nick($connection_id) . ":",
							MSGT_EVENT, "---");

						$sth = $dbh->prepare("DELETE FROM ttirc_messages WHERE
							connection_id = ? AND LOWER(channel) = LOWER(?)");
						$sth->execute([$connection_id, $arguments]);

						update_last_chanmod($_SESSION['uid']);
					}
				} else {
					push_message($connection_id, $channel,
						"$command:$arguments", false, MSGT_COMMAND);
				}
			}

			break;
		default:
			if (is_instance($connection_id)) {

				if (!get_nick($connection_id) && $command != "nick") {

					push_message($connection_id, "---",
						"Please set your nickname first (/nick ...)", true, MSGT_SYSTEM);

					return;
				}

				switch ($command) {
					case "msg":
						list($channel, $message) = explode(" ", $arguments, 2);

						if ($channel && $message) {
							relay_message($connection_id, strip_tags($channel),
								$message);
						}
						break;
					case "join":
						$arguments = strip_tags($arguments);

						if ($arguments[0] == "#") {
							$dbh->beginTransaction();

							$sth = $dbh->prepare("SELECT id FROM ttirc_channels WHERE
								LOWER(channel) = LOWER(?) AND connection_id = ?");

							$sth->execute([$arguments, $connection_id]);

							if (!$sth->fetch()) {

								$sth = $dbh->prepare("INSERT INTO ttirc_channels (channel, connection_id, chan_type) VALUES
									(?, ?, ?)");
								$res = $sth->execute([$arguments, $connection_id, CT_CHANNEL]);

								sync_channel_topic($connection_id, $arguments);

								$dbh->commit();

								update_last_chanmod(connection_owner($connection_id));

								$userhost = mb_substr($_SESSION['name'] . '@local', 0, 119);

								relay_message($connection_id, $arguments,
									"JOIN:" . get_nick($connection_id) . ":$userhost",
									MSGT_EVENT);

								#push_message($connection_id, CT_PRIVATE, "---", "---", MSGT_SYSTEM);
							} else {
								$dbh->commit();
							}
						}

						break;
					case "nick":
						$new_nick = validate_nick($arguments);
						$old_nick = get_nick($connection_id);

						if ($new_nick && $new_nick != $old_nick) {
							$dbh->beginTransaction();

							$sth = $dbh->prepare("SELECT id FROM ttirc_connections
								WHERE title = '_ttirc_instance' AND LOWER(active_nick) = LOWER(?)");
							$sth->execute([$new_nick]);

							if (!$sth->fetch()) {

								$sth = $dbh->prepare("SELECT channel FROM ttirc_channels WHERE
									connection_id = ? AND chan_type = ?");
								$sth->execute([$connection_id, CT_CHANNEL]);

								$notify_channels = [];

								while ($line = $sth->fetch()) {
									array_push($notify_channels, $line["channel"]);
								}

								$sth = $dbh->prepare("UPDATE ttirc_connections SET active_nick = ?
									WHERE id = ?");
								$sth->execute([$new_nick, $connection_id]);

								global $connection_nick_cache;
								unset($connection_nick_cache[$connection_id]);

								$sth = $dbh->prepare("UPDATE ttirc_channels
								  	SET channel = ?
									WHERE id IN (SELECT ch.id
										FROM ttirc_channels ch, ttirc_connections cs
										WHERE	ch.connection_id = cs.id
										AND cs.title = '_ttirc_instance'
										AND LOWER(channel) = LOWER(?))");

								$sth->execute([$new_nick, $old_nick]);

								$dbh->commit();

								update_last_chanmod($_SESSION["uid"]);

								foreach ($notify_channels as $channel)	{
									relay_message($connection_id, $channel, "NICK:$new_nick", MSGT_EVENT, $old_nick);
								}

							} else {
								$dbh->commit();

								push_message($connection_id, "---",
									"Duplicate nick not allowed: $arguments", true, MSGT_SYSTEM);
							}
						}

						break;
					case "topic":

						if ($arguments) {

							$dbh->beginTransaction();

							$sth = $dbh->prepare("SELECT cs.id FROM ttirc_connections cs, ttirc_channels ch WHERE
								ch.connection_id = cs.id AND LOWER(channel) = LOWER(?) AND title = '_ttirc_instance'");
							$sth->execute([$channel]);

							$mynick = get_nick($connection_id);

							while ($line = $sth->fetch()) {

								$usth = $dbh->prepare("UPDATE ttirc_channels SET topic = ?,
									topic_owner = ?, topic_set = NOW()
									WHERE connection_id = ? AND LOWER(channel) = LOWER(?)");

								$usth->execute([$arguments, $mynick, $line['id'], $channel]);

							}

							$dbh->commit();

							relay_message($connection_id, $channel, "TOPIC:$arguments",
								MSGT_EVENT);

							update_last_chanmod($_SESSION["uid"]);

						} else {

							$sth = $dbh->prepare("SELECT topic, topic_owner, topic_set FROM ttirc_channels WHERE
								LOWER(channel) = LOWER(?) AND connection_id = ?");
							$sth->execute([$channel, $connection_id]);

							if ($row = $sth->fetch()) {
								$message = sprintf("Topic for %s is: %s", $channel, $row['topic']);

								push_message($connection_id, $channel, $message,
									true, MSGT_SYSTEM);

								$message = sprintf("Topic for %s set by %s at %s",
									$channel, $row['topic_owner'], mb_substr($row['topic_set'], 0, 16));

								push_message($connection_id, $channel, $message,
									true, MSGT_SYSTEM);

							}
						}

						break;
					case "action":
						break;

				default:
					push_message($connection_id, "---",
						"Command not understood: $command ($arguments)", true, MSGT_SYSTEM);
				}


			} else {
				push_message($connection_id, $channel,
					"$command:$arguments", false, MSGT_COMMAND);
			}
			break;
		}
	}

	function connection_owner($connection_id) {
		if (isset($_SESSION['connection_owner_cache']) && isset($_SESSION['connection_owner_cache'][$connection_id]))
			return $_SESSION['connection_owner_cache'][$connection_id];

		if (!isset($_SESSION['connection_owner_cache']))
			$_SESSION['connection_owner_cache'] = [];

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT owner_uid FROM ttirc_connections WHERE id = ?");
		$sth->execute([$connection_id]);

		if ($row = $sth->fetch()) {
			$_SESSION['connection_owner_cache'][$connection_id] = $row['owner_uid'];

			return $row['owner_uid'];
		} else {
			return -1;
		}
	}

	function push_message($connection_id, $channel, $message,
		$incoming = false, $message_type = MSGT_PRIVMSG, $from_nick = false, $sender_kind = false) {

		if ($message === "") return false;

		$dbh = DB::get();

		$incoming = bool_to_sql_bool($incoming);

		if ($channel != "---") {
			$my_nick = get_nick($connection_id);
		} else {
			$my_nick = "---";
		}

		if ($from_nick) $my_nick = $from_nick;

		if (is_instance($connection_id)) {
			$userhost = mb_substr($_SESSION['name'] . '@local', 0, 119);
		} else {
			$userhost = null;
		}

		/* $sth = $dbh->prepare("INSERT INTO ttirc_messages
			(incoming, connection_id, channel, sender, message, message_type, ts, userhost) VALUES
			(?, ?, ?, ?, ?, ?, NOW(), ?)");

		$sth->execute([$incoming, $connection_id, $channel, $my_nick, $message,
			$message_type, $userhost]); */

		global $redis;

		$message_id = message_id($redis);

		$redis->rpush("ttirc_messages,$connection_id", json_encode([
			"id" => $message_id,
			"sender" => $my_nick,
			"message" => $message,
			"message_type" => $message_type,
			"userhost" => $userhost,
			"ts" => time(),
			"channel" => $channel,
			"incoming" => $incoming,
			"sender_kind" => $sender_kind
		]));

		$owner_uid = connection_owner($connection_id);

		$redis->set("ttirc_last_message," . $owner_uid,
			$message_id);

		$redis->publish("ttirc_connection,$connection_id", "UPDATE");
		$redis->publish("ttirc_user,$owner_uid", "UPDATE");
	}

	/* function get_initial_last_id($offset = 0) {
		$dbh = DB::get();

		if (!$offset) $offset = DEFAULT_BUFFER_SIZE;

		$sth = $dbh->prepare("SELECT ttirc_messages.id
			FROM ttirc_messages, ttirc_connections
			WHERE
				owner_uid = ? AND
				connection_id = ttirc_connections.id AND
				channel != '---'
			ORDER BY id DESC LIMIT 1 OFFSET $offset");
		$sth->execute([$_SESSION['uid']]);

		if ($row = $sth->fetch()) {
			return $row['id'];
		} else {
			return 0;
		}
	} */

	function has_new_lines($last_id) {
		global $redis;

		return $redis->get("ttirc_last_message,".$_SESSION['uid']) > $last_id;

		/* $dbh = DB::get();

		if (!$last_id) $last_id = get_initial_last_id($bufsize);
		if (!$bufsize) $bufsize = DEFAULT_BUFFER_SIZE;

		$sth = $dbh->prepare("SELECT COUNT(*) AS cl
			FROM ttirc_messages, ttirc_connections WHERE
			connection_id = ttirc_connections.id AND
			message_type != ? AND
			ttirc_messages.id > ? AND
			owner_uid = ? LIMIT $bufsize");

		$sth->execute([MSGT_COMMAND, $last_id, $_SESSION['uid']]);

		if ($row = $sth->fetch()) {
			return $row['cl'];
		} else {
			return -1;
		} */
	}

	function get_history_lines($connection_id, $channel, $offset = 0) {

		if (connection_owner($connection_id) != $_SESSION['uid'])
			return false;

		if (!$offset) $offset = 0;

		/* $sth = $dbh->prepare("SELECT * FROM (SELECT ttirc_messages.id,
			message_type, sender, channel, connection_id, incoming,
			userhost, message, ".SUBSTRING_FOR_DATE."(ts,1,19) AS ts
			FROM ttirc_messages, ttirc_connections WHERE
			connection_id = ttirc_connections.id AND
			connection_id = ? AND
			channel = ? AND
			message_type != ? AND
			owner_uid = ? ORDER BY id DESC LIMIT ? OFFSET ?) AS iq ORDER BY id");

		$sth->execute([$connection_id, $channel, MSGT_COMMAND, $_SESSION['uid'], DEFAULT_BUFFER_SIZE, $offset]);

		$lines = [];

		while ($line = $sth->fetch(PDO::FETCH_ASSOC)) {
			$line["sender_color"] = color_of($line["sender"]);
			$line["incoming"] = sql_bool_to_bool($line["incoming"]);
			array_push($lines, $line);
		} */

		$lines = [];

		global $redis;

		for ($start = -DEFAULT_BUFFER_SIZE; $start > -KEEP_MESSAGES_MAX; $start-=DEFAULT_BUFFER_SIZE) {
			$end = $start+DEFAULT_BUFFER_SIZE-1;

			$tmplines = $redis->lrange("ttirc_messages,$connection_id", $start, $end);

			for ($i = count($tmplines); $i >= 0; $i--) {
				$line = json_decode($tmplines[$i], true);

				if ($line['channel'] == $channel) {
					$line["connection_id"] = $connection_id;
					$line["sender_color"] = color_of($line["sender"]);
					$line["ts"] = date("Y-m-d H:i:s", $line['ts']);

					array_push($lines, $line);
				}
			}

			if (count($tmplines) < DEFAULT_BUFFER_SIZE)
				break; // end
		}

		$lines = array_slice($lines, $offset, DEFAULT_BUFFER_SIZE);

		return $lines;
	}

	function get_new_lines($last_id, $bufsize = DEFAULT_BUFFER_SIZE) {

		/* if (!$last_id) $last_id = get_initial_last_id($bufsize);

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT ttirc_messages.id,
			message_type, sender, channel, connection_id, incoming,
			userhost, message, ".SUBSTRING_FOR_DATE."(ts,1,19) AS ts
			FROM ttirc_messages, ttirc_connections WHERE
			connection_id = ttirc_connections.id AND
			message_type != ? AND
			ttirc_messages.id > ? AND
			owner_uid = ? ORDER BY id LIMIT ?");

		$sth->execute([MSGT_COMMAND, $last_id, $_SESSION['uid'], $bufsize]);

		$lines = [];

		while ($line = $sth->fetch(PDO::FETCH_ASSOC)) {
			$line["sender_color"] = color_of($line["sender"]);
			$line["incoming"] = sql_bool_to_bool($line["incoming"]);
			array_push($lines, $line);
		} */

		$lines = [];

		global $redis;

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT cs.id FROM ttirc_connections cs WHERE owner_uid = ?");
		$res = $sth->execute([$_SESSION['uid']]);

		while ($row = $sth->fetch()) {
			// trim to last KEEP_MESSAGES_MAX entries per channel
			$redis->ltrim("ttirc_messages," . $row['id'], -KEEP_MESSAGES_MAX, -1);

			$start_from = $last_id ? -KEEP_MESSAGES_MAX : -$bufsize-1;

			for ($start = -$bufsize; $start > $start_from; $start-=$bufsize) {
				$end = $start+$bufsize-1;

				$found = 0;

				foreach ($redis->lrange("ttirc_messages," . $row['id'], $start, $end) as $line) {
					$line = json_decode($line, true);

					#print $line['id'] . " " . $line['message'] . "\n";

					if ($line['id'] > $last_id - 4) {
						$line["connection_id"] = $row['id'];
						$line["sender_color"] = color_of($line["sender"]);
						$line["ts"] = date("Y-m-d H:i:s", $line['ts']);

						array_push($lines, $line);
						$found++;
					}
				}

				#print $row['id'] . ":$start,$end,$found,$last_id\n";

				if ($found == 0) break;
			}
		}

		return $lines;
	}

	function get_chan_data() {
		global $redis;

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT nicklist,channel,connection_id,
			chan_type,topic,ttirc_channels.muted,
			topic_owner,".SUBSTRING_FOR_DATE."(topic_set,1,16) AS topic_set
			FROM ttirc_channels, ttirc_connections
			WHERE connection_id = ttirc_connections.id AND
			visible = true AND
			owner_uid = ?");
		$sth->execute([$_SESSION['uid']]);

		$rv = [];

		while ($line = $sth->fetch(PDO::FETCH_ASSOC)) {
			$chan = $line["channel"];

			$re = array();

			$re["chan_type"] = $line["chan_type"];

			if (is_instance($line["connection_id"])) {
				$re["users"] = instance_nicklist($line["channel"]);
			} else {
				$re["users"] = sort_nicklist(json_decode($line["nicklist"]));
			}

			$re["last_read"] = (int) $redis->get("ttirc_last_read,".$_SESSION['uid'].",".$line['connection_id'].",".$line['channel']);

			$re["muted"] = sql_bool_to_bool($line["muted"]);
			$re["topic"] = array(
				$line["topic"], $line["topic_owner"], $line["topic_set"]);

			$rv[$line["connection_id"]][$chan] = $re;

		}

		/* set initial value if missing */
		if (!$redis->get("ttirc_last_chanmod,".$_SESSION['uid']))
			$redis->incr("ttirc_last_chanmod,".$_SESSION['uid']);

		return $rv;
	}

	function get_conn_info() {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT id,active_server,active_nick,status,muted,
			title,userhosts
			FROM ttirc_connections
			WHERE visible = true AND owner_uid = ?
			ORDER BY title != '_ttirc_instance', title");

		$sth->execute([$_SESSION['uid']]);

		$conn = array();

		while ($line = $sth->fetch(PDO::FETCH_ASSOC)) {

			$line["muted"] = sql_bool_to_bool($line["muted"]);

			if ($line["title"] == "_ttirc_instance") {
				$line["status"] = CS_CONNECTED;
				$line["title"] = __("Local");
				$line["active_server"] = __("<local>");
				$line["is_instance"] = true;

				$line['userhosts'] = instance_userhosts($line["id"]);
			} else {
				if ($line['userhosts'])
					$line['userhosts'] = json_decode($line['userhosts']);

				// should always be an array
				if (!$line['userhosts']) $line['userhosts'] = [];
			}

			array_push($conn, $line);
		}

		return $conn;
	}

	function sql_bool_to_string($s) {
		if ($s == "t" || $s == "1" || strtolower($s) == "true") {
			return "true";
		} else {
			return "false";
		}
	}

	function sql_bool_to_bool($s) {
		if ($s == "t" || $s == "1" || strtolower($s) == "true") {
			return true;
		} else {
			return false;
		}
	}

	function bool_to_sql_bool($s) {
		if ($s) {
			return "true";
		} else {
			return "false";
		}
	}

	function sanity_check($plaintext = false) {

		global $ERROR_MESSAGES;

		$error_code = 0;

		try {
			$schema_version = get_schema_version();

			if ($schema_version != SCHEMA_VERSION) {
				$error_code = 5;
			}

		} catch (PDOException $e) {
			$error_code = 16;
		}

		global $redis;

		try {
			$redis->ping();
		} catch (CredisException $e) {
			$error_code = 15;
		}

		if ($error_code != 0) {
			if ($plaintext) {
				?>
					<!DOCTYPE html>
					<html>
					<head>
					<title>Startup failed</title>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<style type="text/css">
						body {
							background : #900;
							color : white;
						}
						</style>
					</head>
				<body>
					<h1>Startup failed</h1>

					<ul>
					<li><?php echo $error_code . ": " . $ERROR_MESSAGES[$error_code] ?></li>
					</ul>
				</body>
				</html>
			<?php
			} else {
				print json_encode(array("error" => $error_code,
					"errormsg" => $ERROR_MESSAGES[$error_code]));
			}
			return false;
		} else {
			return true;
		}
	}

	function get_random_server($connection_id) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT * FROM ttirc_servers WHERE
			connection_id = ? ORDER BY RANDOM() LIMIT 1");
		$sth->execute([$connection_id]);

		if ($row = $sth->fetch()) {
			return $row;
		} else {
			return false;
		}

	}

	// shamelessly stolen from xchat source code

	function color_of($name) {

		$rcolors = array( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		  13, 14, 15 );

		$i = 0;
		$sum = 0;

		for ($i = 0; $i < strlen($name); $i++) {
			$sum += ord($name[$i]);
		}

		$sum %= count($rcolors);

		return $rcolors[$sum];
	}

	function update_last_message() {

		$dbh = DB::get();

		if (time() - $_SESSION["message_last"] > 180) {

			$sth = $dbh->prepare("UPDATE ttirc_users SET last_message = NOW()
				WHERE id = ?");
			$sth->execute([$_SESSION['uid']]);

			$_SESSION["message_last"] = time();
		}
	}

	function update_heartbeat() {

		$dbh = DB::get();

		if (time() - $_SESSION["heartbeat_last"] > 180) {

			$sth = $dbh->prepare("SELECT id FROM
				ttirc_users WHERE id = ? AND heartbeat < NOW() - interval '15 min'");
			$sth->execute([$_SESSION['uid']]);

			if ($sth->fetch()) {

				$sth = $dbh->prepare("SELECT channel, connection_id, active_nick
					FROM ttirc_channels ch, ttirc_connections cs WHERE
					ch.connection_id = cs.id AND
					cs.owner_uid = ? AND
					title = '_ttirc_instance' AND
					chan_type = ?");

				$sth->execute([$_SESSION['uid'], CT_CHANNEL]);

				while ($line = $sth->fetch()) {
					relay_others($line['connection_id'], $line["channel"],
						"UNAWAY:" . $line['active_nick'],
						MSGT_EVENT);
				}
			}

			$sth = $dbh->prepare("UPDATE ttirc_users SET heartbeat = NOW()
				WHERE id = ?");
			$sth->execute([$_SESSION['uid']]);

			$_SESSION["heartbeat_last"] = time();
		}
	}

	function initialize_user_prefs($uid, $profile = false) {

		$dbh = DB::get();

		$dbh->beginTransaction();

		$sth = $dbh->query("SELECT pref_name,def_value FROM ttirc_prefs");

		$usth = $dbh->prepare("SELECT pref_name FROM ttirc_user_prefs WHERE owner_uid = ?");
		$usth->execute([$uid]);

		$active_prefs = array();

		while ($line = $usth->fetch()) {
			array_push($active_prefs, $line["pref_name"]);
		}

		while ($line = $sth->fetch()) {
			if (array_search($line["pref_name"], $active_prefs) === false) {
//				print "adding " . $line["pref_name"] . "<br>";

				$isth = $dbh->prepare("INSERT INTO ttirc_user_prefs
					(owner_uid,pref_name,value, profile) VALUES
					(?, ?, ?, null)");

				$isth->execute([$uid, $line['pref_name'], $line['def_value']]);
			}
		}

		$sth = $dbh->prepare("SELECT id FROM ttirc_connections WHERE
			title = '_ttirc_instance' AND owner_uid = ?");
		$sth->execute([$uid]);

		if (!$sth->fetch()) {
			$sth = $dbh->prepare("INSERT INTO ttirc_connections (title, owner_uid)
				VALUES ('_ttirc_instance', ?)");
			$sth->execute([$uid]);
		}

		$dbh->commit();

	}

	function valid_connection($connection_id) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT id FROM ttirc_connections
			WHERE id = ? AND owner_uid = ?");
		$sth->execute([$connection_id, $_SESSION["uid"]]);

		return $sth->fetch();
	}

	function make_password($length = 8) {

		$password = "";
		$possible = "0123456789abcdfghjkmnpqrstvwxyzABCDFGHJKMNPQRSTVWXYZ";

   	$i = 0;

		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		}
		return $password;
	}

	function get_user_email($user) {

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT email FROM ttirc_users WHERE login = ?");
		$sth->execute([$user]);

		if ($row = $sth->fetch()) {
			return $row['email'];
		}

		return "";
	}

	function get_user_host($nick) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT login FROM ttirc_connections cs, ttirc_users u
				WHERE cs.owner_uid = u.id AND LOWER(active_nick) = LOWER(?) LIMIT 1");
		$sth->execute([$nick]);

		if ($row = $sth->fetch()) {
			return $row['nick'] . '@local';
		} else {
			return false;
		}
	}


	function get_user_login($uid) {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT login FROM ttirc_users WHERE id = ?");
		$sth->execute([$uid]);

		if ($row = $sth->fetch()) {
			return $row['login'];
		} else {
			return false;
		}

	}

	function get_iconv_encodings() {
		return explode("\n", (string)file_get_contents("lib/iconv.txt"));
	}

	function print_theme_select() {
		$user_theme = get_pref("USER_THEME");
		$themes = get_all_themes();

		usort($themes, function($a,$b) {
			return strcmp($a['name'], $b['name']);
		});

		print "<select class=\"form-control\" name=\"theme\">";
		print "<option value=''>".__('Default')."</option>";
		print "<option disabled>--------</option>";

		foreach ($themes as $t) {
			$base = $t['base'];
			$name = $t['name'];

			if ($base == $user_theme) {
				$selected = "selected=\"1\"";
			} else {
				$selected = "";
			}

			print "<option $selected value='$base'>".htmlspecialchars($name)."</option>";

		}

		print "</select>";
	}

	function print_user_css() {
		$user_css = get_pref("USER_STYLESHEET");

		if ($user_css) {
			print "<style type=\"text/css\">$user_css</style>";
		}
	}

	function get_misc_params($uniqid = false) {
		if (!$uniqid) $uniqid = uniqid();

		$dbh = DB::get();

		$notify_on = json_decode(get_pref("NOTIFY_ON"));

		if (!is_array($notify_on)) $notify_on = array();

		$notify_events = array();

		foreach ($notify_on as $no) {
			$notify_events[$no] = true;
		}

		$sth = $dbh->prepare("SELECT hide_join_part FROM ttirc_users WHERE id = ?");
		$sth->execute([$_SESSION['uid']]);
		$row = $sth->fetch();

		$hide_join_part = $row["hide_join_part"];

		$row = $dbh->query("SELECT value FROM ttirc_system WHERE	".DB_KEY_FIELD." = 'MASTER_RUNNING'")->fetch();
		$master_running = $row['value'] == 'true'; # text field

		$rv = array(
			"hide_join_part" => $hide_join_part,
			"uniqid" => $uniqid,
			"master_running" => $master_running,
			"fetch_url_titles" => defined('FETCH_URL_TITLES') && FETCH_URL_TITLES,
			"disable_image_preview" => get_pref("DISABLE_IMAGE_PREVIEW"),
			"highlight_on" => explode(",", get_pref("HIGHLIGHT_ON")),
			"emoticons_mtime" => is_readable(EMOTICONS_MAP) ? filemtime(EMOTICONS_MAP) : -1,
			"emoticons_favorite" => get_favorite_emoticons(),
			"sid" => session_id(),
			"default_buffer_size" => DEFAULT_BUFFER_SIZE,
			"notify_events" => $notify_events);

		return $rv;
	}

	function is_server_https() {
		return (!empty($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] != 'off')) || (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https');
	}

	function validate_csrf($csrf_token) {
		return isset($csrf_token) && hash_equals($_SESSION['csrf_token'], $csrf_token);
	}

	function get_self_url_prefix() {
		$url_path = "";

		if (is_server_https()) {
			$url_path = "https://";
		} else {
			$url_path = "http://";
		}

		$url_path .= $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);

		return $url_path;

	}

	function truncate_string($str, $max_len) {
		if (mb_strlen($str, "utf-8") > $max_len - 3) {
			return trim(mb_substr($str, 0, $max_len, "utf-8")) . "...";
		} else {
			return $str;
		}
	}

	function get_random_bytes($length) {
		if (function_exists('random_bytes')) {
			return random_bytes($length);
		} else if (function_exists('openssl_random_pseudo_bytes')) {
			return openssl_random_pseudo_bytes($length);
		} else {
			$output = "";

			for ($i = 0; $i < $length; $i++)
				$output .= chr(mt_rand(0, 255));

			return $output;
		}
	}

	function cleanup_session_cache() {
		if (is_array($_SESSION["cache"])) {
			foreach (array_keys($_SESSION["cache"]) as $uniqid) {
				if (time() - $_SESSION["cache"][$uniqid]["last"] > 120) {
					unset($_SESSION["cache"][$uniqid]);
				}
			}
		}
	}

	function get_emoticons_flat() {
		$map = get_emoticons_map();
		$tmp = [];

		foreach ($map as $sn => $sv) {
			$tmp = array_merge($tmp, $sv);
		}

		return $tmp;
	}

	function get_emoticons_map() {
		if (file_exists(EMOTICONS_MAP)) {

			$obj = json_decode((string)file_get_contents(EMOTICONS_MAP), true);
			if (!$obj) return [];

			return $obj;

		} else {
			return [];
		}
	}

	function render_emoticons_full() {

		?>
		<!DOCTYPE html>
		<html>
		<head>
			<title>Tiny Tiny IRC</title>
			<?php echo stylesheet_tag("css/default.css") ?>
			<?php stylesheet_tag("lib/bootstrap/css/bootstrap.min.css") ?>
			<?php stylesheet_tag("lib/bootstrap/css/bootstrap-theme.min.css") ?>
			<?php javascript_tag("js/functions.js") ?>
			<?php javascript_tag("lib/bootstrap/js/jquery.js") ?>
			<?php javascript_tag("lib/localforage.min.js") ?>
		</head>
		<body>

		<script type="text/javascript">
			var emoticons_map = {};
			var emoticons_full = {};

			function render_section(section, query) {
				var map = emoticons_full[section];

				var list = $(".em-list")
									.html("");

				if (query) map = emoticons_map;

				var sections = Object.keys(map).sort();

				$.each(sections, function(i, k) {

					var v = map[k];

					if (!query || k.match(query) || (v.hint && v.hint.match(query))) {

						var img = $("<img>")
							.attr('src', 'emoticons/' + v.file)
							.attr('alt', k)
							.attr('title', k + (v.hint ? '\n' + v.hint : '') + "\n" + v.height + "px")
							.on('click', function() {
								window.opener.inject_text(k);
							});

						var cell = $("<li>").attr('data-title', v.hint)
							.append($("<div>")
								.attr('class', 'wrapper ' + (v.height >= 200 ? 'emoticon-hd' : ''))
								.append(img));

						list.append(cell);
					}
				});
			}

			function init() {

				$(".em-search").on('change', function() {
					var q = $(this).val();
					var s = $(".em-sections").val();

					window.setTimeout(function() {
						render_section(s, q);
					}, 10);
				});

				$(".em-sections").on('change', function() {
					$(".em-search").val('');

					var s = $(this).val();

					window.setTimeout(function() {
						render_section(s, '');
					}, 10);
				});

				$.each(Object.keys(emoticons_full).sort(), function (i, k) {
					$(".em-sections").append(
					$("<option>").html(k));
				});

				$(".em-sections").val('emoticons');

				render_section('emoticons', '');

			}

			$(document).ready(function() {
				$("#search").focus();

				localforage.getItem("ttirc.emoticons-cache").then(function(obj) {
					if (obj) {
						emoticons_full = obj.fullmap;
						emoticons_map = obj.map;

						init();
					} else {
						model.updateEmoticons();
					}
				});
			});

		</script>

		<div class="panel panel-default">

		<div class="panel-heading">

		<form class="form form-inline pull-right" onsubmit="return false">
			<input type="search" class="em-search form-control"
				placeholder="<?php echo __("Search... ")?>">
		</form>

		<form class="form form-inline" onsubmit="return false">
			<select class="em-sections form-control"></select>
		</form>

		</div> <!-- /heading -->
		<ul class="panel-body emoticons-long em-list">

		<?php

		ksort($map[$section]);
		foreach ($map[$section] as $k => $e) {

			$title = $k . "\n" . $e['hint'];

			print "<li data-title=\"$title\"><div class=\"wrapper\">";
			print "<img onclick=\"window.opener.inject_text('$k')\" title=\"$title\" src=\"emoticons/{$e['file']}\">";
			print "</div>$k";
			print "</li>";
		}

		?>
		</ul>
		</div> <!-- /body -->
		</body>
		</html>
		<?php
	}

	function get_favorite_emoticons() {
		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT emoticon FROM ttirc_emoticons_popcon
					WHERE owner_uid = ? ORDER BY times_used DESC LIMIT 30");
		$sth->execute([$_SESSION['uid']]);

		$emoticons = [];

		while ($row = $sth->fetch()) {
			array_push($emoticons, $row['emoticon']);
		}

		return $emoticons;
	}

	function render_emoticons() {
		$emoticons_map = get_emoticons_flat();
		$favs = get_favorite_emoticons();

		$num_favs = 0;
		$tmp = "";

		foreach ($favs as $k) {
			++$num_favs;

			$v = $emoticons_map[$k];

			$tmp .= "<div class=\"wrapper\"><img onclick=\"inject_text('$k')\" title=\"$k\" src=\"emoticons/{$v['file']}\"></div>";

		}

		$num_more = count($emoticons_map) - $num_favs;

		if ($num_more > 0) {
			$tmp .= "<br clear='both'><p style='text-align : center;margin-top : 10px;'>
				<a href=\"#\" title=\"Show all emoticons (Alt+Shift+E)\" onclick=\"return emoticons_popup()\">$num_more more <span class='text-muted glyphicon glyphicon-new-window'> </span></a></p>";
		}

		return $tmp;
	}

	function stylesheet_tag($filename) {
		$timestamp = filemtime($filename);

		echo "<link media=\"screen\" rel=\"stylesheet\" type=\"text/css\" href=\"$filename?$timestamp\"/>\n";
	}

	function javascript_tag($filename) {
		$query = "";

		if (!(strpos($filename, "?") === false)) {
			$query = substr($filename, strpos($filename, "?")+1);
			$filename = substr($filename, 0, strpos($filename, "?"));
		}

		$timestamp = filemtime($filename);

		if ($query) $timestamp .= "&$query";

		echo "<script type=\"text/javascript\" charset=\"utf-8\" src=\"$filename?$timestamp\"></script>\n";
	}

	function init_js_translations() {

	print 'var T_messages = new Object();

		function __(msg) {
			if (T_messages[msg]) {
				return T_messages[msg];
			} else {
				return msg;
			}
		}

		function ngettext(msg1, msg2, n) {
			return (parseInt(n) > 1) ? msg2 : msg1;
		}';

		if (ENABLE_TRANSLATIONS == true) { // If translations are enabled.
			$l10n = _get_reader();

			for ($i = 0; $i < $l10n->total; $i++) {
				$orig = $l10n->get_original_string($i);
				$translation = __($orig);

				print T_js_decl($orig, $translation);
			}
		}
	}

	function strip_prefix($nick) {
		return preg_replace("/^[\+\@]/", "", $nick);
	}

	function nicklist_sort_callback($o1, $o2) {
		$c1 = $o1[0];
		$c2 = $o2[0];

		if ($c1 == '@' && $c2 != '@') {
			return -1;
		}

		if ($c1 != '@' && $c2 == '@') {
			return 1;
		}

		if ($c1 == '+' && $c2 != '+') {
			return -1;
		}

		if ($c1 != '+' && $c2 == '+') {
			return 1;
		}

		return strip_prefix($o1) < strip_prefix($o2) ? -1 : 1;
	}

	function sort_nicklist($nicklist) {
		if (is_array($nicklist)) usort($nicklist, "nicklist_sort_callback");
		return $nicklist;
	}

	function build_url($parts) {
		$tmp = $parts['scheme'] . "://" . $parts['host'] . $parts['path'];

		if (isset($parts['query'])) $tmp .= '?' . $parts['query'];
		if (isset($parts['fragment'])) $tmp .= '#' . $parts['fragment'];

		return $tmp;
	}

	// extended filtering involves validation for safe ports and loopback
	function validate_url($url, $extended_filtering = false) {

		$url = strip_tags($url);

		# fix protocol-relative URLs
		if (strpos($url, "//") === 0)
			$url = "https:" . $url;

		$tokens = parse_url($url);

		if (!$tokens['host'])
			return false;

		if (!in_array($tokens['scheme'], ['http', 'https']))
			return false;

		//convert IDNA hostname to punycode if possible
		if (function_exists("idn_to_ascii")) {
			if (mb_detect_encoding($tokens['host']) != 'ASCII') {
				$tokens['host'] = idn_to_ascii($tokens['host']);
			}
		}

		// separate set of tokens with urlencoded 'path' because filter_var() rightfully fails on non-latin characters
		// (used for validation only, we actually request the original URL, in case of urlencode breaking it)
		$tokens_filter_var = $tokens;

		if ($tokens['path']) {
			$tokens_filter_var['path'] = implode("/",
										array_map("rawurlencode",
											array_map("rawurldecode",
												explode("/", $tokens['path']))));
		}

		$url = build_url($tokens);
		$url_filter_var = build_url($tokens_filter_var);

		if (filter_var($url_filter_var, FILTER_VALIDATE_URL) === false)
			return false;

		if ($extended_filtering) {
			if (!in_array($tokens['port'], [80, 443, '']))
				return false;

			if ($tokens['host'] == 'localhost' || $tokens['host'] == '::1' || strpos($tokens['host'], '127.') === 0)
				return false;
		}

		return $url;
	}

	function resolve_redirects($url, $timeout, $nest = 0) {

		// too many redirects
		if ($nest > 10)
			return false;

		$context_options = array(
			'http' => array(
				 'header' => array(
					 'Connection: close'
				 ),
				 'method' => 'HEAD',
				 'timeout' => $timeout,
				 'protocol_version'=> 1.1)
			);

		if (defined('_HTTP_PROXY')) {
			$context_options['http']['request_fulluri'] = true;
			$context_options['http']['proxy'] = _HTTP_PROXY;
		}

		$context = stream_context_create($context_options);
		$headers = get_headers($url, 0, $context);

		if (is_array($headers)) {
			$headers = array_reverse($headers); // last one is the correct one

			foreach($headers as $header) {
				if (stripos($header, 'Location:') === 0) {
					$url = rewrite_relative_url($url, trim(substr($header, strlen('Location:'))));

					return resolve_redirects($url, $timeout, $nest + 1);
				}
			}

			return $url;
		}

		// request failed?
		return false;
	}


	function rewrite_relative_url($url, $rel_url) {

		$rel_parts = parse_url($rel_url);

		if ($rel_parts['host'] && $rel_parts['scheme']) {
			return validate_url($rel_url);
		} else if (strpos($rel_url, "//") === 0) {
			# protocol-relative URL (rare but they exist)
			return validate_url("https:" . $rel_url);
		} else if (strpos($rel_url, "magnet:") === 0) {
			# allow magnet links
			return $rel_url;
		} else {
			$parts = parse_url($url);

			$rel_parts['host'] = $parts['host'];
			$rel_parts['scheme'] = $parts['scheme'];

			if (strpos($rel_parts['path'], '/') !== 0)
				$rel_parts['path'] = '/' . $rel_parts['path'];

			$rel_parts['path'] = str_replace("/./", "/", $rel_parts['path']);
			$rel_parts['path'] = str_replace("//", "/", $rel_parts['path']);

			return validate_url(build_url($rel_parts));
		}
	}

	function arr_qmarks($arr) {
		return str_repeat('?,', count($arr) - 1) . '?';
	}

	function ffmpeg_thumbnail($data) {
		$tmpfname = tempnam("/tmp", "ttirc_ffmpeg_thumb");

		if ($tmpfname) {
			if (file_put_contents($tmpfname, $data)) {

				$cmdline_tpl = "ffmpeg -loglevel quiet -ss 00:00:01 -i %s -frames:v 1 ".
					"-f singlejpeg - 2>/dev/null";

				$cmdline = sprintf($cmdline_tpl, escapeshellarg($tmpfname));
				$buf = shell_exec($cmdline);

				unlink($tmpfname);

				return $buf;
			}
		}
	}

	// https://stackoverflow.com/questions/280658/can-i-detect-animated-gifs-using-php-and-gd
	function is_animated_gif($raw) {
		$offset = 0;
		$frames = 0;
		while ($frames < 2 && $offset <= 500000) {
			$where1 = strpos($raw, "\x00\x21\xF9\x04", $offset);

			if ( $where1 === false ) {
            break;
			} else {
				$offset = $where1 + 1;
				$where2 = strpos( $raw, "\x00\x2C", $offset );
				if ($where2 === false) {
					break;
				} else {
					if ($where1 + 8 == $where2) {
						$frames ++;
					}
					$offset = $where2 + 1;
				}
			}
		}
		return $frames > 1;
	}

	function make_thumbnail($data, $dim_max_x = 600, $dim_max_y = 600, $force_stamp = false) {

		$exif = false;

		$tmpfname = tempnam(sys_get_temp_dir(), "ttirc-thumb");
		$content_type = "";

		if ($tmpfname) {
			if (file_put_contents($tmpfname, $data)) {
				$content_type = mime_content_type($tmpfname);

				if ($content_type == "image/webp") {
					$o_im = @imagecreatefromwebp($tmpfname);
				} else if ($content_type == "image/jpeg") {
					$exif = exif_read_data($tmpfname, 'IFD0');
					$o_im = imagecreatefromjpeg($tmpfname);
				}

				@unlink($tmpfname);
			}
		}

		if (!isset($o_im))
			$o_im = @imagecreatefromstring($data);

		if ($o_im) {
			$imageinfo = @getimagesizefromstring($data);

			$o_width = imagesx($o_im) ;
			$o_height = imagesy($o_im) ;

			if (max($o_width, $o_height) < max($dim_max_x, $dim_max_y)) {
				$t_height = $o_height;
				$t_width = $o_width;
			} else {
				if ($o_height > $o_width) {
					$t_height = $dim_max_x;
					$t_width = round($o_width/$o_height * $t_height);
				} else {
					$t_width = $dim_max_y;
					$t_height = round($o_height/$o_width * $t_width);
				}
			}

	//		print "$o_file : $t_file : $o_height * $o_width -> $t_height * $t_width<br>";

			$t_im = imagecreatetruecolor($t_width, $t_height);

			if ($force_stamp || ($imageinfo && $imageinfo["mime"] == "image/gif" && is_animated_gif($data))) {
				$need_stamp = true;
				$need_alpha = false;

				imagefill($t_im, 0, 0, 0xffffff);
			} else {
				$need_stamp = false;
				if ($imageinfo && $imageinfo["mime"] == "image/png") {
					$need_alpha = true;

					imagealphablending($t_im, false);
					imagesavealpha($t_im, true);
				}
			}

			imagecopyresampled($t_im, $o_im, 0, 0, 0, 0,
					$t_width, $t_height, $o_width, $o_height);

			if ($exif) {
				if (isset($exif["Orientation"])) {
					switch ($exif['Orientation']) {
						case 3:
							 $t_im = imagerotate($t_im, 180, 0);
							 break;

						case 6:
							 $t_im = imagerotate($t_im, -90, 0);
							 break;

						case 8:
							 $t_im = imagerotate($t_im, 90, 0);
							 break;
				  }
				}
			}

			if ($need_stamp) {
				$stamp = imagecreatefrompng('images/play-outline.png');

				if ($stamp) {
					$sx = imagesx($stamp);
					$sy = imagesy($stamp);

					imagecopy($t_im, $stamp,
						imagesx($t_im)/2 - $sx/2,
						imagesy($t_im)/2 - $sy/2,
						0, 0,
						imagesx($stamp), imagesy($stamp));
				}
			}

			/* Dirty, dirty hack */

			$t_width = imagesx($t_im) ;
			$t_height = imagesy($t_im) ;

			$tmp = tempnam(sys_get_temp_dir(), 'ggx');

			if ($tmp) {

				/*if ($need_stamp || !$need_alpha)
					imageJpeg($t_im, $tmp, 85);
				else
					imagePng($t_im, $tmp, 5); */

				imagewebp($t_im, $tmp, 80);

				$thumb_binary = file_get_contents($tmp);
				unlink($tmp);
			} else {
				$thumb_binary = '';
			}

			imagedestroy($o_im);
			imagedestroy($t_im);

			//return array($thumb_binary, $o_width, $o_height, $t_width, $t_height);

			return $thumb_binary;
		}

		return false;
	}

	function is_internal_req($base_url, $url) {
		$parts = parse_url($base_url);
		$cmp_base = $parts['scheme'] . '://' . $parts['host'] . $parts['path'];

		return mb_strpos($url, $cmp_base) === 0;
	}

	function mark_duplicate_objects($uniqid, &$conn, &$chandata, &$params) {
		if ($uniqid) {
			if (serialize($conn) == $_SESSION["cache"][$uniqid]["conn"]) {
				$conn = array("duplicate" => true);
			} else {
				$_SESSION["cache"][$uniqid]["conn"] = serialize($conn);
			}

			/*if (serialize($chandata) == $_SESSION["cache"][$uniqid]["chandata"]) {
				$chandata = array("duplicate" => true);
			} else {
				$_SESSION["cache"][$uniqid]["chandata"] = serialize($chandata);
			}*/

			if (serialize($params) == $_SESSION["cache"][$uniqid]["params"]) {
				$params = array("duplicate" => true);
			} else {
				$_SESSION["cache"][$uniqid]["params"] = serialize($params);
			}

			$_SESSION["cache"][$uniqid]["last"] = time();
		}
	}

	function is_valid_cid($cid) {
		global $redis;

		if ($cid) {
			$sessions = $redis->keys("ttirc_sessions,*");

			foreach ($sessions as $session) {
				list ($tmp, $sid) = explode(",", $session);

				if (sha1($sid . date("Ymd")) == $cid) {
					return true;
				}
			}
		}

		return false;
	}

?>
