<?php
	function format_users() {

		$dbh = DB::get();

		$sth = $dbh->query("SELECT * FROM ttirc_users ORDER BY login");

		$tmp = "";

		while ($line = $sth->fetch()) {
			$id = $line['id'];

			$tmp .= "<li id='U-$id' class='row' user_id='$id'>";
			$tmp .= "<input type='checkbox'
				row_id='U-$id'>";
			$tmp .= "&nbsp;<a href=\"#\" title=\"".__('Click to edit user')."\"
				onclick=\"edit_user($id)\">".
				$line['login']."</a>";
			$tmp .= "</li>";
		}

		return $tmp;

	}

	function show_users() {

	?>
	<div class="modal-header">
		<button type="button" data-dismiss="modal" class="close">&times;</button>
		<h3><?php echo __("Edit Users") ?></h3></div>
	<div class="modal-body">
		<div id="mini-notice" class="alert alert-warning" style='display : none'>&nbsp;</div>

		<ul class="list-unstyled scrollable panel panel-default" id="users-list">
			<?php echo format_users(); ?>
		</ul>
	</div>

	<div class="modal-footer">
		<div style='float : left'>
			<button class="btn" onclick="create_user()">
				<?php echo __('Add user') ?></button>
			<button class="btn" onclick="reset_user()">
				<?php echo __('Reset password') ?></button>
			<button class="btn btn-danger" onclick="delete_user()">
				<?php echo __('Delete') ?></button>
		</div>
		<button class="btn btn-primary" type="submit" data-dismiss="modal">
			<?php echo __('Close') ?></button></div>
	</div>
	<?php

	}
?>
