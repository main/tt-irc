<?php
	// Original from http://www.daniweb.com/code/snippet43.html

	require_once "config.php";

	$session_expire = SESSION_EXPIRE_TIME; //seconds
	$session_name = defined('TTIRC_SESSION_NAME') && TTIRC_SESSION_NAME ? TTIRC_SESSION_NAME : "ttirc_sid2";

	if (is_server_https()) {
		ini_set("session.cookie_secure", true);
	}

	ini_set("session.gc_probability", 50);
	ini_set("session.name", $session_name);
	ini_set("session.use_only_cookies", true);
	ini_set("session.gc_maxlifetime", SESSION_EXPIRE_TIME);

	function validate_session() {
		$dbh = DB::get();

		if (SINGLE_USER_MODE) {
			return true;
		}

		if ($_SESSION["ref_schema_version"] != get_schema_version(true)) {
			return false;
		}

		if ($_SESSION["uid"]) {

			if (!defined('_SKIP_SESSION_UA_CHECKS') && $_SESSION['http_ua_hash'] != sha1($_SERVER['HTTP_USER_AGENT']))
				return false;

			$sth = $dbh->prepare("SELECT pwd_hash FROM ttirc_users WHERE id = ?");
			$sth->execute([$_SESSION['uid']]);
			if ($row = $sth->fetch()) {
				if ($row['pwd_hash'] != $_SESSION["pwd_hash"]) {
					return false;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	function ttirc_open ($s, $n) {
		return true;
	}

	function ttirc_read ($id) {
		global $redis;

		try {
			return base64_decode($redis->get("ttirc_sessions,$id"));
		} catch (CredisException $e) {
			return false;
		}
	}

	function ttirc_write ($id, $data) {
		global $session_expire;
		global $redis;

		if (!$data) {
			return false;
		}

		try {
			$redis->set("ttirc_sessions,$id", base64_encode($data));
			$redis->expire("ttirc_sessions,$id", $session_expire);
		} catch (CredisException $e) {
			return false;
		}

		return true;
	}

	function ttirc_close () {
		return true;
	}

	function ttirc_destroy ($id) {
		global $redis;

		try {
			$redis->del("ttirc_sessions,$id");
		} catch (CredisException $e) {
			return false;
		}

		return true;
	}

	function ttirc_gc ($expire) {
		return true;
	}

	session_set_save_handler("ttirc_open",
		"ttirc_close", "ttirc_read", "ttirc_write",
		"ttirc_destroy", "ttirc_gc");

	session_set_cookie_params(SESSION_COOKIE_LIFETIME);

	if (isset($_COOKIE[session_name()])) {
		session_start();
	}
?>
