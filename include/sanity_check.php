<?php
	require_once "functions.php";

	define('SCHEMA_VERSION', 16);
	define('EXPECTED_CONFIG_VERSION', 2);

	require_once "config.php";

	function initial_sanity_check() {
		$errors = [];

		if (CONFIG_VERSION != EXPECTED_CONFIG_VERSION) {
			array_push($errors, "config: your config file version is incorrect. See config.php-dist.");
		}

		foreach (array_filter(glob("cache/*"), "is_dir") as $dir) {
			if (!is_writable($dir)) {
				array_push($errors, "Cache directory is not writable: $dir");
			}
		}

		if (!function_exists("json_encode")) {
			array_push($errors, "PHP support for JSON is required, but was not found.");
		}

		if (!function_exists("pg_connect")) {
			array_push($errors, "PHP support for PostgreSQL is required for configured DB_TYPE in config.php");
		}

		if (!class_exists("PDO")) {
			array_push($errors, "PHP support for PDO is required but was not found.");
		}

		if (!function_exists("mb_strlen")) {
			array_push($errors, "PHP support for mbstring functions is required but was not found.");
		}

		if (!function_exists("hash")) {
			array_push($errors, "PHP support for hash() function is required but was not found.");
		}

		if (!function_exists("mime_content_type")) {
			array_push($errors, "PHP function mime_content_type() is missing, try enabling fileinfo module.");
		}

		if (!class_exists("DOMDocument")) {
			array_push($errors, "PHP support for DOMDocument is required, but was not found.");
		}

		if (!function_exists("imagecreatefromstring") && FETCH_URL_TITLES) {
			array_push($errors, "PHP support for GD is required when using FETCH_URL_TITLES, but was not found.");
		}

		if (count($errors) > 0 && $_SERVER['REQUEST_URI']) { ?>
			<!DOCTYPE html>
			<html>
			<head>
			<title>Startup failed</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<style type="text/css">
				body {
					background : #900;
					color : white;
				}
				</style>
			</head>
		<body>
			<h1>Startup failed</h1>

			<ul>
			<?php foreach ($errors as $error) { echo "<li>" . format_error($error) . "</li>"; } ?>
			</ul>
		</body>
		</html>

		<?php
			die;
		} else if (count($errors) > 0) {
			echo "Tiny Tiny IRC was unable to start properly. This usually means a misconfiguration or an incomplete upgrade.\n";
			echo "Please fix errors indicated by the following messages:\n\n";

			foreach ($errors as $error) {
				echo " * $error\n";
			}

			exit(-1);
		}
	}

	initial_sanity_check();

?>
