<?php
	require_once "config.php";

	if (!defined('DISABLE_SESSIONS')) {
		if (!$_SESSION["prefs_cache"])
			$_SESSION["prefs_cache"] = array();
	}

	function get_pref($pref_name, $user_id = false, $die_on_error = false) {

		$prefs_cache = true;

		if (!$user_id) {
			$user_id = $_SESSION["uid"];
		} else {
			$user_id = sprintf("%d", $user_id);
			//$prefs_cache = false;
		}

		if (!defined('DISABLE_SESSIONS')) {
			if ($_SESSION["prefs_cache"] && @$_SESSION["prefs_cache"][$pref_name]) {
				$tuple = $_SESSION["prefs_cache"][$pref_name];
				return convert_pref_type($tuple["value"], $tuple["type"]);
			}
		}

		$dbh = DB::get();

		$sth = $dbh->prepare("SELECT
			value,ttirc_prefs_types.type_name as type_name
			FROM
				ttirc_user_prefs,ttirc_prefs,ttirc_prefs_types
			WHERE
				ttirc_user_prefs.pref_name = ? AND
				ttirc_prefs_types.id = type_id AND
				owner_uid = ? AND
				ttirc_user_prefs.pref_name = ttirc_prefs.pref_name");

		$sth->execute([$pref_name, $user_id]);

		if ($row = $sth->fetch()) {

			$value = $row['value'];
			$type_name = $row['type_name'];

			if (!defined('DISABLE_SESSIONS')) {
				if ($user_id == $_SESSION["uid"]) {
					$_SESSION["prefs_cache"][$pref_name]["type"] = $type_name;
					$_SESSION["prefs_cache"][$pref_name]["value"] = $value;
				}
			}

			return convert_pref_type($value, $type_name);

		} else {
			if ($die_on_error) {
				die("Fatal error, unknown preferences key: $pref_name");
			} else {
				return null;
			}
		}
	}

	function convert_pref_type($value, $type_name) {
		if ($type_name == "bool") {
			return $value == "true";
		} else if ($type_name == "integer") {
			return sprintf("%d", $value);
		} else {
			return $value;
		}
	}

	function set_pref($pref_name, $value, $user_id = false, $strip_tags = true) {

		if ($strip_tags) $value = strip_tags($value);

		$dbh = DB::get();

		if (!$user_id) {
			$user_id = $_SESSION["uid"];
		} else {
			$user_id = sprintf("%d", $user_id);
			$prefs_cache = false;
		}

		$type_name = "";
		$current_value = "";

		if (!defined('DISABLE_SESSIONS')) {
			if ($_SESSION["prefs_cache"] && @$_SESSION["prefs_cache"][$pref_name]) {
				$type_name = $_SESSION["prefs_cache"][$pref_name]["type"];
				$current_value = $_SESSION["prefs_cache"][$pref_name]["value"];
			}
		}

		if (!$type_name) {
			$sth = $dbh->prepare("SELECT type_name
				FROM ttirc_prefs,ttirc_prefs_types
				WHERE pref_name = ? AND type_id = ttirc_prefs_types.id");
			$sth->execute([$pref_name]);

			if ($row = $sth->fetch()) {
				$type_name = $row['type_name'];
			} else if ($current_value == $value) {
				return;
			}
		}

		if ($type_name) {
			if ($type_name == "bool") {
				if ($value == "1" || $value == "true") {
					$value = "true";
				} else {
					$value = "false";
				}
			} else if ($type_name == "integer") {
				$value = sprintf("%d", $value);
			}

			if ($pref_name == 'USER_TIMEZONE' && $value == '') {
				$value = 'UTC';
			}

			$sth = $dbh->prepare("UPDATE ttirc_user_prefs SET
				value = ? WHERE pref_name = ?
					AND owner_uid = ?");
			$sth->execute([$value, $pref_name, $user_id]);

			if (!defined('DISABLE_SESSIONS')) {
				if ($user_id == $_SESSION["uid"]) {
					$_SESSION["prefs_cache"][$pref_name]["type"] = $type_name;
					$_SESSION["prefs_cache"][$pref_name]["value"] = $value;
				}
			}
		}
	}
?>
