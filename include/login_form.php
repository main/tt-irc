<!DOCTYPE html>
<html>
<head>
	<title>Tiny Tiny IRC : Login</title>
	<link rel="shortcut icon" type="image/png" href="images/favicon.png">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script type="text/javascript" src="lib/bootstrap/js/jquery.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<?php stylesheet_tag("lib/bootstrap/css/bootstrap.min.css") ?>
	<?php stylesheet_tag("lib/bootstrap/css/bootstrap-theme.min.css") ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
	body {
		padding : 2em;
	}

	div.header {
		border-width : 0px 0px 1px 0px;
		border-style : solid;
		border-color : #88b0f0;
		margin-bottom : 1em;
		padding-bottom : 5px;
	}

	div.footer {
		margin-top : 1em;
		padding-top : 5px;
		border-width : 1px 0px 0px 0px;
		border-style : solid;
		border-color : #88b0f0;
		text-align : center;
		color : gray;
		font-size : 12px;
	}

	div.footer a {
		color : gray;
	}

	div.footer a:hover {
		color : #88b0f0;
	}

	form {
		max-width : 450px;
		margin-left : auto;
		margin-right : auto;
	}

	</style>

</head>

<body>

<script type="text/javascript">
var cookie_lifetime = <?php print SESSION_COOKIE_LIFETIME ?>;

$(document).ready(function() {
	sskeys = Object.keys(sessionStorage) || [];

	$.each(sskeys, function (i,k) {
		if (k.indexOf("ttirc") !== -1)
			sessionStorage.removeItem(k);
	});

	$(".login").focus();

	$(".cb_disable_emoticons").on('click', function() {
		set_cookie("ttirc_emoticons", !this.checked, cookie_lifetime);
	});

	$(".cb_noembed_mode").on('click', function() {
		set_cookie("ttirc_noembed_mode", this.checked, cookie_lifetime);
	});

	$(".cb_image_proxy").on('click', function() {
		set_cookie("ttirc_imgproxy", this.checked, cookie_lifetime);
	});

	$(".lang_select").on('change', function() {
		set_cookie("ttirc_lang", $(this).val(), cookie_lifetime);

		window.location.reload();
	});

	if (get_cookie("ttirc_emoticons") == "false")
		$(".cb_disable_emoticons").attr("checked", "true");

	if (get_cookie("ttirc_noembed_mode") == "true")
		$(".cb_noembed_mode").attr("checked", "true");

	if (get_cookie("ttirc_imgproxy") == "true")
		$(".cb_image_proxy").attr("checked", "true");

});
</script>

<div class='header'>
	<img src="images/logo_big.png">
</div>

<form class="form-horizontal" action="" method="POST" id="loginForm" name="loginForm">
<input type="hidden" name="login_action" value="do_login">

		<?php if ($_SESSION["login_error_msg"]) { ?>
			<div class="pagination-centered">
			<div class="alert alert-danger">
				<?php echo $_SESSION["login_error_msg"] ?>
			</div>
			<?php $_SESSION["login_error_msg"] = ""; ?>
			</div>
		<?php } ?>

	<div class="form-group">
		<label class="col-sm-4 control-label"><?php echo __("Login:") ?></label>
		<div class="col-sm-6">
			<input class="form-control login" name="login" type="text" required="1"
				value="<?php echo $_SESSION["fake_login"] ?>" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label"><?php echo __("Password:") ?></label>
		<div class="col-sm-6">
			<input class="form-control" type="password" name="password" required="1"
				value="<?php echo $_SESSION["fake_password"] ?>"/>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label"><?php echo __("Language:") ?></label>
		<div class="col-sm-6">
			<?php
				print_select_hash("language", $_COOKIE["ttirc_lang"], get_translations(),
					"class='form-control lang_select'");
			?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-4"></div>
		<div class="col-sm-6">
			<div class="checkbox"><label>
				<input class="cb_disable_emoticons" type="checkbox">
				<?php echo __("Disable emoticons") ?>
			</label></div>
		</div>
		<div class="col-sm-4"></div>
		<div class="col-sm-6">
			<div class="checkbox"><label>
				<input class="cb_noembed_mode" type="checkbox">
				<?php echo __("Disable media inlining") ?>
			</label></div>
		</div>
		<div class="col-sm-4"></div>
		<div class="col-sm-6">
			<div class="checkbox"><label>
				<input class="cb_image_proxy" type="checkbox">
				<?php echo __("Enable image proxy") ?>
			</label></div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-4"></div>
		<div class="col-sm-6">
			<button class="btn btn-primary" type="submit"><?php echo __('Log in') ?></button>
		</div>
	</div>

</form>

<div class='footer'>
	<a href="https://tt-rss.org/tt-irc/">Tiny Tiny IRC</a>
	&copy; 2010&ndash;<?php echo date('Y') ?> <a href="https://fakecake.org/">Andrew Dolgov</a>
</div>

</form>

</body></html>
